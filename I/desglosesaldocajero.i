
/*------------------------------------------------------------------------
    File        : desglosesaldocajero.i
    Purpose     : 

    Syntax      :

    Description : Se definen la tablas temporales del webservice	

    Author(s)   : JVA
    Created     : Wed Feb 19 23:41:45 CST 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE ttEncdatos
    FIELD INid              AS INTEGER
    FIELD INtoken           AS CHARACTER
    FIELD INnIDUnion        AS CHARACTER
    FIELD INnIDCajaPopular  AS CHARACTER
    FIELD INnIDSucursa      AS CHARACTER
    FIELD INsIDUsuario      AS CHARACTER
    FIELD respuesta         AS CHARACTER
    FIELD INnCajaAsignada   AS CHARACTER
    INDEX ttencdatos    INid.
        
    
DEFINE TEMP-TABLE tt-desMov
    FIELD tt-id                  AS INTEGER
    FIELD tt-DesMovCaja          AS CHARACTER
    FIELD tt-DesMovFichaDep      AS CHARACTER
    FIELD tt-DesMovFichaRet      AS CHARACTER
    FIELD tt-DesMovFondoDep      AS CHARACTER
    FIELD tt-DesMovFondoRet      AS CHARACTER
    FIELD tt-DesMovSumaDep       AS CHARACTER
    FIELD tt-DesMovSumaRet       AS CHARACTER
    
    FIELD tt-DesMovResConsSalIni AS CHARACTER
    FIELD tt-DesMovSaldCajSalIni AS CHARACTER
    FIELD tt-DesMovDifSalIni     AS CHARACTER
    
    FIELD tt-DesMovResConsTotIng AS CHARACTER
    FIELD tt-DesMovSaldCajTotIng AS CHARACTER
    FIELD tt-DesMovDifTotIng     AS CHARACTER
    
    FIELD tt-DesMovResConsTotEge AS CHARACTER
    FIELD tt-DesMovSaldCajTotEge AS CHARACTER
    FIELD tt-DesMovDifTotEge     AS CHARACTER
    
    FIELD tt-DesMovResConsSalAct AS CHARACTER
    FIELD tt-DesMovSaldCajSalAct AS CHARACTER
    FIELD tt-DesMovDifSalAct     AS CHARACTER

    FIELD tt-DesMovDesCajNumCheq AS CHARACTER
    FIELD tt-DesMovDesCajTotCheq AS CHARACTER
    FIELD tt-DesMovDesCajNumDoc  AS CHARACTER
    FIELD tt-DesMovDesCajTotDoc  AS CHARACTER
    FIELD tt-DesMovDesCajEfec    AS CHARACTER
    
    INDEX ktt-desMov     tt-id.
    
    
    

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
