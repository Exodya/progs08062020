DEFINE TEMP-TABLE ttEncdatos NO-UNDO /*Definiendo la tabla temporal*/
    FIELD INid             AS INTEGER
    FIELD INtoken          AS CHARACTER
    FIELD INnIDUnion       AS CHARACTER
    FIELD INnIDCajaPopular AS CHARACTER 
    FIELD INnIDSucursal    AS CHARACTER
    FIELD INsIDUsuario     AS CHARACTER
    FIELD INsFechaIni      AS CHARACTER
    FIELD INsFechafin      AS CHARACTER
    FIELD respuesta        AS CHARACTER
    FIELD titulo           AS CHARACTER
    INDEX kEncdatos INid. 
    .
    
/*CASANDRA* AQUI SE DEFINE LA TABLA DETALLE*/
DEFINE TEMP-TABLE ttDetalle NO-UNDO 
    FIELD id               AS INTEGER
    FIELD Numero           AS CHARACTER
    FIELD Unione           AS CHARACTER
    FIELD CajaPopular      AS CHARACTER
    FIELD Sucursal         AS CHARACTER
    FIELD NumerodeSocio    AS CHARACTER
    FIELD Nombre           AS CHARACTER
    FIELD ApellidoPaterno  AS CHARACTER
    FIELD ApellidoMaterno  AS CHARACTER
    FIELD FechaAlta        AS CHARACTER
    FIELD UnionR           AS CHARACTER
    FIELD CajaPopularR     AS CHARACTER
    FIELD SucursalR        AS CHARACTER
    FIELD NumerodeSocioR   AS CHARACTER
    FIELD NombreS          AS CHARACTER
    FIELD ApellidoPaternoS AS CHARACTER
    FIELD ApellidoMaternoS AS CHARACTER
    INDEX KDetalle id.