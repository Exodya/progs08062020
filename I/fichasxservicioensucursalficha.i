
/*------------------------------------------------------------------------
    File        : fichaxservicioensucursalficha.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Fri Jun 05 19:28:11 CDT 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
DEFINE TEMP-TABLE ttEncdatos NO-UNDO /*Definiendo la tabla temporal*/
    FIELD iNid             AS INTEGER
    FIELD iNtoken          AS CHARACTER
    FIELD iNnIDUnion       AS CHARACTER
    FIELD iNnIDCajaPopular AS CHARACTER 
    FIELD iNnIDSucursal    AS CHARACTER
    FIELD iNsIDUsuario     AS CHARACTER
    
    FIELD iNsFechaInicial  AS CHARACTER
    FIELD iNsFechaFinal    AS CHARACTER
    FIELD respuesta        AS CHARACTER
    FIELD iNGenerar        AS CHARACTER
    FIELD iNMenu           AS CHARACTER
    FIELD iNCajero         AS CHARACTER
    FIELD nIDCajeroInicio  AS CHARACTER
    FIELD iNServicio       AS CHARACTER
    
    FIELD sNombreCaja      AS CHARACTER
    FIELD sNombreSucursal  AS CHARACTER
    FIELD fecha            AS CHARACTER
    FIELD termino          AS CHARACTER
    INDEX kEncdatos iNid.

DEFINE TEMP-TABLE ttDetficha NO-UNDO
    FIELD id                AS INTEGER
    FIELD idSucursal        AS CHARACTER
    FIELD idCajaAsignada    AS CHARACTER
    FIELD idTipoTransaccion AS CHARACTER
    FIELD usuario           AS CHARACTER
    FIELD fecha             AS CHARACTER
    FIELD hora              AS CHARACTER
    FIELD sucsocio          AS CHARACTER
    FIELD iDSocio           AS CHARACTER
    FIELD nomsocio          AS CHARACTER
    FIELD servicio          AS CHARACTER
    FIELD cargo             AS CHARACTER
    FIELD abono             AS CHARACTER
    FIELD aHref             AS CHARACTER
    INDEX kdetficha id.
    
    
DEFINE TEMP-TABLE tttotales NO-UNDO
    FIELD id     AS INTEGER
    FIELD cargos AS CHARACTER
    FIELD abonos AS CHARACTER

    INDEX ktotal id.

DEFINE TEMP-TABLE ttCajaAsignada NO-UNDO
    FIELD IDCajaAsignada AS CHARACTER
    FIELD Descripcion    AS CHARACTER
    INDEX kcaja IDCajaAsignada.
    
DEFINE TEMP-TABLE ttServicio NO-UNDO
    FIELD IDServicio     AS CHARACTER
    FIELD Accion         AS CHARACTER 
    INDEX kservicio IDServicio.    
