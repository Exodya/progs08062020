
/*------------------------------------------------------------------------
    File        : menulistadodefichas.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Sat May 16 16:05:04 CDT 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
DEFINE TEMP-TABLE ttEncdatos
    FIELD INid              AS INTEGER
    FIELD INtoken           AS CHARACTER
    FIELD INnIDUnion        AS CHARACTER
    FIELD INnIDCajaPopular  AS CHARACTER
    FIELD INnIDSucursal     AS CHARACTER
    FIELD INsIDUsuario      AS CHARACTER
    FIELD respuesta         AS CHARACTER
    
    
    FIELD INMenu                AS CHARACTER
    FIELD INFechaReporte        AS CHARACTER
    INDEX ttencdatos INid.

DEFINE TEMP-TABLE ttdetcajaasignada NO-UNDO
    FIELD INid           AS INTEGER
    FIELD IDCajaasignada AS INTEGER
    FIELD nombre         AS CHARACTER
    INDEX kdetcaja INid IDCajaasignada.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
