
/*------------------------------------------------------------------------
    File        : fichasconhorarioxsuc.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Wed May 27 18:49:27 CDT 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE ttEncdatos
    FIELD iNid             AS INTEGER
    FIELD iNtoken          AS CHARACTER
    FIELD iNnIDUnion       AS CHARACTER
    FIELD iNnIDCajaPopular AS CHARACTER
    FIELD iNnIDSucursal    AS CHARACTER
    FIELD iNsIDUsuario     AS CHARACTER
    FIELD iNEjercicio      AS CHARACTER
    FIELD iNdFechaInicial  AS CHARACTER
    FIELD iNdFechaFinal    AS CHARACTER
    FIELD iNsServicio      AS CHARACTER
    FIELD iNsAccion        AS CHARACTER
    FIELD sNombreCaja AS CHARACTER
    FIELD sRFCCaja    AS CHARACTER
    FIELD fecha       AS CHARACTER
    FIELD respuesta        AS CHARACTER
    INDEX ttencdatos INid.
    
/*DANIEL AQUI DEFINIR TYABLA TEMPORAL DETALLE*/
DEFINE TEMP-TABLE ttDetFicha NO-UNDO
    FIELD id                AS INTEGER
    FIELD iDSucursal        AS CHARACTER
    FIELD iDCajaAsignada    AS CHARACTER
    FIELD iDTipotransaccion AS CHARACTER
    FIELD iDFicha           AS CHARACTER
    FIELD fechaFicha        AS CHARACTER
    FIELD hora              AS CHARACTER
    INDEX kdetficha id.


/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
