DEFINE TEMP-TABLE ttEncdatos NO-UNDO /*Definiendo la tabla temporal*/
    FIELD INid                          AS INTEGER
    FIELD INtoken                    AS CHARACTER
    FIELD INnIDUnion              AS CHARACTER
    FIELD INnIDCajaPopular    AS CHARACTER 
    FIELD INnIDSucursal          AS CHARACTER
    FIELD INsIDUsuario            AS CHARACTER
    FIELD respuesta         AS CHARACTER
 INDEX KEncdatos    INid.
    .

DEFINE TEMP-TABLE ttSucursal NO-UNDO 
    FIELD IDSucursal  AS INTEGER
    FIELD Nombre     AS CHARACTER
 INDEX kSucursal IDSucursal.