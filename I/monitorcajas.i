
/*------------------------------------------------------------------------
    File        : MonitorCajas.i
    Purpose     : 

    Syntax      :

    Description : Se definen la tablas temporales del webservice	

    Author(s)   : JVA
    Created     : Wed Feb 19 23:41:45 CST 2020
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE ttEncdatos
    FIELD INid              AS INTEGER
    FIELD INtoken           AS CHARACTER
    FIELD INnIDUnion        AS CHARACTER
    FIELD INnIDCajaPopular  AS CHARACTER
    FIELD INnIDSucursa      AS CHARACTER
    FIELD INsIDUsuario      AS CHARACTER
    FIELD ttEncPrincCaj     AS CHARACTER
    FIELD ttEncSecCaj       AS CHARACTER
    FIELD respuesta         AS CHARACTER
    FIELD nSucursalVER      AS CHARACTER
    INDEX ttencdatos    INid.
    
/***DEFINE TEMP-TABLE tt-EncMonitorCaj
    FIELD tt-id             AS INTEGER
    FIELD ttEncPrincCaj     AS CHARACTER
    FIELD ttEncSecCaj       AS CHARACTER
    INDEX Ktt-EncMonitorCaj  tt-id.***/


DEFINE TEMP-TABLE tt-MonitorCajas
    FIELD tt-id             AS INTEGER
    FIELD tt-nombre         AS CHARACTER
    FIELD tt-cajero         AS CHARACTER
    FIELD tt-tipo           AS CHARACTER
    FIELD tt-saldoIni       AS CHARACTER
    FIELD tt-depositos      AS CHARACTER
    FIELD tt-retiros        AS CHARACTER
    FIELD tt-saldoDia       AS CHARACTER
    FIELD tt-Estado         AS CHARACTER
    FIELD tt-saldoAcum      AS CHARACTER
    INDEX Ktt-MonitorCajas tt-id.
    
    
DEFINE TEMP-TABLE tt-totalesCaj
    FIELD tt-id             AS INTEGER
    FIELD tt-totCajeros     AS CHARACTER
    FIELD tt-totSaldoIni    AS CHARACTER
    FIELD tt-totDepositos   AS CHARACTER
    FIELD tt-totRetiros     AS CHARACTER
    FIELD tt-totSaldoDia    AS CHARACTER
    FIELD tt-totSaldoAcum   AS CHARACTER
    INDEX Ktt-totales  tt-id. 
    
    
DEFINE TEMP-TABLE tt-SaldoServCaj
    FIELD tt-id             AS INTEGER
    FIELD tt-totSalIniServ  AS CHARACTER
    FIELD tt-totDepoServ    AS CHARACTER
    FIELD tt-totRetiServ    AS CHARACTER
    FIELD tt-totSaldAcuServ AS CHARACTER
    FIELD tt-diferencia     AS CHARACTER
    INDEX ktt-SaldoServ   tt-id.
    
    
DEFINE TEMP-TABLE tt-desMov
    FIELD tt-id                  AS INTEGER
    FIELD tt-DesMovCaja          AS CHARACTER
    FIELD tt-DesMovFichaDep      AS CHARACTER
    FIELD tt-DesMovFichaRet      AS CHARACTER
    FIELD tt-DesMovFondoDep      AS CHARACTER
    FIELD tt-DesMovFondoRet      AS CHARACTER
    FIELD tt-DesMovSumaDep       AS CHARACTER
    FIELD tt-DesMovSumaRet       AS CHARACTER
    
    FIELD tt-DesMovResConsSalIni AS CHARACTER
    FIELD tt-DesMovSaldCajSalIni AS CHARACTER
    FIELD tt-DesMovDifSalIni     AS CHARACTER
    
    FIELD tt-DesMovResConsTotIng AS CHARACTER
    FIELD tt-DesMovSaldCajTotIng AS CHARACTER
    FIELD tt-DesMovDifTotIng     AS CHARACTER
    
    FIELD tt-DesMovResConsTotEge AS CHARACTER
    FIELD tt-DesMovSaldCajTotEge AS CHARACTER
    FIELD tt-DesMovDifTotEge     AS CHARACTER
    
    FIELD tt-DesMovResConsSalAct AS CHARACTER
    FIELD tt-DesMovSaldCajSalAct AS CHARACTER
    FIELD tt-DesMovDifSalAct     AS CHARACTER

    FIELD tt-DesMovDesCajNumCheq AS CHARACTER
    FIELD tt-DesMovDesCajTotCheq AS CHARACTER
    FIELD tt-DesMovDesCajNumDoc  AS CHARACTER
    FIELD tt-DesMovDesCajTotDoc  AS CHARACTER
    FIELD tt-DesMovDesCajEfec    AS CHARACTER
    
    INDEX ktt-desMov     tt-id.
    
    
    

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
