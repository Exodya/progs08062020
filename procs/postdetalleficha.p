/*CTRLSOFT*
<!--WSS
/*-------------------------------------------------------------------------------
  File              : detalleficha.html  
  Description       : Aplicativo que genera el movimiento final de fichas
  Author            : 
  Created           :
  Notes             : 
                                            
------------------------------- Modificaciones ----------------------------------
  No.  Author          Date        		Description
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
    1  LSO 						 03/12/2015     Se a�ade verificaci�n de movimiento de remesa. 
    2  Wilbert Cetina  20/07/2016     bloqueo al servicio SEAU 
    3  Wilbert Cetina  20/08/2015  se anexa el proces para desglosar el iva en servicios 
                                  por comision en aquellos servicios que lo requieran.     
---------------------------------------------------------------------------------*/
-->

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">

<html>
<head>
<!--WSS
 {colores.i}
 {ssiwebsecurity.i}
*CTRLSOFT*/
 
/*CTRLSOFT* AQUI ESTABAN LAS NUEVAS TABLAS TEMPORALES *CTRLSOFT*/
 
/*CTRLSOFT*
 -->

<meta name="wsoptions" content="no-output">
<title>Detalle Ficha</title>
<script language="JavaScript1.2" src="/class/funcionesjs.js"></script>
<script language="JavaScript1.2" src="/class/funcionesjsdepositos.js"></script>
<script type="text/javascript" src="/class/validarcampos_jr.js"></script>
<script type="text/javascript" src="/class/distribucionsalvobuencobro.js"></script>

<script>
identificador= new Array();
tecla = new Array();
function arrayservice(){
   var i = 0;
   var j = 0;
<!--wss
for each servicio where Servicio.TeclaRapida <> "" USE-INDEX TeclaRapida NO-LOCK:
-->
   identificador[i] = '`Servicio.IDServicio`';
   i= i + 1;
   tecla[j] = '`Servicio.TeclaRapida`';
   j = j + 1;
<!--wss end. -->
}
/* INICIO PROVI-2 */
function valida(){
   var srv = document.getElementById("Servicio").value;
   srv = srv.toUpperCase(); 
   if(srv == "SEAU"){
     alert('El servicio ya no es vigente, selecciona otro servicio para el seguro');
     document.getElementById("Servicio").value="";
     document.getElementById("Servicio").focus();
   }

}
/* FIN:PROVI-2 */
</script>
<script language="JavaScript1.2" src="/class/fastkeys.js"></script>
<Script Language="SpeedScript">
*CTRLSOFT*/

/*------------------------------------------------------------------------
  File: DetalleFicha.html
  Description: Aplicacion de un catalogo modelo
               Realiza mantenimiento de varias tablas.
  Input Parameters:
      <none>
  Output Parameters:
      <none>
  Author: Direccion de Operaciones
          Servicios y Suministros en Informatica, S.A. de C.V.
  In charge of : Ing. Walter Hau Echeverr�a. 
  Created: Febrero, 2000.
  Last Updated:2000/feb/24
------------------------------------------------------------------------*/
/*      This .html file was created with the WebSpeed Workshop 2.1      */
/*----------------------------------------------------------------------*/
/*Cobro Servicio*/
DEF VAR sIDSerForaneoSer    AS CHAR.
DEF VAR nPagoSerReferencia  AS CHAR.
DEF VAR nAbonoNoPag         AS INT     INITIAL 0.
DEF VAR ComisionQseCobra    AS CHAR.
DEF VAR ComisionQseCobraFin AS DECIMAL.
DEF VAR IVAComisionQseCobra AS DECIMAL. 
DEF VAR nUltimoDetalle      AS INT     INITIAL 0.
DEF VAR SiComision          AS INT     INITIAL 0.  
DEF VAR nNumeroDetalle      AS INT.
/* ************************* SECCION DE DEFINICION DE VARIABLES ****************************** */


 &SCOPED-DEFINE TABLE DetalleFichaTemp
 &SCOPED-DEFINE TABLE0 DetalleFicha
 &SCOPED-DEFINE TABLE1 Ficha
 &SCOPED-DEFINE TABLE2 Servicio
 &SCOPED-DEFINE TABLE3 Socio
 &SCOPED-DEFINE TABLE4 TipoTransaccion
 &SCOPED-DEFINE TABLE5 Usuario
 &SCOPED-DEFINE TABLE6 CajaAsignada
 &SCOPED-DEFINE TABLE7 FolioCaja
 &SCOPED-DEFINE TABLE9 ParametrosGenerales
 /****************************************MAM*************************************
 &SCOPED-DEFINE TABLE99 AsignacionVT */
 
 &SCOPED-DEFINE TABLE_ DetalleFichaTemp_
 &SCOPED-DEFINE TABLE1_ Ficha_
 &SCOPED-DEFINE TABLE2_ Servicio_
 &SCOPED-DEFINE TABLE3_ Socio_
 &SCOPED-DEFINE TABLE6_ CajaAsignada_
 &SCOPED-DEFINE TABLE7_ FolioCaja_

 &SCOPED-DEFINE tmTABLE tmDetalleFichaTemp
 &SCOPED-DEFINE tmTABLE1 tmFicha
 &SCOPED-DEFINE tmTABLE2 tmServicio
 &SCOPED-DEFINE tmTABLE3 tmSocio
 &SCOPED-DEFINE tmTABLE6 tmCajaAsignada
 &SCOPED-DEFINE tmTABLE7 tmFolioCaja
 
 &SCOPED-DEFINE PK   IDDetalleFicha
 &SCOPED-DEFINE FK11 IDUnion
 &SCOPED-DEFINE FK12 IDCajaPopular
 &SCOPED-DEFINE FK13 IDSucursal
 &SCOPED-DEFINE FK14 IDTipoTransaccion
 &SCOPED-DEFINE FK15 IDFicha
 &SCOPED-DEFINE FK16 Ejercicio
 &SCOPED-DEFINE FK17 IDUsuario
 &SCOPED-DEFINE FK18 IDCajaAsignada
 &SCOPED-DEFINE PK2  FechaFicha
 &SCOPED-DEFINE FK21 IDServicio
 &SCOPED-DEFINE FIELD1FK2 Descripcion
 &SCOPED-DEFINE FIELD1 IDUnionSocio
 &SCOPED-DEFINE FIELD2 IDCajaPopularSocio
 &SCOPED-DEFINE FIELD3 IDSucursalSocio
 &SCOPED-DEFINE FIELD4 IDSocio
 &SCOPED-DEFINE FIELD5 IDServicioForaneo
 &SCOPED-DEFINE FIELD51 Descripcion
 &SCOPED-DEFINE FIELD6 Concepto
 &SCOPED-DEFINE FIELD7 Cargo
 &SCOPED-DEFINE FIELD8 Abono
 &SCOPED-DEFINE FIELD9 SaldoAnterior
 &SCOPED-DEFINE FIELD10 Referencia
 &SCOPED-DEFINE FK32 IDTipoSocio
 &SCOPED-DEFINE FIELD1FK3 Nombre
 &SCOPED-DEFINE FIELD2FK3 PrimerApellido
 &SCOPED-DEFINE FIELD3FK3 SegundoApellido
 &SCOPED-DEFINE FIELD4FK3 IDTipoSocio

 &SCOPED-DEFINE LABEL-PK Mov.
 &SCOPED-DEFINE LABEL-FK6 Servicio
 &SCOPED-DEFINE LABEL-FIELD1 Union
 &SCOPED-DEFINE LABEL-FIELD2 Caja Popular
 &SCOPED-DEFINE LABEL-FIELD3 Sucursal
 &SCOPED-DEFINE LABEL-FIELD4 Socio
 &SCOPED-DEFINE LABEL-FIELD6 Concepto
 &SCOPED-DEFINE LABEL-FIELD9 Saldo Anterior
 &SCOPED-DEFINE LABEL-FIELD1FK7 Nombre
 &SCOPED-DEFINE LABEL-FIELD2FK7 PrimerApellido
 &SCOPED-DEFINE LABEL-FIELD3FK7 SegundoApellido

/*Definici�n de tama�os para los campos del forma html */
 &SCOPED-DEFINE SIZE-PK 5
 &SCOPED-DEFINE SIZE-FK6 4
 &SCOPED-DEFINE SIZE-FIELD6 30 /* M�ximo 80 chrs */
 &SCOPED-DEFINE SIZE-FIELD9 12 /* M�ximo 80 chrs */
 &SCOPED-DEFINE SIZE-FIELD7 12
 &SCOPED-DEFINE SIZE-FIELD8 12

/*Definici�n de anchos de columna de la tabla html */
&SCOPED-DEFINE WIDTH-PK 5%
&SCOPED-DEFINE WIDTH-FK6 10%
&SCOPED-DEFINE WIDTH-FIELD6 40%
&SCOPED-DEFINE WIDTH-FIELD9 20%
&SCOPED-DEFINE WIDTH-FIELD8 20%
&SCOPED-DEFINE WIDTH-ARROW 5%
&SCOPED-DEFINE TOTAL-WIDTH 100%

/*variables compartidas que ser�n compiladas y ejecutadas en el procedimiento externo OPENQRY.p*/
&SCOPED-DEFINE DEFINE-SHARED ~
    DEFINE SHARED BUFFER {&TABLE_} FOR {&TABLE}.
/*lista de los nombres de los buffers compartidos que ser�n usados para abrir el query*/
&SCOPED-DEFINE BUFFERS {&TABLE_}

/* sentencias para el query compartido (openqry.p) */
 &SCOPED-DEFINE EACH EACH {&TABLE_}
 &SCOPED-DEFINE NEW NEW
 &SCOPED-DEFINE BROWSE-NAME tblDetalleFichaTemp
 &SCOPED-DEFINE Result-Rows 10
 
/***************************************************************************************
***************************************************************************************/
/*LAST UPDATE Ing Susana Rodriguez S.
Description: Aplicaciones ide*/
&GLOBAL-DEFINE DEF-GENERALESIDE 

/*CTRLSOFT AQUI CODIGO DE RECAUDAIDLIB*/

/***************************************************************************************
***************************************************************************************/
        
DEF VAR sEACH            AS CHAR    INITIAL "{&EACH}".
DEF VAR sEACHAux1        AS CHAR    INITIAL "{&EACH}".
DEF VAR sEACHAux2        AS CHAR.
DEF VAR sOrder           AS CHAR    NO-UNDO.
DEF VAR sWhereEnt        AS CHAR.
DEF VAR sServicioForaneo AS CHAR    INITIAL "".

DEF VAR nSocio           AS INT.
DEF VAR nImporteTotal    AS DECIMAL.
DEF VAR nImporteTotal1   AS DECIMAL.

/*CTRLSOFT AQUI CODIGO DE {defvarcajas.i}*/

DEF VAR sServicioElegido        AS CHAR.
DEF VAR sDescripcion1           AS CHAR.
DEF VAR sTransaccion1           AS CHAR.
DEF VAR sTransaccion            AS CHAR.
DEF VAR sTransaccion2           AS CHAR.
DEF VAR sCajaAsignada           AS CHAR.
DEF VAR sWhereMod               AS CHAR.

DEF VAR sUnionFicha             AS CHAR.
DEF VAR sCajaPopularFicha       AS CHAR.
DEF VAR sSucursalFicha          AS CHAR.
DEF VAR sFolioFicha             AS CHAR.
DEF VAR sTipoServicio           AS CHAR.
DEF VAR sCadenaficha            AS CHAR.
DEF VAR sNuevoUsuario           AS CHAR.
DEF VAR sReferencia1            AS CHAR.
DEF VAR sFichasAcumuladas       AS CHAR.
DEF VAR sValorCookie            AS CHAR.
DEF VAR sMsgError               AS CHAR    FORMAT "X(100)".

DEF VAR nSaldo                  AS INT.
DEF VAR nCobroAcumulado         AS DECIMAL.
DEF VAR nGTAcumulado            AS DECIMAL.
DEF VAR i-count1                AS INT     INIT 0.

DEF VAR bHuboError              AS LOGICAL NO-UNDO.
DEF VAR bHuboError1             AS LOGICAL NO-UNDO.
DEF VAR bTablaVacia             AS LOGICAL NO-UNDO INITIAL NO.
DEF VAR banError                AS LOGICAL NO-UNDO INIT NO.
DEF VAR bRequiereSocio          AS LOGICAL NO-UNDO INITIAL NO.
/**************************Variables de Pago a TERCEROS**************************/
DEF VAR nYaSeAgregoComision     AS INT     INITIAL 0.
DEF VAR nPagoaTerceros          AS INT.
DEF VAR dComisionGRIM           AS DECIMAL.
DEF VAR dComisionGRIMIVAT       AS DECIMAL.
DEF VAR nSerVinculados          AS INT.
DEF VAR nCreaPT                 AS INT.
DEF VAR sIVATrasladado          AS CHAR.
DEF VAR sIVATrasladados         AS CHAR    EXTENT 12.
DEF VAR sPagTercerosSocCom      AS CHAR    EXTENT 4.
DEF VAR sNomServicioPT          AS CHAR    EXTENT 50.
DEF VAR nServicioPT             AS INT     INITIAL 0.      
/*Variables para la creacion D DetalleFichaTemp de pago a Terceros*/
DEF VAR sCreaDetallesServicio   AS CHAR    EXTENT 10.
DEF VAR sCreaDetallesCargo      AS CHAR    EXTENT 10.
DEF VAR sCreaDetallesAbono      AS CHAR    EXTENT 10.
DEF VAR sCreaDetallesReferencia AS CHAR    EXTENT 10.
/************Variables de Comision Porcentual************/
DEF VAR sReferenciaPT           AS CHAR.
DEF VAR sPorcentaje             AS CHAR.
DEF VAR dPorcentaje             AS DECIMAL.
DEF VAR dTotalIVATPorcentual2   AS DECIMAL.
DEF VAR dTotalIVATPorcentual1   AS DECIMAL.
DEF VAR dTotalPorcentual1       AS DECIMAL.
DEF VAR dTotalPorcentual2       AS DECIMAL. 
/************Variables de Comision Caja************/
DEF VAR dTotalComCaja           AS DECIMAL.
DEF VAR dTotalCpturado          AS DECIMAL.
DEF VAR dTotalIVATCaja          AS DECIMAL.
DEF VAR dTotalComCajaIVAT       AS DECIMAL.
/************Variables comision x tipo de Socio************/
DEF VAR nSocioPT                AS INT.
DEF VAR nSocioMorosoPT          AS INT.
DEF VAR nNoSocioPT              AS INT.
/**************************MAM*Pago*de*Servicios*AGUA*LUZ****************************/
DEF VAR nOcultarBoton           AS INT     INITIAL 0.
DEF VAR vprueba                 AS CHAR. /*Cadena q se obtiene del lector Optico*/
DEF VAR cConstante              AS CHAR.
DEF VAR cRegistro               AS CHAR. /*FORMAT "x(12)".*/
DEF VAR cFechaVenAno            AS CHAR.
DEF VAR cFechaVenMes            AS CHAR.
DEF VAR cFechaVenDia            AS CHAR.
DEF VAR dFechaVencimientoR      AS DATE.
DEF VAR cImporteRecibo          AS CHAR. /*FORMAT "x(12)".*/
DEF VAR cDigitoVeri             AS CHAR.
DEF VAR vPuebaAux               AS CHAR. /*Cadena q contendra los 12 digitos si la
                           cadena solo trae los 12 digitos de verificacion*/                              
DEF VAR cvprueba                AS CHAR.   
DEF VAR com                     AS CHAR.
DEF VAR ni                      AS DECIMAL.
DEF VAR sConceptoServicio       AS CHAR.
DEF VAR sConceptoServicio2      AS CHAR.                        
/*************************************************************************************************************/ 
DEF VAR sReferenciaDF           AS INT     INITIAL 0. /*Contendra el numero de ficha */
DEF VAR sSeGuardoYa             AS CHAR    INITIAL "NO". /*Contendra Una bandera para evitar la duplicacion en la tabla*/
DEF VAR nMovInusual30           AS INT     INITIAL 0. /*Contendra la bandera si fue inusual x barrido de 30 dias*/
DEF VAR dMontoInusual           AS DECIMAL. /*Contendra el monto acumulado en 30 dias*/             
DEF VAR dMovRelevanteMonto      AS DECIMAL. /*Contendra la cantida minima para que sea un MOVIMIENTO Relevante*/
DEF VAR dMovInusualMonto        AS DECIMAL. /*Contendra la cantida minima para que sea un MOVIMIENTO Inusual*/
DEF VAR cNombreMov              AS CHAR. /*Contendra el nombre del movimiento (RELEVANTE E INUSUAL)*/
DEF VAR nAlertaTemprana         AS INT     INITIAL 0. /*Contendra valor 1 si se quiere desplegar la alerta temprana y 0 si no*/
DEF VAR dAbonoLVD               AS DECIMAL. /*Variable q contendra el monto q se acumulo en 30 dias*/
DEF VAR cValidacionLV           AS CHAR    INITIAL "".
/*************************************************************************************************************/
DEF VAR nUnionSocioServicio     AS INT     INITIAL 0.
DEF VAR nCajaSocioServicio      AS INT     INITIAL 0.
DEF VAR nSucursalSocioServicio  AS INT     INITIAL 0.
DEF VAR nIDSocioServicio        AS INT     INITIAL 0.
DEF BUFFER bfSerPagoTerceros FOR SerPagoTerceros.

def var lValidaSocDepGob        as logical.
def var lEntroDepGob            as logical.
def var nMontoCaso_Validar      as decimal.
def var lDocumentosCompletos    as logical.
def var nMontoAbono_Validar     as decimal format "->>>,>>>,>>>.99".
def var nAbonosCompleto_Validar as integer.
def buffer bfDetalleFichaTemp_Validar for DetalleFichaTemp.
def var lValidaSocioUsuario as logical.
def var nSaldoValida        as decimal format "->>>,>>>,>>>.99".
def buffer bfServicio_Validar for Servicio.
/* 20111116, rvargass: Valida si el servicio requiere comision. */
def var sIDServicioVCom       as char.
def var sConceptoVCom         as char.
def var nImporteVCom          as decimal.
def var nImporteVComAux       as decimal.
def var nImporteFODE          as decimal.
def var sImporteFODE          as char.
def var sConceptoFODE         as char.
def var nImporteCOOE          as decimal.
def var sImporteCOOE          as char.
def var sConceptoCOOE         as char.
def var nImporteIntCAST       as decimal.
def var nEdadV                as integer.
def var nSaldoCASO_V          as decimal.
def var nCompletaConAHO_V     as decimal.
def var lPromo80-20           as logical initial NO.
def var nImporte_80-20        as decimal.
def var lOmiteValidacionS     as logical.
def var sMotivoVal            as char.
def var lPromo80-20_SoloEmp   as logical initial NO.
def var sPromo80-20_SoloEmp   as char.
DEF VAR nTasaIVAComision      AS DECIMAL. /* PROVI-3 */
DEF VAR sDistribuirCheque     AS CHAR.
DEF VAR sCodigoPromo7030-8020 AS CHAR.
DEF VAR nLine_Promo7030-8020  AS INTEGER.
DEF VAR sA_Promo7030-8020     AS CHAR.
DEF VAR sB_Promo7030-8020     AS CHAR.
DEF VAR sS_Promo7030-8020     AS CHAR.
DEF VAR sC_Promo7030-8020     AS CHAR.

{I/detalleficha.i}
{I/cajahead.i}
{I/mensajesocio.i}
  
DEFINE INPUT-OUTPUT PARAMETER TABLE FOR ttEncdatdetficha.
DEFINE OUTPUT PARAMETER TABLE FOR ttDetdatdetficha.
DEFINE OUTPUT PARAMETER TABLE FOR ttservicios.    
DEFINE OUTPUT PARAMETER TABLE FOR ttEncdatcajahead.
DEFINE OUTPUT PARAMETER TABLE FOR ttsuccajahead.
DEFINE OUTPUT PARAMETER TABLE FOR ttEncmensasoc.
DEFINE OUTPUT PARAMETER TABLE FOR ttdetmensajesoc.
    
DEFINE VARIABLE vchrMensaje AS CHARACTER NO-UNDO.
DEFINE VARIABLE vintIdDet   AS INTEGER   NO-UNDO.


FIND FIRST ttEncdatdetficha EXCLUSIVE-LOCK NO-ERROR.
IF NOT AVAILABLE ttEncdatdetficha THEN 
DO:
    CREATE ttEncdatdetficha.
    ASSIGN
        ttEncdatdetficha.INid      = 1
        ttEncdatdetficha.respuesta = "ERROR no se encontro ttEncdatdetficha".
    RETURN.
END.    
    
    /*ASSIGN ttEncdatosficha.respuesta = "".*/
    
    /*ASSIGN ttEncdatos.respuesta = "".*/
    
{I/ttencdatos.i}
CREATE ttEncdatos.
BUFFER-COPY ttEncdatdetficha TO ttEncdatos.

{wslibrerias/ssiwebsecurityWS.i}   
 
DEFINE VAR sIDUsuario    LIKE Usuario.IDUsuarioEncode.
DEF    VAR sIDAplicacion AS CHAR      INITIAL "DETFICHTRAS".
DEF    VAR sRenova       AS CHARACTER INITIAL ''.



IF SSICanIn() = "" THEN DO:
    ttEncdatdetficha.respuesta = "ERROR SSICanIn()".
     RETURN.
END.     
RUN SLogin(INPUT-OUTPUT nUnion, INPUT-OUTPUT nCaja, INPUT-OUTPUT nSucursal, INPUT-OUTPUT sIDUsuario).


/***CTRLSOFT****/
{wslibrerias/recaudaidelibWS.i}  /*ISRA OK*/
RUN ParametrosGenerealesIDE NO-ERROR.

{defvarcajas.i}
/***CTRLSOFT****/
ASSIGN 
    nEjercicio = YEAR(TODAY).

FIND FIRST parametrosgenerales WHERE parametrosgenerales.idunion = nUnion AND
    parametrosgenerales.idcajapopular = nCaja AND
    parametrosgenerales.nombre = 'Promo80-20' NO-LOCK NO-ERROR.
IF AVAILABLE(parametrosgenerales) THEN 
    if CAPS(parametrosgenerales.valor) = "NO" then lPromo80-20 = NO. 
    else lPromo80-20 = YES.
FIND FIRST parametrosgenerales WHERE parametrosgenerales.idunion = nUnion AND
    parametrosgenerales.idcajapopular = nCaja AND
    parametrosgenerales.nombre = 'Promo80-20_SoloEmp' NO-LOCK NO-ERROR.
IF AVAILABLE(parametrosgenerales) THEN 
    if CAPS(parametrosgenerales.valor) = "NO" then lPromo80-20_SoloEmp = NO. 
    else lPromo80-20_SoloEmp = YES.

/*if nSucursal >= 22 and nSucursal <= 37 then lPromo80-20_SoloEmp = NO.*/ /* 20140218.- Se deshabilita a Solicitud telef�nica de Alma Cortes */

FIND FIRST parametrosgenerales WHERE parametrosgenerales.idunion = nUnion AND
    parametrosgenerales.idcajapopular = nCaja AND
    parametrosgenerales.nombre = 'MONTOPARTESOCIAL' NO-LOCK NO-ERROR.
IF AVAILABLE(parametrosgenerales) THEN 
DO:
    nMontoCaso_Validar = DECIMAL(parametrosgenerales.valor).
END.

/*INICIO: PROVI-3 */
/*FIND FIRST ParametrosGenerales WHERE ParametrosGenerales.IDUnion = nUnion 
     AND ParametrosGenerales.IDCajaPopular = nCaja
     AND ParametrosGenerales.IDSucursal = nSucursal 
     AND ParametrosGenerales.NombreParametro = "TASAIVA" 
     NO-LOCK NO-ERROR.
IF AVAILABLE(ParametrosGenerales) THEN 
   nTasaIVAComision = decimal(ParametrosGenerales.Valor). */

/*FIN: PROVI-3 */
 
/*--------Variables:Servicios adiconales que generan IVA------------------*/
DEF    VAR lGeneraIVA       AS LOGICAL   INITIAL NO.
DEF    VAR sServGeneraIVA   AS CHAR      INITIAL ''.
DEF    VAR dIVAGenerado     AS DECIMAL   INITIAL 0 FORMAT "->>>,>>>,>>9.99".
DEF    VAR sDescIVAGenerado AS CHAR      INITIAL ''.
DEF    VAR lPremiteMovto    AS LOGICAL   INITIAL YES.
DEF    VAR dImporteMto      AS DECIMAL   INITIAL 0 FORMAT "->>>,>>>,>>9.99".
    
DEFINE VAR stipocalciva     AS CHARACTER INITIAL ''.
/*------------------------------------------------------------------------*/
DEF    VAR nTasaIVA_DF      AS DECIMAL.
FOR FIRST ParametrosGenerales WHERE ParametrosGenerales.IDUnion = nUnion and ParametrosGenerales.IDCajaPopular = nCaja 
    and ParametrosGenerales.IDSucursal = nSucursal and ParametrosGenerales.NombreParametro = "TASAIVA" no-lock:
    nTasaIVA_DF = DECIMAL(ParametrosGenerales.Valor).         
END. /*FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionQseCobra":*/

DEF VAR lPRSM AS LOGICAL INITIAL NO.
/*CTRLSOFT*
{cookie-value.i}
*CTRLSOFT*/
{wslibrerias/despliegamensajesWS.i}
{parametroscajas.i}
/*{generarficha.i}*/
{wslibrerias/asignafolioWS.i}
{mes.i}

/*CTRLSOFT*
IF get-value("RefreshHead")="SI" THEN DO:
    </script><SCRIPT language="JavaScript">    
       window.open("cajahead.r?","encabezado");    
   </script> <script language="SpeedScript">
END.
IF get-value("RefreshHead")="IV" THEN DO:
    </script><SCRIPT language="JavaScript">    
       window.open("inversionhead.r?","encabezado");    
   </script><script language="SpeedScript">
END.
*CTRLSOFT*/

IF INRefreshHead ="SI" THEN 
DO:
    RUN procs/postcajahead.p (INPUT-OUTPUT TABLE ttEncdatcajahead,
                          INPUT-OUTPUT TABLE ttsuccajahead).
/*CTRLSOFT*
</script><SCRIPT language="JavaScript">    
   window.open("cajahead.r?","encabezado");    
</script> <script language="SpeedScript">*CTRLSOFT*/
END.
IF INRefreshHead ="IV" THEN 
DO:
    RUN procs/postinversionhead.p (INPUT-OUTPUT TABLE ttEncdatcajahead,
                                   INPUT-OUTPUT TABLE ttsuccajahead).
/*CTRLSOFT*PENDIENTE                       
 </script><SCRIPT language="JavaScript">    
    window.open("inversionhead.r?","encabezado");    
</script><script language="SpeedScript">
*CTRLSOFT*/
END.

def var srefenciainv as char initial ''.
/*CTRLSOFT*
PROCEDURE output-headers:
ASSIGN sFolioFicha = get-value("FolioFicha":U)
       sServicio = TRIM(CAPS(get-value("Servicio":U)))
       sCajaAsignada= TRIM(CAPS(get-value("CajaAsignada":U)))
       sTransaccion2= cookie-value("cookieTran","Transaccion").
   sCadenaFicha =sCajaAsignada + '$CajaAsignada,' + sTransaccion2 + '$Transaccion,' + sFolioFicha + '$FolioFicha,' + sServicio + '$Servicio,'.

END PROCEDURE.*CTRLSOFT*/
        
    
ASSIGN
    sFolioFicha   = ttEncdatdetficha.INsFolioFicha
    sServicio     = ttEncdatdetficha.INsServicio
    sCajaAsignada = ttEncdatdetficha.INsCajaAsignada
    sTransaccion2 = ttEncdatdetficha.INsTransaccion2
    sCadenaFicha  = ttEncdatdetficha.INsCadenaFicha.

RUN AsignaValores.

RUN CreaArreglosJS.
PROCEDURE EnviaCA:
    ttEncdatdetficha.respuesta = "El Cobro Acumulado, NO ser� v�lido para operaciones combinadas de CHEQUE y EFECTIVO \n por L I D E el Cobro Acumulado queda exclusivo de pagos con CHEQUE \n VER AYUDA EN COBRO ACUMULADO" +
        CHR(10) + "�Est� usted seguro de querer acumular la ficha?".
END PROCEDURE.    
/*CTRLSOFT*
</SCRIPT></head>
<body bgcolor="#C0C0C0" bgproperties="fixed" topmargin="0">
<SCRIPT language="JavaScript">
function EnviaCA(){ 
alert('El Cobro Acumulado, NO ser� v�lido para operaciones combinadas de CHEQUE y EFECTIVO \n por L I D E el Cobro Acumulado queda exclusivo de pagos con CHEQUE \n VER AYUDA EN COBRO ACUMULADO ');
if( ! window.confirm('�Est� usted seguro de querer acumular la ficha?')){}
else {window.open('creafichaydetalle.r?MaintOption=Acumular%20ficha','CuerpoDetalle');} 
}

</script>
<form action="detalleficha.r" method="POST" name="formacja">

<!--WSS
*CTRLSOFT*/
/* 1-LSO PLD */
DEFINE VARIABLE vcServiciosRemesa AS CHARACTER NO-UNDO.
DEFINE VARIABLE vcNumeroSocio     AS INTEGER   NO-UNDO.

/*
ASSIGN
  vcNumeroSocio = INT(cookie-value('cookie',"NumeroSocio")).
*/
vcNumeroSocio  = INTEGER(ttEncdatdetficha.INsNumeroSocio).

FIND LAST PLDConfiguracion WHERE PLDConfiguracion.IDUnion       = nUnion
    AND PLDConfiguracion.IDCajaPopular = nCaja 
    NO-LOCK NO-ERROR.
IF AVAIL PLDConfiguracion THEN 
DO:
    FOR EACH PLDConfiguracionServRem WHERE PLDConfiguracionServRem.IDUnion            = nUnion
        AND PLDConfiguracionServRem.IDCajaPopular      = nCaja
        AND PLDConfiguracionServRem.IDConfiguracionPLD = PLDConfiguracion.IDConfiguracionPLD
        NO-LOCK:
        IF vcServiciosRemesa <> "" THEN
            ASSIGN
                vcServiciosRemesa = vcServiciosRemesa + "," + PLDConfiguracionServRem.IDServicio.
        ELSE
            ASSIGN
                vcServiciosRemesa = PLDConfiguracionServRem.IDServicio.
    END.
END.
/* #1-LSO */
/*CTRLSOFT*
-->

<INPUT type="hidden" name="ServiciosRemesa" id="ServiciosRemesa" value="`vcServiciosRemesa`" />	<!-- 1-LSO PLD -->
<INPUT type="hidden" name="Cajero" value="">
<INPUT type="hidden" name="NomCajero" value="">
<INPUT type="hidden" name="UCSCajero" value="">
</form>

<SCRIPT Language="SpeedScript">
 *CTRLSOFT*/

FIND FIRST CajaAsignada WHERE CajaAsignada.IDUnion = nUnionFicha AND
    CajaAsignada.IDCajaPopular = nCajaFicha AND
    CajaAsignada.IDSucursal = nSucursalFicha AND
    CajaAsignada.IDUsuario = sUsuario NO-LOCK NO-ERROR.
IF NOT ERROR-STATUS:ERROR THEN 
DO:
    ASSIGN 
        nCajaAsignada = CajaAsignada.IDCajaAsignada.
    RUN AsignaFolio (INPUT ttEncdatdetficha.INNuevaFicha,
                     INPUT ttEncdatdetficha.INDespliegaMensaje)   NO-ERROR.
END.
/*CTRLSOFT*
IF (get-value("Servicio")<>"" AND get-value("sPageStatus")="IN") OR
   (get-value("MaintOption1")="Continuar") THEN DO:
       *CTRLSOFT*/
        
IF (sServicio <> "" AND ttEncdatdetficha.sPageStatus = "IN") OR
    (ttEncdatdetficha.INMaintOption1 = "Continuar") THEN 
DO:    
    RUN ContinuaPagina.
END.
/* Tabla temporal utilizada en la forma de captura */
DEF TEMP-TABLE {&tmTABLE} LIKE {&TABLE}.
DEF TEMP-TABLE {&tmTABLE1} LIKE {&TABLE1}.
DEF TEMP-TABLE {&tmTABLE2} LIKE {&TABLE2}.
DEF TEMP-TABLE {&tmTABLE3} LIKE {&TABLE3}.
DEF VAR nArrayLength AS INTEGER INITIAL 7.
DEF VAR TypeField    AS CHAR    EXTENT 7 INITIAL["INT","INT","INT","CHAR","INT","DATE","INT"].
DEF VAR NameField    AS CHAR    EXTENT 7 INITIAL["{&TABLE_}.{&FK1}","{&TABLE_}.{&FK2}","{&TABLE_}.{&FK3}",~
    "{&TABLE_}.{&FK4}","{&TABLE_}.{&FK5}","{&TABLE_}.{&PK2}",~
    "{&TABLE_}.{&PK}"].
DEF VAR ValueField   AS CHAR    EXTENT 7 INITIAL["0","0","0","","0","TODAY","0"].
DEFINE VARIABLE HostURL AS CHARACTER NO-UNDO.
{wslibrerias/ssiwebdefsWS.i}
/*CTRLSOFT*{ssijavascript.i}*CTRLSOFT*/
{wslibrerias/procesoscajaWS.i} /*CTRLSOFT ME LA MANDA ISRA*/
/*{procesosfichaforanea.i}*/
{calculainteresprestamo.i} 
{wslibrerias/generamovtosprestamosWS.i} /*CTRLSOFT PASAR A ISRA*/
{wslibrerias/calculaivaserviciosWS.i} /*CTRLSOFT PASAR A ISRA*/
{existecorreo.i}
{eliminamovtostemp.i}
{wslibrerias/validamovimientoservicioWS.i} /*CTRLSOFT PREGUNTAR ISRAEL*/  

/*-------SUSANA: CALCULO IVA SERVICIOS ADICIONALES--------*/
RUN parametrotipocalculoIVA(input nUnion, input nCaja, OUTPUT vchrMensaje) NO-ERROR.
IF vchrMensaje <> "" THEN 
DO:
    ASSIGN 
        ttEncdatdetficha.respuesta = vchrMensaje.
    RETURN.
END.    
RUN parametrosdescuento (INPUT ttEncdatdetficha.INvalidapago, OUTPUT vchrMensaje) no-error.
IF vchrMensaje <> "" THEN 
DO:
    ASSIGN 
        ttEncdatdetficha.respuesta = vchrMensaje.
    RETURN.
END.    
/*--------------------------------------------------------*/
    
RUN FichasAcumuladas.

/*CTRLSOFT* IF  get-value("MaintOption")="Pago" THEN DO: *CTRLSOFT*/
IF  ttEncdatdetficha.INMaintOption ="Pago" THEN 
DO:
    RUN ValidaPago.
END.
/*CTRLSOFT* IF get-value("MaintOption")="Ir a ficha" THEN *CTRLSOFT*/
IF ttEncdatdetficha.INMaintOption = "Ir a ficha" THEN
    ASSIGN sType = "".
ASSIGN 
    sApp    = "detalleficha"
    sURLApp = sURLInit + sApp + ".r"
    /*CTRLSOFT*  sCallType = GET-USER-FIELD("sCallType")
    sCallerProcedure = GET-VALUE("sCallerProcedure") *CTRLSOFT*/.
       
       
ASSIGN
    sCallType        = ttEncdatdetficha.INsCallType
    sCallerProcedure = ttEncdatdetficha.sCallerProcedure.
/*CTRLSOFT*              
FORM 
    {&tmTABLE}.{&FK11}
    {&tmTABLE}.{&FK12}
    {&tmTABLE}.{&FK13}
    {&tmTABLE}.{&FK14}
    {&tmTABLE}.{&FK15}
    {&tmTABLE}.{&FK16}
    {&tmTABLE}.{&FK18}
    {&tmTABLE}.{&PK}
    {&tmTABLE}.{&PK2}
    {&tmTABLE}.{&FK21}
    {&tmTABLE2}.{&FIELD1FK2}
    {&tmTABLE}.{&FIELD1}
    {&tmTABLE}.{&FIELD2}
    {&tmTABLE}.{&FIELD3}
    {&tmTABLE}.{&FIELD4}
    {&tmTABLE}.{&FIELD5}
    sServicioForaneo
    {&tmTABLE}.{&FIELD6}
    {&tmTABLE}.{&FIELD7}
    {&tmTABLE}.{&FIELD8}
    {&tmTABLE}.{&FIELD9}
    {&tmTABLE}.{&FIELD10}
    {&tmTABLE1}.{&FK11}
    {&tmTABLE1}.{&FK12}
    {&tmTABLE1}.{&FK13}
    {&tmTABLE1}.{&FK14}
    {&tmTABLE1}.{&FK15}
    sFolioFicha    
    WITH FRAME DetailFrame WIDTH 200.
*CTRLSOFT*/    

RUN iniciaproceso.

PROCEDURE iniciaproceso:
    MESSAGE "1.- iniciaproceso"
    VIEW-AS ALERT-BOX.
    IF NUM-ENTRIES(sCallerProcedure," ") > 0 THEN
        ASSIGN sCallerApp = sURLInit + ENTRY(NUM-ENTRIES(sCallerProcedure," "),sCallerProcedure," ") + ".r".
    /*CTRLSOFT*ASSIGN  sType = get-value("sType":U). *CTRLSOFT*/
    ASSIGN  
        sType = ttEncdatdetficha.sType.
    IF bContinua = NO THEN
        ASSIGN sType = "".
    IF sType = "dialog" THEN 
    DO:
        ASSIGN 
            sMode       = ttEncdatdetficha.sMode
            sPageStatus = ttEncdatdetficha.sPageStatus
            MaintOption = TRIM(ttEncdatdetficha.INMaintOption)
            sFilter     = ttEncdatdetficha.sFilter
            sFilterAux  = ttEncdatdetficha.sFilterAux
            Navigate    = ttEncdatdetficha.Navigate
            NavRowid    = ttEncdatdetficha.NavRowid
            LinkRowid   = ttEncdatdetficha.LinkRowid
            sWhere      = ttEncdatdetficha.sWhere
            sWhereAux   = ttEncdatdetficha.sWhereAux
            sPk         = ttEncdatdetficha.sPk
            sOrder      = ttEncdatdetficha.sOrder.
           
    /*CTRLSOFT*
    ASSIGN sMode       = get-value("sMode":U)
           sPageStatus = get-value("sPageStatus":U)
           MaintOption = TRIM(get-value("MaintOption":U))
           sFilter     = GET-VALUE("sFilter":U)
           sFilterAux  = GET-VALUE("sFilterAux":U)
           Navigate    = GET-VALUE("Navigate":U)
           NavRowid    = GET-VALUE("NavRowid")
           LinkRowid   = GET-VALUE("LinkRowid":U)
           sWhere      = GET-VALUE("sWhere":U)
           sWhereAux   = GET-VALUE("sWhereAux":U)
           sPk         = GET-VALUE("sPk":U)
           sOrder      = GET-VALUE("sOrder":U).
           *CTRLSOFT*/
    END.
    ELSE 
    DO:
        ASSIGN 
            sMode       = ""
            sPageStatus = "IN"
            MaintOption = ""
            sFilter     = ""
            sFilterAux  = ""
            Navigate    = ""
            NavRowid    = ""
            LinkRowid   = ""
            sWhere      = ""
            sWhereAux   = ""
            sOrder      = "".
    END.

END PROCEDURE.


/* ************************************* INICIO DEL MAIN-BLOCK ******************************* */
RUN iniciamain.

PROCEDURE iniciamain:
    MESSAGE "2.- iniciamain"
    VIEW-AS ALERT-BOX.

    /*CTRLSOFT* IF get-value("MaintOption")="Cancelar movimientos" THEN DO: *CTRLSOFT*/
    IF ttEncdatdetficha.INMaintOption ="Cancelar movimientos" THEN 
    DO:
        RUN EliminaMovimientos.
    END.
    /*CTRLSOFT* IF sMode="CON" and get-value("Servicio")="" THEN *CTRLSOFT*/
    IF sMode="CON" and ttEncdatdetficha.INServicio ="" THEN
        ASSIGN sType       = ""
            sPageStatus = "IN"
            sMode       = "".
          
    /**CTRLSOFT* IF get-value("MaintOption")="Acumular ficha" OR get-value("MaintOption")="Imprimir" THEN DO: *CTRLSOFT*/
    IF ttEncdatdetficha.INMaintOption ="Acumular ficha" OR ttEncdatdetficha.INMaintOption ="Imprimir" THEN 
    DO:
        RUN creafichaydetalle.r.
    /*CTRLSOFT*
    </script><SCRIPT language="JavaScript">
      window.open("creafichaydetalle.r?MaintOption=`get-value("MaintOption")`","CuerpoDetalle");
      window.close();
    </script> <script language="SpeedScript">
    *CTRLSOFT*/
    END.

    MESSAGE "sType: " sType
    VIEW-AS ALERT-BOX.

    IF sType = "dialog" THEN 
    DO:
        IF sPageStatus = "IN" THEN 
        DO:
            
            CREATE {&tmTABLE}.
            CREATE {&tmTABLE2}.
            MESSAGE ""
            "AVAILABLE {&tmTABLE}: NUEVO " AVAILABLE {&tmTABLE}
            VIEW-AS ALERT-BOX.
            RUN CreaMovtoTempIN.
        END. /* IF PageStatus = "IN" */
        ELSE  IF sPageStatus= "OUT" THEN 
            DO: /* Cuando esta regresando de la detail form */
                RUN CreaMovimientosTemporales NO-ERROR.
                IF ERROR-STATUS:ERROR THEN 
                DO:
                    ASSIGN 
                        ttEncdatdetficha.respuesta = "buscasocio.r?sTransaccion=D&sIrPagina=detalleframed".
                /*CTRLSOFT*
               </script><SCRIPT language="JavaScript">
                 window.open("buscasocio.r?sTransaccion=D&sIrPagina=detalleframed","detalle");
               </script> <script language="SpeedScript">
               *CTRLSOFT*/
                END.
            END. /*  Del ELSE  IF sPageStatus= "OUT .. */
    END. /* Del IF sType = "dialog" ..  */

END PROCEDURE.

/* ********************* CREACION DE LA TABLA PRINCIPAL *************************************** */
IF sType = "" OR (sType = "dialog" AND sPageStatus="OUT") OR~
    (sType="dialog" AND sPageStatus="IN" AND sMode="CON") THEN 
DO:
    MESSAGE "3.- CREACION DE LA TABLA PRINCIPAL" SKIP
        sType="dialog" AND sPageStatus="IN" AND sMode="CON" SKIP
        sType "=dialog AND " sPageStatus "= IN AND" sMode " = CON"
    VIEW-AS ALERT-BOX.
    /*CTRLSOFT*
FORM
    {&TABLE}.{&PK}
    {&TABLE}.{&FK21}
    {&TABLE}.{&FIELD6}
    {&TABLE}.{&FIELD9}
    {&TABLE}.{&FIELD8}
    nSaldoActual       
WITH FRAME ReportFrame WIDTH 200.
*CTRLSOFT*/
    /* Especificaci�n de buffers globalmente compartidos */
    DEFINE {&NEW} SHARED BUFFER {&TABLE_} FOR {&TABLE}.

    ASSIGN 
        TmpUrl = sURLApp.
   
    /*CTRLSOFT*  
    </script>
    <!--WSS
    
        DEF VAR sOnSubmitDistribucion AS CHAR.
        IF sServicio = "CSBC" or sServicio = "RENA" or sServicio = "REMB" or sServicio = "CSEF" THEN 
           sOnSubmitDistribucion = "if(document.getElementById('Importe').value=='')~{document.getElementById('Importe').focus();return false;~}".
    /*	if sServicio = "CAFO" and lPromo80-20_SoloEmp = YES and lPromo80-20 = YES THEN sOnSubmitDistribucion = "if(document.getElementById('Importe').value=='')~{document.getElementById('Importe').focus();return false;~}".*/
    -->
    <form action="`sURLApp`" method="POST" name="forma" id="forma" onSubmit="`sOnSubmitDistribucion`">
    <table  border="0"  bgcolor="`'{&GREY}'`"  WIDTH="`'{&TOTAL-WIDTH}'`" HEIGTH="80%" cellspacing="0" cellpadding="1">
    <tr><th colspan="7">
        <p style="background-color: #000080; color: #00FFFF; border-style: outset; border-width: 1">
        <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">Detalle ficha de Depositos</font></p>
      </th></tr>
       <tr valign="middle">
        <!-- Display Column Headings -->
        <th align="centered" style="`'{&OUTSET}'`">
            <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">`'{&LABEL-PK}'`</font></th>
        <th WIDTH="`'{&WIDTH-FK6}'`" align="centered" style="`'{&OUTSET}'`">
            <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">`'{&LABEL-FK6}'`</font></a></th>
        <th WIDTH="`'{&WIDTH-FIELD6}'`" align="centered" style="`'{&OUTSET}'`"><div id="divConcepto">
           <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">`'{&LABEL-FIELD6}'`</font></a></div><div id="divNombre" style="visibility:hidden"><font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">Nombre</font></a></div></th>
        <th WIDTH="`'{&WIDTH-FIELD9}'`" align="centered" style="`'{&OUTSET}'`">
           <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">`'{&LABEL-FIELD9}'`</font></a></th>
        <th WIDTH="`'{&WIDTH-FIELD8}'`" align="centered" style="`'{&OUTSET}'`">
            <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">Importe</font></a></th>
        <th WIDTH="`'{&WIDTH-FIELD6}'`" align="centered" style="`'{&OUTSET}'`">
            <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">Saldo actual</font></a></th>
        <!--WSS IF bPrev THEN DO: -->
        <td width="`'{&WIDTH-ARROW}'`" align ="center" width="10" border="2" style="`'{&OUTSET}'`">
            <a href="`TmpUrl%20+%20BuildURL('','','','Prev','',sWhere,sWhereAux,sFilter,sFilterAux,sOrder,sCallerProcedure,DelimiterField)`"
                onmouseover="window.status='Ver los anteriores `{&Result-Rows}` registros';return true"
                onmouseout="window.status='';return true"><img src="/gifs/prev-ver.gif" border="0" width="16"
                height="16" alt="Ver los anteriores `{&Result-Rows}` registros"></a></td>
        <!--WSS END.
            ELSE  DO: -->
        <td WIDTH="`'{&WIDTH-ARROW}'`" style="`'{&OUTSET}'`">&nbsp;</td>
        <!--WSS END. -->
    <script language="SpeedScript">
        /* Output the number of requested Rows to "Result List" */
    *CTRLSOFT*/
    /*************************************************************************/    
    /**************************PAGO*TERCEROS**********************************/
    /*************************************************************************/
    /*CTRLSOFT*           
    nUnionSocioServicio    = INT(cookie-value('cookie',"UnionSocio":U)).
    nCajaSocioServicio     = INT(cookie-value('cookie',"CajaSocio":U)).
    nSucursalSocioServicio = INT(cookie-value('cookie',"SucursalSocio":U)).
    nIDSocioServicio       = INT(cookie-value('cookie',"NumeroSocio")).
    *CTRLSOFT*/
    nUnionSocioServicio    = INT(ttEncdatdetficha.INsUnionSocio).
    nCajaSocioServicio     = INT(ttEncdatdetficha.INsCajaSocio).
    nSucursalSocioServicio = INT(ttEncdatdetficha.INsSucursalSocio).
    nIDSocioServicio       = INT(ttEncdatdetficha.INsNumeroSocio).

    RUN PagoTerceros.
    
PROCEDURE PagoTerceros:
    FIND LAST DetalleFichaTemp WHERE DetalleFichaTemp.IDUnion           = nUnionFicha 
        AND DetalleFichaTemp.IDCajaPopular     = nCajaFicha 
        AND DetalleFichaTemp.IDSucursal        = nSucursalFicha 
        AND DetalleFichaTemp.Ejercicio         = nEjercicio 
        AND DetalleFichaTemp.IDCajaAsignada    = nCajaAsignada 
        AND DetalleFichaTemp.IDTipoTransaccion = sTransaccion 
        AND DetalleFichaTemp.IDFicha           = nFolioFicha  
        NO-ERROR.
    IF NOT ERROR-STATUS:ERROR THEN 
    DO:
        nUltimoDetalle     = DetalleFichaTemp.IDDetalleFicha.
        nPagoSerReferencia = DetalleFichaTemp.Referencia.
        nCajaAsignada      = DetalleFichaTemp.IDCajaAsignada.
        sTransaccion       = DetalleFichaTemp.IDTipoTransaccion.  
        nFolioFicha        = DetalleFichaTemp.IDFicha. 
        sIDSerForaneoSer   = DetalleFichaTemp.IDServicioForaneo. 
        /*************************************LUZ*AGUA************************************	
            nAbonoNoPag = 0.
            FOR EACH Prestamo WHERE Prestamo.IDUnionSocio = INT(cookie-value('cookie',"UnionSocio":U))
                              AND Prestamo.IDCajaPopularSocio = INT(cookie-value('cookie',"CajaSocio":U))
                              AND Prestamo.IDSucursalSocio = INT(cookie-value('cookie',"SucursalSocio":U))
                              AND Prestamo.IDSocio = INT(cookie-value('cookie',"NumeroSocio")) NO-LOCK:	
             FOR EACH PlanPago OF Prestamo NO-LOCK:	
                 IF FechaVencimientoAbono < TODAY AND AbonoPagado = NO AND Prestamo.IDEstadoPrestamo <> "CANC" AND Prestamo.IDEstadoPrestamo <> "PAGA" 
                    AND Prestamo.IDEstadoPrestamo <> "NOEN" THEN 
                 DO:
                    nAbonoNoPag = nAbonoNoPag + 1.
                 END. /*IF FechaVencimientoAbono < TODAY THEN */	
             END. /*FOR EACH PlanPago OF Prestamo:*/	
            END. /*FOR EACH Prestamo WHERE Prestamo.IDUnionSocio = cookie-value('cookie',"UnionSocio":U)*/
        	
           IF (DetalleFichaTemp.IDSocio = 0 AND (DetalleFichaTemp.IDServicio = "SLUZ" OR DetalleFichaTemp.IDServicio = "SAGU"))
               OR (nAbonoNoPag > 0 AND (DetalleFichaTemp.IDServicio = "SLUZ" OR DetalleFichaTemp.IDServicio = "SAGU")) THEN
           DO:
              FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionQseCobra"
                  NO-LOCK: 
                               ComisionQseCobra = ParametrosGenerales.Valor.         
                               ni = (DECIMAL(ComisionQseCobra) / 1.15) * .15.       
              END. /*FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionQseCobra":*/
              
              FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionNoSocio"
                                             AND ParametrosGenerales.Valor = "SI" NO-LOCK:                                                      						         
                               DO nCreaPT = 1 TO 2:
                                  ASSIGN  sCreaDetallesServicio[1] = "COSE"
                                          sCreaDetallesServicio[2] = "IVAC"
                                          sCreaDetallesServicio[3] = STRING(DECIMAL(ComisionQseCobra) - ni)
                                          sCreaDetallesServicio[4] = STRING((DECIMAL(ComisionQseCobra) / 1.15) * .15).
                                  CREATE  DetalleFichaTemp.
                                  ASSIGN  DetalleFichaTemp.IDUnion = nUnionFicha
                                          DetalleFichaTemp.IDCajaPopular = nCajaFicha
                                          DetalleFichaTemp.IDSucursal = nSucursalFicha
                                          DetalleFichaTemp.Ejercicio = nEjercicio 
                                          DetalleFichaTemp.IDCajaAsignada = nCajaAsignada 
                                          DetalleFichaTemp.IDTipoTransaccion = sTransaccion 
                                          DetalleFichaTemp.IDFicha = nFolioFicha    
                                          DetalleFichaTemp.IDDetalleFicha   = nUltimoDetalle + nCreaPT            
                                          DetalleFichaTemp.IDServicio = sCreaDetallesServicio[nCreaPT] 
                                          DetalleFichaTemp.FechaFicha  = TODAY 
                                          DetalleFichaTemp.IDUnionSocio = nUnionSocioServicio
                                          DetalleFichaTemp.IDCajaPopularSocio = nCajaSocioServicio
                                          DetalleFichaTemp.IDSucursalSocio = nSucursalSocioServicio
                                          DetalleFichaTemp.IDSocio = nIDSocioServicio
                                          DetalleFichaTemp.IDServicioForaneo  = sIDSerForaneoSer.
                                          IF nCreaPT = 1 THEN ASSIGN DetalleFichaTemp.Concepto = "Comision x Pago de Servicio".
                                          ELSE ASSIGN DetalleFichaTemp.Concepto = "IVA x Comision x Pago de Servicio".
                                   ASSIGN DetalleFichaTemp.cargo = 0
                                          DetalleFichaTemp.abono = DECIMAL(sCreaDetallesServicio[nCreaPT + 2])
                                          DetalleFichaTemp.Referencia = nPagoSerReferencia. 
                               END. /*DO nCreaPT = 1 TO 2:*/                               
              END. /*FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionSocio" :*/  
           END. /*IF DetalleFichaTemp.IDSocio = 00 AND DetalleFichaTemp.IDServicio = "SLUZ" THEN*/                               
        ***************************************************************************/              
        FIND FIRST SerPagoTerceros WHERE SerPagoTerceros.IDServicioVinculado = DetalleFichaTemp.IDServicio 
            NO-LOCK NO-ERROR.
                 
        FOR EACH bfSerPagoTerceros WHERE bfSerPagoTerceros.Referencia = "IVATrasladado" NO-LOCK:
                                   
            IF bfSerPagoTerceros.Referencia = "IVATrasladado" THEN
                sIVATrasladado  = bfSerPagoTerceros.IDServicio.
           
        END. /*FOR EACH bfSerPagoTerceros WHERE bfSerPagoTerceros.Referencia = "IVATrasladado"*/
        sIVATrasladados[1]    = STRING((DetalleFichaTemp.abono / 1.15) * .15).
        sIVATrasladados[12]   = "Pago_Terceros".
        RUN TipoSociosComision. /*PROCEDIMIENTO EN EL CUAL SE BUSCAN LAS COMISIONES POR TIPO DE SOCIO*/          
        RUN CreaDetallesPagTer. /*SE AGREGAN LOS DETALLES A LA TABLA TEMPORAL PARA PAGO A TERCEROS*/         
    END. /*IF NOT ERROR-STATUS:ERROR THEN*/ 
END PROCEDURE.

MESSAGE "4.- TERMINA" SKIP
        "nUnionFicha: " nUnionFicha SKIP
        
        " nCajaFicha: " nCajaFicha
        " nSucursalFicha : " nSucursalFicha
        " nEjercicio : " nEjercicio
        " nCajaAsignada : " nCajaAsignada
        " sTransaccion : " sTransaccion
        " nFolioFicha : " nFolioFicha
VIEW-AS ALERT-BOX.

    /******************************TERMINA************************************/    
    /**************************PAGO*TERCEROS**********************************/
    /*************************************************************************/
    FIND LAST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
        {&TABLE}.{&FK12} = nCajaFicha AND
        {&TABLE}.{&FK13} = nSucursalFicha AND
        {&TABLE}.{&FK16} = nEjercicio AND
        {&TABLE}.{&FK18} = nCajaAsignada AND
        {&TABLE}.{&FK14} = sTransaccion AND
        {&TABLE}.{&FK15} = nFolioFicha NO-LOCK NO-ERROR.
    IF NOT ERROR-STATUS:ERROR AND AVAILABLE {&TABLE} THEN
        ASSIGN nDetalleFicha = {&TABLE}.{&PK} + 1 NO-ERROR.
    ELSE
        ASSIGN nDetalleFicha = 1 NO-ERROR.
   
    FOR EACH DetalleFichaTemp WHERE DetalleFichaTemp.IDUnion           = nUnionFicha AND
        DetalleFichaTemp.IDCajaPopular     = nCajaFicha AND
        DetalleFichaTemp.IDSucursal        = nSucursalFicha AND
        DetalleFichaTemp.Ejercicio         = nEjercicio AND
        DetalleFichaTemp.IDCajaAsignada    = nCajaAsignada AND
        DetalleFichaTemp.IDTipoTransaccion = sTransaccion AND
        DetalleFichaTemp.IDFicha           = nFolioFicha NO-LOCK:

        ASSIGN 
            i-count = i-count + 1.
        FIND Servicio WHERE Servicio.IDServicio=DetalleFichaTemp.IDServicio NO-LOCK NO-ERROR.
        IF AVAILABLE(Servicio) THEN 
        DO:
            IF Servicio.Clasificacion="ACR" THEN
                ASSIGN nSaldoActual = {&TABLE}.{&FIELD8} + {&TABLE}.{&FIELD9}.
            ELSE
                IF {&TABLE}.{&FIELD9}=0 THEN
                    ASSIGN nSaldoActual = 0.
                ELSE
                    ASSIGN nSaldoActual = {&TABLE}.{&FIELD9} - {&TABLE}.{&FIELD8}.
        END.
       
        vintIdDet = vintIdDet + 1. 
        CREATE ttDetdatdetficha.
        ASSIGN
            ttDetdatdetficha.INid    = ttEncdatdetficha.INid
            ttDetdatdetficha.INidDet = vintIdDet.
        /*/**/ ttDetdatdetficha.IDDetalleFicha      = DetalleFichaTemp.IDDetalleFicha       /*{&TABLE}.{&PK}- Mov*/
        /**/ ttDetdatdetficha.IDServicio          = STRING(DetalleFichaTemp.IDServicio)   /*{&TABLE}.{&FK21}- Servicio*/
             ttDetdatdetficha.IDServicioForaneo   = DetalleFichaTemp.IDServicioForaneo    /*{&FIELD5}- */
        /**/ ttDetdatdetficha.Concepto            = DetalleFichaTemp.Concepto             /*{&FIELD6}- Comcepto*/
        /**/ ttDetdatdetficha.SaldoAnterior       = DetalleFichaTemp.SaldoAnterior        /*{&FIELD9}- Saldo Anterior*/
             ttDetdatdetficha.Cargo               = DetalleFichaTemp.Cargo                /*{&FIELD7}- */
             ttDetdatdetficha.Abono               = DetalleFichaTemp.Abono                /*{&FIELD8}- IMPORTE*/
        /**/ ttDetdatdetficha.nSaldoActual        = nSaldoActual                          /*         -SaldoActual*/
        .
        */
           
        /*CTRLSOFT*
        DISPLAY
            {&TABLE}.{&PK}
            {&TABLE}.{&FK21}
            {&TABLE}.{&FIELD5}
            {&TABLE}.{&FIELD6}
            {&TABLE}.{&FIELD9}
            {&TABLE}.{&FIELD7}
            {&TABLE}.{&FIELD8}
            nSaldoActual
         WITH FRAME ReportFrame.
        </script>
        <tr bgcolor="`'{&BLANK}'`" valign="top">
          <td style="`'{&DETAIL-LEFT}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
          <!--WSS
          *CTRLSOFT*/ 
        FIND Servicio WHERE Servicio.IDServicio = DetalleFichaTemp.IDServicio NO-LOCK NO-ERROR.
        IF AVAILABLE (Servicio) THEN 
            sTipoServicio = Servicio.IDTipoServicio.
        /********************* Calculo IVA *********************/
        /*         IF sTipoServicio = "PRE" or sTipoServicio = "INT" or sTipoServicio = "INV" or sTipoServicio="DEU" THEN DO:*/
        /*******************************************************/
        IF sTipoServicio = "PRE" or sTipoServicio = "IMP" or sTipoServicio = "INT" or sTipoServicio = "INV" or sTipoServicio="DEU" THEN 
        DO:            
            CASE sTipoServicio:
                WHEN "INV" THEN 
                    DO:
                        ASSIGN
                            ttDetdatdetficha.hRef           = sURLInit + "depositoinversiones.r"
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999"). 
                    /*CTRLSOFT* -->
                    <a href="`sURLInit + 'depositoinversiones.r'`"
                            onmouseover="window.status='Mantenimiento del registro actual';return true"
                            onmouseout="window.status='';return true">
                          `STRING({&TABLE}.{&PK},"99999")`</a></font></td>
                    <!--WSS 
                    *CTRLSOFT*/
                    /********************* Calculo IVA *********************/
                    END.
                WHEN "IMP" THEN 
                    DO:
                        ASSIGN
                            ttDetdatdetficha.hRef           = ""
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                    /*CTRLSOFT* -->
                    `STRING({&TABLE}.{&PK},"99999")`
                    <!--WSS *CTRLSOFT*/
                    /*******************************************************/
                    END.
                WHEN "PRE" THEN 
                    DO:
                        ASSIGN
                            ttDetdatdetficha.hRef           = sURLInit + "datosprestamoframe.r?sType=dialog&sMode=MTO&sPageStatus=IN&Servicio=" + STRING({&TABLE}.{&FK21}) + 
                                                      "&DetalleFicha=" + STRING({&TABLE}.{&PK}) + "&Importe=" + STRING({&TABLE}.{&FIELD8})
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                    /*CTRLSOFT* -->
                <a href="`sURLInit + 'datosprestamoframe.r'`?sType=dialog&sMode=MTO&sPageStatus=IN&Servicio=`{&TABLE}.{&FK21}`&DetalleFicha=`{&TABLE}.{&PK}`&Importe=`{&TABLE}.{&FIELD8}`"
                        onmouseover="window.status='Mantenimiento del registro actual';return true"
                        onmouseout="window.status='';return true">
                      `STRING({&TABLE}.{&PK},"999999")`</a>
                <!--WSS *CTRLSOFT*/
                    END.
                WHEN "INT" THEN 
                    DO: 
                        ASSIGN
                            ttDetdatdetficha.hRef           = ""
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                    /*CTRLSOFT*-->
                    `STRING({&TABLE}.{&PK},"999999")`
                    <!--WSS*CTRLSOFT*/
                    END.
                WHEN "DEU" THEN 
                    DO:
                        IF {&TABLE}.{&FK21} = sServicioChequeDevuelto OR {&TABLE}.{&FK21} = sServicioComisionBancaria OR {&TABLE}.{&FK21} = sServicioIndemnizacion THEN 
                        DO:
                            ASSIGN
                                ttDetdatdetficha.hRef           = sURLInit + "pagochequedevuelto.r?sType=dialog&sMode=MTO&sPageStatus=IN&Servicio=" +
                                                          STRING({&TABLE}.{&FK21}) + "&DetalleFicha=" + STRING({&TABLE}.{&PK}) + "&Importe=" + STRING({&TABLE}.{&FIELD8})
                                ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                        /*CTRLSOFT*-->
                        <a href="`sURLInit + 'pagochequedevuelto.r'`?sType=dialog&sMode=MTO&sPageStatus=IN&Servicio=`{&TABLE}.{&FK21}`&DetalleFicha=`{&TABLE}.{&PK}`&Importe=`{&TABLE}.{&FIELD8}`"
                              onmouseover="window.status='Mantenimiento del registro actual';return true"
                              onmouseout="window.status='';return true">
                            `STRING({&TABLE}.{&PK},"999999")`</a>
                        <!--WSS *CTRLSOFT*/
                        END.
                        ELSE 
                        DO:
                            IF {&TABLE}.{&FK21} = sServicioInteresDocumentado THEN 
                            DO:
                                ASSIGN
                                    ttDetdatdetficha.hRef           = "cobranzapagareinteresdocumentado.r?DetalleFicha=" + STRING({&TABLE}.{&PK}) + "&Union=" + sUnionSocio +
                                                              "&Caja=" + sCajaSocio + "&Sucursal=" + sSucursalSocio + "&Socio=" + 
                                                              sNumeroSocio + "&Importe=" + STRING({&TABLE}.{&FIELD8})
                                    ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                            /*CTRLSOFT* -->
                               <a href="cobranzapagareinteresdocumentado.r?DetalleFicha=`{&TABLE}.{&PK}`&Union=`sUnionSocio`&Caja=`sCajaSocio`&Sucursal=`sSucursalSocio`&Socio=`sNumeroSocio`&Importe=`{&TABLE}.{&FIELD8}`"
                                  onmouseover="window.status='Mantenimiento del registro actual';return true"
                                  onmouseout="window.status='';return true">
                                  `STRING({&TABLE}.{&PK},"999999")`</a>
                            <!--WSS *CTRLSOFT*/
                            END.
                            ELSE 
                            DO: 
                                ASSIGN
                                    ttDetdatdetficha.hRef           = sURLApp + "?sType=dialog&sMode=MTO&sPageStatus=IN&sServicio=" + STRING(DetalleFichaTemp.IDServicio) + "&DetalleFicha=" + STRING({&TABLE}.{&PK})
                                    ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                            /*CTRLSOFT* -->
                               <a href="`sURLApp`?sType=dialog&sMode=MTO&sPageStatus=IN&sServicio=`DetalleFichaTemp.IDServicio`&DetalleFicha=`{&TABLE}.{&PK}`"
                                  onmouseover="window.status='Mantenimiento del registro actual *$*';return true"
                                  onmouseout="window.status='';return true">
                                 `STRING({&TABLE}.{&PK},"999999")`</a>                        
                            <!--WSS *CTRLSOFT*/
                            END.
                        END.
                    END.
            END CASE.
        END.
        ELSE 
        DO: /*CTRLSOFT* -->
<!--WSS *CTRLSOFT*/
            if DetalleFichaTemp.IDServicio <> "ASEF" and DetalleFichaTemp.IDServicio <> "RENA" and DetalleFichaTemp.IDServicio <> "CRRN" and DetalleFichaTemp.IDServicio <> "IVRN"
                and DetalleFichaTemp.IDServicio <> "REMB" and DetalleFichaTemp.IDServicio <> "CRRI" and DetalleFichaTemp.IDServicio <> "IVRI" 
                and DetalleFichaTemp.IDServicio <> "CSEF" and DetalleFichaTemp.IDServicio <> "CCSE" and DetalleFichaTemp.IDServicio <> "ICCS" THEN
            do:
                if sServicio = "CAFO" then
                do:
                    if lPromo80-20 = YES then
                    do:
                        ASSIGN
                            ttDetdatdetficha.hRef           = ""
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                    /*CTRLSOFT* -->
               `STRING({&TABLE}.{&PK},"99999")`
<!--WSS *CTRLSOFT*/	
                    end.
                    else
                    do: 
                        ASSIGN
                            ttDetdatdetficha.hRef           = ""
                            ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                    /*CTRLSOFT* -->
              <a href="`sURLApp`?sType=dialog&sMode=MTO&sPageStatus=IN&sServicio=`DetalleFichaTemp.IDServicio`&DetalleFicha=`{&TABLE}.{&PK}`"
                     onmouseover="window.status='Mantenimiento del registro actual';return true"
                     onmouseout="window.status='';return true">
                    `STRING({&TABLE}.{&PK},"99999")`</a>
<!--WSS *CTRLSOFT*/	
                    end.
                end.
                else
                do:
                    ASSIGN
                        ttDetdatdetficha.hRef           = ""
                        ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
                /*CTRLSOFT* -->
          <a href="`sURLApp`?sType=dialog&sMode=MTO&sPageStatus=IN&sServicio=`DetalleFichaTemp.IDServicio`&DetalleFicha=`{&TABLE}.{&PK}`"
                 onmouseover="window.status='Mantenimiento del registro actual';return true"
                 onmouseout="window.status='';return true">
                `STRING({&TABLE}.{&PK},"99999")`</a>
<!--WSS *CTRLSOFT*/
                end.
            end.
            else
            do: 
                ASSIGN
                    ttDetdatdetficha.hRef           = ""
                    ttDetdatdetficha.IDDetalleFicha = STRING(DetalleFichaTemp.IDDetalleFicha,"99999").
            /*CTRLSOFT*	-->
                      `STRING({&TABLE}.{&PK},"99999")`
  <!--WSS 	*CTRLSOFT*/
            end. /*CTRLSOFT* -->
         <!--WSS *CTRLSOFT*/
        END. 
        ASSIGN
            ttDetdatdetficha.IDServicio = STRING(DetalleFichaTemp.IDServicio).
        
        /*CTRLSOFT*-->
       <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
               `{&TABLE}.{&FK21}`</font></td>
       <td style="`'{&DETAIL}'`">
       <!--WSS *CTRLSOFT*/
        lPRSM = NO.
        IF {&TABLE}.{&FK21} = 'PRCM' THEN 
        DO:
            FIND FIRST Prestamo WHERE Prestamo.IDUnion = INT(SUBSTRING({&TABLE}.{&FIELD10},5,2))
                AND Prestamo.IDCajaPopular = INT(SUBSTRING({&TABLE}.{&FIELD10},7,2))
                AND Prestamo.IDSucursal = INT(SUBSTRING({&TABLE}.{&FIELD10},9,2))
                AND Prestamo.IDPrestamo = INT(SUBSTRING({&TABLE}.{&FIELD10},11,6)) NO-ERROR.
            IF AVAILABLE(Prestamo) THEN
            DO:
                IF Prestamo.IDTipoPrestamo = "PRSM" THEN
                DO:
                    lPRSM = YES.
                END.
            END. /* IF AVAILABLE(Prestamo)THEN */
        END.

        IF lPRSM = YES THEN
        DO:
        /*CTRLSOFT*PENDIENTE*
        -->
        <textarea id="txtConcp" name="txtConcp" style="border:0px; width:100%; font-size:10px; font-family:Arial, Helvetica, sans-serif; " onChange=" var conc = this.value;  capturaValor('`DetalleFichaTemp.IDUnion`','`DetalleFichaTemp.IDCajaPopular`','`DetalleFichaTemp.IDSucursal`','`DetalleFichaTemp.Ejercicio`','`DetalleFichaTemp.IDCajaAsignada`','`DetalleFichaTemp.IDTipoTransaccion`','`DetalleFichaTemp.IDFicha`','`DetalleFichaTemp.IDDetalleFicha`',conc);" > `{&TABLE}.{&FIELD6}` </textarea>
        <!--WSS
        *CTRLSOFT*/
        END.
        ELSE
        DO:
            ASSIGN 
                ttDetdatdetficha.Concepto = DetalleFichaTemp.Concepto.             /*{&FIELD6}*/  
                
           
        /*CTRLSOFT* -->
        <font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`"> `{&TABLE}.{&FIELD6}` </font>
        <!--WSS *CTRLSOFT*/
        END.
        /*CTRLSOFT* --></td>
        <!--WSS *CTRLSOFT*/

            
        IF {&TABLE}.{&FIELD7} = 0 THEN 
        DO:
            ASSIGN
                ttDetdatdetficha.SaldoAnterior = STRING(DetalleFichaTemp.SaldoAnterior,"->,>>>,>>>,>>9.99") /*{&FIELD9}*/
                ttDetdatdetficha.Importe       = STRING(DetalleFichaTemp.Abono,"->,>>>,>>>,>>9.99")         /*{&FIELD8}*/
                ttDetdatdetficha.nSaldoActual  = STRING(nSaldoActual,"->,>>>,>>>,>>9.99").
            
        /*CTRLSOFT* -->
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                `STRING({&TABLE}.{&FIELD9},"->,>>>,>>>,>>9.99")`</font></td>
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                `STRING({&TABLE}.{&FIELD8},"->,>>>,>>>,>>9.99")`</font></td>
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                `STRING(nSaldoActual,"->,>>>,>>>,>>9.99")`</font></td>
        <!--WSS *CTRLSOFT*/
        END.
        ELSE 
        DO:
            ASSIGN
                ttDetdatdetficha.SaldoAnterior = "0.00" /*{&FIELD9}*/
                ttDetdatdetficha.Importe       = STRING(DetalleFichaTemp.Cargo,"->,>>>,>>>,>>9.99")         /*{&FIELD8}*/
                ttDetdatdetficha.nSaldoActual  = "0.00". 
                 
        /*CTRLSOFT* -->
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
            0.00</font></td>
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`" color="#DC2A35">
            `STRING({&TABLE}.{&FIELD7},"->,>>>,>>>,>>9.99")`</font></td>
        <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
            0.00</font></td>
    <!--WSS*CTRLSOFT*/ 
        END.
    /*CTRLSOFT*
    IF i-count = 1 THEN DO: -->
      <td bgcolor="`'{&GREY}'`" rowspan="11" style="`'{&OUTSET}'`">
         <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`" color="`'{&GREY}'`">
         &nbsp</font></td>
    <!--WSS END.
    *CTRLSOFT*/
    END. /*... del FOR EACH*/
    
    vintidDet = 0.
    
    IF i-count = 0 THEN
        ASSIGN bTablaVacia = YES.
        
MESSAGE "bTablaVacia: " bTablaVacia SKIP
    "i-count1 < {&Result-Rows}: " i-count1 < {&Result-Rows} SKIP
    "i-count1: " i-count1 SKIP
    "{&Result-Rows}: " {&Result-Rows} SKIP
    "sMode=CON" sMode /*FMR*/
VIEW-AS ALERT-BOX.    

    IF i-count1 < {&Result-Rows} THEN 
    DO:
        IF sMode="CON" AND sPageStatus="IN" THEN 
        DO:
            vintidDet = vintidDet + 1.
            CREATE ttDetdatdetficha.
            ASSIGN
                ttDetdatdetficha.INid              = ttEncdatdetficha.INid
                ttDetdatdetficha.INidDet           = vintidDet.
      
            ASSIGN   
                /**/ 
                ttDetdatdetficha.IDDetalleFicha = STRING(tmDetalleFichaTemp.IDDetalleFicha,"99999") /*{&TABLE}.{&PK}- Mov*/
                /**/ 
                ttDetdatdetficha.IDServicio     = TRIM(tmDetalleFichaTemp.IDServicio)   /*{&TABLE}.{&FK21}- Servicio*/
                /**/ 
                ttDetdatdetficha.Concepto       = TRIM(CAPS(tmDetalleFichaTemp.Concepto))             /*{&FIELD6}- Comcepto*/
                /**/ 
                ttDetdatdetficha.SaldoAnterior  = STRING(tmDetalleFichaTemp.SaldoAnterior,"->,>>>,>>>,>>9.99").        /*{&FIELD9}- Saldo Anterior*/
           
           
            /*CTRLSOFT* -->
          <tr><td style="`'{&DETAIL-LEFT}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
               `STRING(TRIM(CAPS(html-encode({&tmTABLE}.{&PK}:SCREEN-VALUE IN FRAME DetailFrame))),"99999")`
                <input type="hidden" name="DetalleFicha"
                       value="`TRIM(CAPS(html-encode({&tmTABLE}.{&PK}:SCREEN-VALUE IN FRAME DetailFrame)))`" width="5">
              </font></td>
              <td style="`'{&DETAIL-LEFT}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
               `TRIM(CAPS(html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame)))`
               *CTRLSOFT*/
               /*CTRLSOFT*
                <input type="hidden" name="Servicio"
                       value="`TRIM(CAPS(html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame)))`" width="5">
                *CTRLSOFT*/       
                ASSIGN
                    ttEncdatdetficha.INsFolioFicha = TRIM(CAPS(tmDetalleFichaTemp.IDServicio)).       
              /*CTRLSOFT*
              </font></td>
              <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                <input type="hidden" name="Concepto"
                      value="`TRIM(CAPS(html-encode({&tmTABLE}.{&FIELD6}:SCREEN-VALUE IN FRAME DetailFrame)))`" >
                             `TRIM(CAPS(html-encode({&tmTABLE}.{&FIELD6}:SCREEN-VALUE IN FRAME DetailFrame)))`</font></td>
              <td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                <input type="hidden" name="`'{&tmTABLE}.{&FIELD9}'`"
                       value="`html-encode({&tmTABLE}.{&FIELD9}:SCREEN-VALUE IN FRAME DetailFrame)`" size="14">
                 `string({&tmTABLE}.{&FIELD9},"->,>>>,>>>,>>9.99")`</font></td>
              <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
              <!--WSS *CTRLSOFT*/ 
             
            IF sMode = "MTO" THEN 
            DO:
                ASSIGN
                    ttDetdatdetficha.Importe = STRING(tmDetalleFichaTemp.Abono).                /*{&FIELD8}- IMPORTE*/
           
            /*CTRLSOFT* -->
             <input type="text" name="Importe" id="Importe"
                    value="`html-encode({&tmTABLE}.{&FIELD8}:SCREEN-VALUE IN FRAME DetailFrame)`" size="14" >
           <!--WSS *CTRLSOFT*/
            END.
            else 
            do:
                /*CTRLSOFT*sConceptoFODE = TRIM(CAPS(html-encode({&tmTABLE}.{&FIELD6}:SCREEN-VALUE IN FRAME DetailFrame))).*CTRLSOFT*/
                sConceptoFODE = TRIM(CAPS(tmDetalleFichaTemp.{&FIELD6})).
                /*CTRLSOFT*if TRIM(CAPS(html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame))) = "FODE" then*CTRLSOFT*/
                if TRIM(CAPS({&tmTABLE}.{&FK21})) = "FODE" then
                do:
                    for each PrestamoPagoFODE where PrestamoPagoFODE.IDUnion = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 2,2)) 
                        and PrestamoPagoFODE.IDCajaPopular = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 4,2))
                        and PrestamoPagoFODE.IDSucursal = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 6,2))
                        and PrestamoPagoFODE.IDPrestamo = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 9,6))
                        and PrestamoPagoFODE.Aplicada = NO no-lock:
                        nImporteFODE = nImporteFODE + PrestamoPagoFODE.Importe.
                    end.
                end.
                /**CTRLSOFT*if TRIM(CAPS(html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame))) = "COOE" then*CTRLSOFT*/
                if TRIM(CAPS(tmDetalleFichaTemp.{&FK21})) = "COOE" then
                do:
                    for each PrestamoPagoComision where PrestamoPagoComision.IDUnion = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 2,2)) 
                        and PrestamoPagoComision.IDCajaPopular = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 4,2))
                        and PrestamoPagoComision.IDSucursal = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 6,2))
                        and PrestamoPagoComision.IDPrestamo = INT(SUBSTRING(sConceptoFODE,INDEX(sConceptoFODE,'PR') + 9,6))
                        and PrestamoPagoComision.Aplicada = NO no-lock:
                        nImporteFODE = nImporteFODE + PrestamoPagoComision.ImporteFichaTemp.
                    end.
                end.
                if nImporteFODE > 0 then sImporteFODE = string(nImporteFODE).
                /*CTRLSOFT* -->
                <!--WSS*CTRLSOFT*/
                IF sServicio <> "CSBC" and sServicio <> "RENA" and sServicio <> "REMB" and sServicio <> "CSEF" THEN
                DO:
                    if sServicio = "CTMD" or sServicio = "CTIN" then
                    do:
                        nImporteIntCAST = 0.
           
                        FOR FIRST SaldoSocio WHERE SaldoSocio.IDUnion = INT(ttEncdatdetficha.INsUnionSocio) /*CTRLSOFT*INT(cookie-value('cookie',"UnionSocio":U))*CTRLSOFT*/
                            AND SaldoSocio.IDCajaPopular = INT(ttEncdatdetficha.INsCajaSocio) /*CTRLSOFT*INT(cookie-value('cookie',"CajaSocio":U))*CTRLSOFT*/
                            AND SaldoSocio.IDSucursal = INT(ttEncdatdetficha.INsSucursalSocio) /*CTRLSOFT*INT(cookie-value('cookie',"SucursalSocio":U))*CTRLSOFT*/
                            AND SaldoSocio.IDSocio = INT(ttEncdatdetficha.INsNumeroSocio) /*CTRLSOFT*INT(cookie-value('cookie',"NumeroSocio")) *CTRLSOFT*/
                            AND SaldoSocio.Anio = YEAR(TODAY)
                            AND SaldoSocio.IDServicio = sServicio
                            NO-LOCK:
                            nImporteIntCAST = ROUND(SaldoSocio.Saldo * (1 + (nTasaIVA_DF / 100)), 2).
                        END.
                        ASSIGN
                            ttDetdatdetficha.Importe = STRING(nImporteIntCAST).                /*{&FIELD8}- IMPORTE*/
                                
                    /*CTRLSOFT*
            -->
            <input type="text" name="Importe" value="`nImporteIntCAST`" id="Importe" size="14" onkeypress='aceptaimporte();' >
            <!--WSS *CTRLSOFT*/
                    end.
                    else
                    do:
                        if sServicio = "CAFO" then
                        do:
                            if lPromo80-20_SoloEmp = NO then
                            do:
                            /*CTRLSOFT PENDIENTE
            -->
            <input type="text" name="Importe" id="Importe" size="14" onkeypress='aceptaimporte();' >
            <!--WSS *CTRLSOFT*/	
                            end.
                        end.
                        else
                        do:
                        /*CTRLSOFT PENDIENTE
            -->
            <input type="text" name="Importe" id="Importe" size="14" onkeypress='aceptaimporte();' >
            <!--WSS    *CTRLSOFT*/
                        end.
                    end.
                END.
                ELSE
                DO:
                    IF sServicio = "CSBC" THEN
                    DO:
                        sDistribuirCheque = "if(event.keyCode==43 || event.keyCode==13)~{" +
                            "chequeSalvoBuenCobro('" + 
                            STRING(nUnion) + "','" + 
                            STRING(nCaja) + "','" + 
                            STRING(nSucursal) + "','" + 
                            STRING(nEjercicio) + "','" + 
                            STRING(nCajaAsignada) + "','" + 
                            sTransaccion + "','" + 
                            STRING(nFolioFicha) + "','" + 
                            sServicio + "','" + 
                            sUnionSocio + "','" + 
                            sCajaSocio + "','" + 
                            sSucursalSocio + "','" + 
                            sNumeroSocio +
                            "','btnaceptar();');" + "~}".
                    /*CTRLSOFT* PENMDIENTE
                -->
                        <input type="text" name="Importe" id="Importe" size="14" onkeypress="`sDistribuirCheque`" >
                <!--WSS *CTRLSOFT*/
                    END.
                    IF sServicio = "RENA" THEN
                    DO:
                        sDistribuirCheque = "if(event.keyCode==43 || event.keyCode==13)~{" +
                            "RemesaNacionalDeposito('" + 
                            STRING(nUnion) + "','" + 
                            STRING(nCaja) + "','" + 
                            STRING(nSucursal) + "','" + 
                            STRING(nEjercicio) + "','" + 
                            STRING(nCajaAsignada) + "','" + 
                            sTransaccion + "','" + 
                            STRING(nFolioFicha) + "','" + 
                            sServicio + "','" + 
                            sUnionSocio + "','" + 
                            sCajaSocio + "','" + 
                            sSucursalSocio + "','" + 
                            sNumeroSocio +
                            "','btnaceptar();');" + "~}".
                    /*CTRLSOFT PENDIENTE TAL VEZ SOLO GUARDAR EN VARIABLES*
                -->
                        <input type="hidden" name="hRNDepositante" id="hRNDepositante" size="14">
                        <input type="hidden" name="hRNComision" id="hRNComision" size="14">
                        <input type="hidden" name="hRNFolio" id="hRNFolio" size="14">
                        <input type="text" name="Importe" id="Importe" size="14" onkeypress="`sDistribuirCheque`" >
                <!--WSS
                *CTRLSOFT*/
                    END.
                    IF sServicio = "REMB" THEN
                    DO:
                        sDistribuirCheque = "if(event.keyCode==43 || event.keyCode==13)~{" +
                            "RemesaInterNacionalDeposito('" + 
                            STRING(nUnion) + "','" + 
                            STRING(nCaja) + "','" + 
                            STRING(nSucursal) + "','" + 
                            STRING(nEjercicio) + "','" + 
                            STRING(nCajaAsignada) + "','" + 
                            sTransaccion + "','" + 
                            STRING(nFolioFicha) + "','" + 
                            sServicio + "','" + 
                            sUnionSocio + "','" + 
                            sCajaSocio + "','" + 
                            sSucursalSocio + "','" + 
                            sNumeroSocio +
                            "','btnaceptar();');" + "~}".
                    /*CTRLSOFT PENDIENTE EN VARIABLES
                -->
                        <input type="hidden" name="hRNDepositante" id="hRNDepositante" size="14">
                        <input type="hidden" name="hRNComision" id="hRNComision" size="14">
                        <input type="hidden" name="hRNFolio" id="hRNFolio" size="14">
                        <input type="text" name="Importe" id="Importe" size="14" onkeypress="`sDistribuirCheque`" >
                <!--WSS
                *CTRLSOFT*/
                    END.
                    IF sServicio = "CSEF" THEN
                    DO:
                        sDistribuirCheque = "if(event.keyCode==43 || event.keyCode==13)~{" +
                            "CobroClienteSEFIDEC('" + 
                            STRING(nUnion) + "','" + 
                            STRING(nCaja) + "','" + 
                            STRING(nSucursal) + "','" + 
                            STRING(nEjercicio) + "','" + 
                            STRING(nCajaAsignada) + "','" + 
                            sTransaccion + "','" + 
                            STRING(nFolioFicha) + "','" + 
                            sServicio + "','" + 
                            sUnionSocio + "','" + 
                            sCajaSocio + "','" + 
                            sSucursalSocio + "','" + 
                            sNumeroSocio +
                            "','btnaceptar();');" + "~}".
                    /*CTRLSOFT PENDIENTE VARIABLES*
                -->
                        <input type="hidden" name="hRNDepositante" id="hRNDepositante" size="14">
                        <input type="hidden" name="hRNComision" id="hRNComision" size="14">
                        <input type="hidden" name="hRNFolio" id="hRNFolio" size="14">
                        <input type="text" name="Importe" id="Importe" size="14" onkeypress="`sDistribuirCheque`" >
                <!--WSS *CTRLSOFT*/
                    END.
                END.
                if sServicio = "CAFO" then
                do:
                    if lPromo80-20_SoloEmp = YES and lPromo80-20 = YES then
                    do:
                        sDistribuirCheque = "if(event.keyCode==43 || event.keyCode==13)~{" +
                            "Promo8020_SoloEmp('" + 
                            /*	                        STRING(nUnion) + "','" + 
                                                        STRING(nCaja) + "','" + 
                                                        STRING(nSucursal) + "','" + 
                                                        STRING(nEjercicio) + "','" + 
                                                        STRING(nCajaAsignada) + "','" + 
                                                        sTransaccion + "','" + 
                                                        STRING(nFolioFicha) + "','" + */
                            sServicio + "','" + 
                            sUnionSocio + "','" + 
                            sCajaSocio + "','" + 
                            sSucursalSocio + "','" + 
                            sNumeroSocio +
                            "','btnaceptar();');" + "~}".
                    /*CTRLSOFT PENDIENTE
                -->
                        <input type="hidden" name="jsFolioPromo" id="hGPServicioGG" value="SI" size="14">
                        <input type="text" name="Importe" id="Importe" size="14" onkeypress="`sDistribuirCheque`" >
                <!--WSS *CTRLSOFT*/
                ASSIGN 
                    ttEncdatdetficha.jsFolioPromo = "SI".
                
                    end.
                end.
            /*CTRLSOFT*
            -->

    <!--WSS	*CTRLSOFT*/
            /*               {&out} "<input type='text' name='Importe' size='14' maxlength='17' onkeypress= 'aceptaimporte();' >".*/
            END. 
        /*
                     ELSE DO:
                     /******************************************************************************/ 
        RUN Recibo no-error.             
        PROCEDURE Recibo:               
        
            if available({&tmTABLE}) and ({&tmTABLE}.{&FK21} = "SLUZ" OR {&tmTABLE}.{&FK21} = "SAGU")then
            do:	
                IF {&tmTABLE}.{&FK21} = "SLUZ" THEN /*Cuando se Paga el Servicio de Luz*/
                   vprueba = SUBSTRING({&tmTABLE}.{&FIELD6},11,42). 
                IF {&tmTABLE}.{&FK21} = "SAGU" THEN /*Cuando se Paga el Servicio de Agua*/
                   vprueba = SUBSTRING({&tmTABLE}.{&FIELD6},11,22). 
        		   
                      nOcultarBoton = 0.
                      IF vprueba = "" THEN
                      DO:
                         nOcultarBoton = 1.	
                      END. /*IF vprueba = "" OR vprueba =  ? THEN*/
                      
                IF {&tmTABLE}.{&FK21} = "SAGU" THEN /*Cuando se Paga el Servicio de Agua*/
                DO:
                
                vPuebaAux = SUBSTRING(vprueba,13,1).
                IF vpuebaAux = "" THEN
                DO: 
                    dFechaVencimiento = ?.
                    cRegistro = vprueba.
                END. /*IF vprueba = SUBSTRING(vprueba,1,13) = "" THEN*/
                ELSE
                DO:
                     cRegistro    = SUBSTRING(vprueba,1,12).
                     cImporteRecibo = SUBSTRING(vprueba,13,8) + "." + SUBSTRING(vprueba,21,2).   
                  END. /*ELSE*/
        		  
                END. /*IF {&tmTABLE}.{&FK21} = "SLUZ" THEN*/      
                IF {&tmTABLE}.{&FK21} = "SLUZ" THEN  /*Cuando se Paga el Servicio de Luz*/
                DO:
                
                  vPuebaAux = SUBSTRING(vprueba,13,1).
                  IF vpuebaAux = "" THEN
                  DO: 
                      dFechaVencimiento = ?.
                      cRegistro = vprueba.
                    END. /*IF vprueba = SUBSTRING(vprueba,1,13) = "" THEN*/
                  ELSE
                  DO:
                cConstante   = SUBSTRING(vprueba,1,2).
                cRegistro    = SUBSTRING(vprueba,3,12).
                cFechaVenAno = SUBSTRING(vprueba,15,2).
                cFechaVenAno = "20" + cFechaVenAno.
                cFechaVenMes = SUBSTRING(vprueba,17,2).
                cFechaVenDia = SUBSTRING(vprueba,19,2).
                dFechaVencimientoR = DATE(INT(cFechaVenMes),INT(cFechaVenDia),INT(cFechaVenAno)).
                cImporteRecibo = SUBSTRING(vprueba,21,9). 
                cDigitoVeri    = SUBSTRING(vprueba,30,1).  
                END. /*ELSE*/
        		  
                END. /*IF {&tmTABLE}.{&FK21} = "SLUZ" THEN*/
            end. /* if available({&tmTABLE}) then */
                     /******************************************************************************/
                         if cImporteRecibo <> "" then
                         do: 
                             {&out} "<input type='hidden' name='cvprueb'a value='" + string(vprueba) + "'>".
                            -->
                         <input type="text" name="Importe" value="`cImporteRecibo`" size="14" maxlength="17" onkeypress= "aceptaimporte();" >
               <!--WSS  end. /*if cImporteRecibo <> "" then*/
                        else 
                        do:
                     
                       {&out} "<input type='text' name='Importe' size='14' maxlength='17' onkeypress= 'aceptaimporte();' >".
                     
                        END. 
        END PROCEDURE.  
        
                     END.
        */
        /*CTRLSOFT*
         -->
         </font>
        </td>
          <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">&nbsp</font></td>
        <!--WSS IF bTablaVacia = yes THEN DO: -->
          <td bgcolor="`'{&GREY}'`" rowspan="11" style="`'{&OUTSET}'`">
          <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`" color="`'{&GREY}'`">
          &nbsp</font></td>
         <!--WSS END.-->
        </tr>
       <script language="JavaScript">
        window.document.forma.Importe.focus();
       </Script>
            
 <!--WSS*CTRLSOFT*/
        END.
        ELSE 
        DO:
            vintidDet = vintidDet + 1.
            CREATE ttDetdatdetficha.
            ASSIGN
                ttDetdatdetficha.INid              = ttEncdatdetficha.INid
                ttDetdatdetficha.INidDet           = vintidDet.
            
            ASSIGN
                ttDetdatdetficha.IDServicio = STRING(nDetalleFicha,"99999").
            /*CTRLSOFT TABLA VACIA */
            /*CTRLSOFT*-->
             <tr><td style="`'{&DETAIL}'`" align="right"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                      <input type="hidden" name="DetalleFicha">`STRING(nDetalleFicha,"99999")`</font></td>
                 <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                 <!--wss      if LogIDE = yes then do:  -->
                      <input type="text" name="Servicio" id="Servicio" onChange="window.document.forma.Servicio.value.toUpperCase(); muestraOculta(Concepto,'`nUnionSocioServicio`','`nCajaSocioServicio`','`nSucursalSocioServicio`','`nIDSocioServicio`','`nSucursalFicha`','`nCajaAsignada`','`sTransaccion`','`nFolioFicha`');"
                      size="5" maxlength="4" onkeypress= "if ((event.keyCode == 43 || event.keyCode == 13 ) && validateclaser(this.value)) {validatecla(this.value)}else{leeteclad(name,event.keyCode);}" onblur="valida()"></font></td>
                         <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                  <!--wss     end.
                              else do:   -->       
                      <input type="text" name="Servicio" id="Servicio" onChange="window.document.forma.Servicio.value.toUpperCase(); cambiaconcepto(forma,'Servicio',Concepto)"
                      size="5" maxlength="4" onkeypress= "if (event.keyCode == 43 || event.keyCode == 13 ){validatecla()}else{leeteclad(name,event.keyCode);}" onblur="valida()"></font></td>
                         <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
                      <!--wss end. -->
                      <div id="Conc1">
                      <input type="text" name="Concepto" id="Concepto" onBlour="validaconcepto()" size=50 maxlength=50>
                      </div>
                      <div id="Conc2"></div>
                      </font></td>
                 <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">&nbsp</font></td>
                 <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">&nbsp</font></td>
                 <td style="`'{&DETAIL}'`"><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">&nbsp</font></td>
                 <!--WSS IF bTablaVacia = yes THEN DO: -->
                 <td bgcolor="`'{&GREY}'`" rowspan="11" style="`'{&OUTSET}'`">
                    <font face="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`" color="`'{&GREY}'`">
                    &nbsp</font></td>
                 <!--WSS END.-->
            <!--WSS END.
            *CTRLSOFT*/
            ASSIGN 
                i-count1 = i-count1 + 1.
        END. 
        /*CTRLSOFT RENGLONES VACIOS*
        DEF VAR nContf AS INT.
        DO  i-count1 = 1 TO {&Result-Rows} - i-count:
            {&OUT} "<tr><td style=`{&DETAIL-LEFT}`><font face=`{&FONT-TYPE}` size=`{&FONT-SIZE}` color=`{&GREY}`>&nbsp</font></td>".
            DO nContf = 1 to 5:
              {&out} "<td style=`{&DETAIL}`><font face=`{&FONT-TYPE}`  size=`{&FONT-SIZE}` color=`{&GREY}`>&nbsp</font></td>".
            END.
        END.
        *CTRLSOFT*/
        FOR EACH DetalleFichaTemp WHERE DetalleFichaTemp.IDUnion = nUnionFicha AND
            DetalleFichaTemp.IDCajaPopular =nCajaFicha AND
            DetalleFichaTemp.IDSucursal = nSucursalFicha AND
            DetalleFichaTemp.Ejercicio = nEjercicio AND
            DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
            DetalleFichaTemp.IDTipoTransaccion = sTransaccion AND
            DetalleFichaTemp.IDFicha = nFolioFicha and
            DetalleFichaTemp.IDServicio <> "CSBC" and
            not detallefichatemp.referencia matches("*&DIP,*") NO-LOCK:
            ASSIGN 
                nImporteTotal1 = nImporteTotal1 + {&TABLE}.{&FIELD8} - {&TABLE}.{&FIELD7}.
        END.
        ASSIGN 
            nImporteTotal = ROUND(nImporteTotal1,1). 
        FOR EACH DetalleFichaTemp WHERE DetalleFichaTemp.IDUnion = nUnionFicha AND
            DetalleFichaTemp.IDCajaPopular =nCajaFicha AND
            DetalleFichaTemp.IDSucursal = nSucursalFicha AND
            DetalleFichaTemp.Ejercicio = nEjercicio AND
            DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
            DetalleFichaTemp.IDTipoTransaccion = sTransaccion AND
            DetalleFichaTemp.IDFicha = nFolioFicha and
            DetalleFichaTemp.IDServicio = "CSBC" and
            not detallefichatemp.referencia matches("*&DIP,*") NO-LOCK:
            ASSIGN 
                nImporteTotal = nImporteTotal + {&TABLE}.{&FIELD8} - {&TABLE}.{&FIELD7}.
        END.
        /*************************************************************************************************************************/    
        /**************************************************************MAM********************************************************/
        /*************************************************************************************************************************/

        ASSIGN
            ttEncdatdetficha.importeTotalficha = STRING(nImporteTotal,"->,>>>,>>9.99").
        /*CTRLSOFT*
        -->
           <tr><td colspan="4" style="`'{&OUTSET}'`" align = "right"><font size="1" face="MS Sans Serif">
                   <b>Total de la ficha</b></font></td>
               <td style="`'{&OUTSET}'`" align = "right"><font size="1" face="MS Sans Serif" color="#000080">
               <input type="hidden" name="ImporteTotal" value="`nImporteTotal`"><b>`STRING(nImporteTotal,"->,>>>,>>9.99")`</b></font></td>
               <td style="`'{&OUTSET}'`" align = "right"><font size="1">&nbsp</font></td>
               <td border="1" style="`'{&OUTSET}'`"><font size="1">
                   <img src="/gifs/corner.gif" align="right"></font></td>
           </tr>
           <!--WSS *CTRLSOFT*/
        IF nCobroAcumulado <> 0 THEN 
        DO: 
            nGTAcumulado = nImporteTotal + nCobroAcumulado.
            ASSIGN
                ttEncdatdetficha.importefichasacum = STRING(nCobroAcumulado,"->,>>>,>>9.99")
                ttEncdatdetficha.importeTotal      = STRING(nGTAcumulado,"->,>>>,>>9.99").
        /*CTRLSOFT*  -->
       <tr><td colspan="4" style="`'{&OUTSET}'`" align="right"><font size="1" face="MS Sans Serif">
           <b>Importe fichas acumuladas</b></font></td>
           <td style="`'{&OUTSET}'`" align="right"><font size="1" face="MS Sans Serif" color="#000080">
             <b>`STRING(nCobroAcumulado,"->,>>>,>>9.99")`</b></font></td>
           <td style="`'{&OUTSET}'`" align = "right"><font size="1">&nbsp</font></td>
           <td border="1" style="`'{&OUTSET}'`"><font size="1">
               <img src="/gifs/corner.gif" align="right"></font></td>
       </tr>
       <tr><td colspan="4" style="`'{&OUTSET}'`" align="right"><font size="1" face="MS Sans Serif">
           <b>Importe Total</b></font></td>
           <td style="`'{&OUTSET}'`" align="right"><font size="1" face="MS Sans Serif" color="#000080">
             <b>`STRING(nGTAcumulado,"->,>>>,>>9.99")`</b></font></td>
           <td style="`'{&OUTSET}'`" align="right"><font size="1">&nbsp</font></td>
           <td border="1" style="`'{&OUTSET}'`"><font size="1">
               <img src="/gifs/corner.gif" align="right"></font></td>
       </tr>
   <!--WSS*CTRLSOFT*/ 
        END. /*CTRLSOFT*-->
  </table> 
  <!--WSS IF sMode="" OR (sPagestatus="OUT" AND sMode="CON") THEN DO: -->
 <script language="JavaScript">
    window.document.forma.Servicio.focus();
 </script>
 <table>
  <tr><td colspan="2">
  		<!--WSS
        
         {&out} "<input type='hidden' name='nNumeroDetalle' value=' + string(nNumeroDetalle) + '>" +
         "<input type='hidden' name='nNumeroDetalle2' value=' + string(nNumeroDetalle) + '>" +
         "<input type='hidden' name='MaintOption1' value=''>" +
         "<input type='hidden' name='MaintOption' value=''>".
         -->
        *CTRLSOFT*/
    
        ASSIGN 
            ttEncdatdetficha.BTNContinuar  = "SI"
            ttEncdatdetficha.BTNCancelarMv = "SI"
            ttEncdatdetficha.INMaintOption = ""
            ttEncdatdetficha.INMaintOption1 = "".
            
        /*CTRLSOFT*
        <input type="button" name="Continuar" value="Continuar"
          <!--wss      if LogIDE = yes then do:  -->
               onClick="if(window.document.forma.Servicio.value != 'IVPF' && window.document.forma.Servicio.value != 'ivpf') { if(window.document.forma.Servicio.value != 'GTCO' && window.document.forma.Servicio.value != 'CSBC' && window.document.forma.Servicio.value != 'OTMT' && window.document.forma.Servicio.value != 'FODE' && window.document.forma.Servicio.value != 'SCFE' && window.document.forma.Servicio.value != 'COOE'){ continuaficha();}else{ if(window.document.forma.Concepto.value!=''){ continuaficha();}else{alert('Ingrese el concepto');}}} else{ alert('Inversiones se aplican por traspaso'); window.document.forma.Servicio.value=''; }">
           <!--wss     end.
                       else do:   -->       
               onClick="continuaficha();">
               <!--wss end.
        {&out} "<input type='button' name='CancelarMv' value='Cancelar movimientos'
               onClick='cancelamovtos();'>".
        *CTRLSOFT*/       
        /*CTRLSOFT* IF GET-VALUE("nOcultarBoton") <> "" THEN DO:
            nOcultarBoton = INT(GET-VALUE("nOcultarBoton")).
        END.*CTRLSOFT*/
        IF ttEncdatdetficha.nOcultarBoton <> "" THEN 
        DO:
            nOcultarBoton = INT(ttEncdatdetficha.nOcultarBoton).
        END.
        IF bTablaVacia = NO AND nOcultarBoton = 0 THEN 
        DO:
            ASSIGN 
                ttEncdatdetficha.BTNPago = "SI".
            /*CTRLSOFT* 
        /*susana*/
        {&out} "<input type='button' name='Pago' value='Pago' onClick='pagoficha();'>".
        *CTRLSOFT*/
            IF SSICanDo(sIDUsuario,nUnion,nCaja,sIDAplicacion,"ACUMFICH") and 1 = 2 THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.BTNAcumularficha = "SI".
            /*CTRLSOFT*  {&out} "<input type='button' name='Acumular' value='Acumular ficha'
                     onClick='EnviaCA();'>". *CTRLSOFT*/

            END.
            ELSE 
            DO: 
                ASSIGN 
                    ttEncdatdetficha.BTNAcumularficha = "NO".
            /*CTRLSOFT*
                {&out} "<input type='button' name='Acumular' value='Acumular ficha' onClick='alert('No tiene permiso para realizar esta acci�n')'>".
            *CTRLSOFT*/    
            END. /*CTRLSOFT* -->
                                     
         <!--WSS *CTRLSOFT*/ 
        END.
        ELSE 
        DO:
            IF bSaldoAcumulado = yes AND nOcultarBoton = 0 THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.BTNPago = "SI". 
            /*susana*/
            /*CTRLSOFT* {&out} "<input type='button' name='pago' value='Pago' onClick='pagoficha();'>". *CTRLSOFT*/
            END.
        END. /*CTRLSOFT* -->
   </tr>
 </table>
 <!--WSS 
 {&out} "<input type='hidden' name='sType' VALUE='dialog'>" + 
 	 "<input type='hidden' name='sMode' VALUE='CON'>" + 
	 "<input type='hidden' name='sPageStatus' VALUE='IN'>".
	 *CTRLSOFT*/

	 ASSIGN
	   ttEncdatdetficha.sType       = "dialog"
	   ttEncdatdetficha.sMode       = "CON"
	   ttEncdatdetficha.sPageStatus = "IN".
	   
    END.
    ELSE 
    DO:
        MESSAGE "STYPE DIALOG 2"
        VIEW-AS ALERT-BOX.
        ASSIGN
            ttEncdatdetficha.INacepta     = ""
            ttEncdatdetficha.INcancelar   = ""
            ttEncdatdetficha.sType       = "dialog"
            ttEncdatdetficha.sMode       = "CON"
            ttEncdatdetficha.sPageStatus = "OUT".
        /*CTRLSOFT*
      {&out} "<input type='hidden' name='acepta' VALUE=''>" +
              "<input type='hidden' name='cancelar' VALUE=''>" +
              "<input type='hidden' name='sType' VALUE='dialog'>" +
              "<input type='hidden' name='sMode' VALUE='CON'>" +
              "<input type='hidden' name='sPageStatus' VALUE='OUT'>". -->
      <table>
       <tr><td colspan="2">
       <!--WSS   
       *CTRLSOFT*/
    
        IF sServicio <> "CSBC" AND sServicio <> "RENA" AND sServicio <> "REMB" AND sServicio <> "CAFO" AND sServicio <> "CSEF" THEN
            ASSIGN ttEncdatdetficha.BTNAceptar = "SI". 
        /*CTRLSOFT* {&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='btnaceptar();'>".*CTRLSOFT*/
        ELSE IF sServicio = "CSBC" THEN /*CTRLSOFT* {&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='" 'chequeSalvoBuenCobro("' nUnion '","' nCaja '","' nSucursal '","' nEjercicio '","' nCajaAsignada '","' sTransaccion '","' nFolioFicha '","' sServicio '","' sUnionSocio '","' sCajaSocio '","' sSucursalSocio '","' sNumeroSocio '","btnaceptar();");' "'>". *CTRLSOFT*/
                ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
            ELSE IF sServicio = "RENA" THEN /*CTRLSOFT*{&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='" 'RemesaNacionalDeposito("' nUnion '","' nCaja '","' nSucursal '","' nEjercicio '","' nCajaAsignada '","' sTransaccion '","' nFolioFicha '","' sServicio '","' sUnionSocio '","' sCajaSocio '","' sSucursalSocio '","' sNumeroSocio '","btnaceptar();");' "'>". *CTRLSOFT*/
                    ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
                ELSE IF sServicio = "REMB" THEN /*CTRLSOFT*{&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='" 'RemesaInterNacionalDeposito("' nUnion '","' nCaja '","' nSucursal '","' nEjercicio '","' nCajaAsignada '","' sTransaccion '","' nFolioFicha '","' sServicio '","' sUnionSocio '","' sCajaSocio '","' sSucursalSocio '","' sNumeroSocio '","btnaceptar();");' "'>". *CTRLSOFT*/
                        ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
                    ELSE IF sServicio = "CSEF" THEN /*CTRLSOFT*{&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='" 'CobroClienteSEFIDEC("' nUnion '","' nCaja '","' nSucursal '","' nEjercicio '","' nCajaAsignada '","' sTransaccion '","' nFolioFicha '","' sServicio '","' sUnionSocio '","' sCajaSocio '","' sSucursalSocio '","' sNumeroSocio '","btnaceptar();");' "'>". *CTRLSOFT*/
                            ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
                        ELSE IF sServicio = "CAFO" THEN
                            do:
                                if lPromo80-20_SoloEmp = YES and lPromo80-20 = YES then /*CTRLSOFT* {&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='" 'Promo8020_SoloEmp("' sServicio '","' sUnionSocio '","' sCajaSocio '","' sSucursalSocio '","' sNumeroSocio '","btnaceptar();");' "'>". *CTRLSOFT*/
                                    ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
                                else /*CTRLSOFT* {&out} "<input type='button' name='btnAceptar' value='Aceptar' onClick='btnaceptar();'>". *CTRLSOFT*/
                                    ASSIGN ttEncdatdetficha.BTNAceptar = "SI".
                            end.
        ASSIGN 
            ttEncdatdetficha.BTNlimpiar = "SI".
    /*CTRLSOFT*
    {&out} "<input type='reset'  name='limpiar' value='Limpiar'>" +
     "<input type='button' name='btnCancelar' value='Cancelar' onClick='cancelamvto();'>".
     -->
    </td>
</tr>
</table>
<!--WSS*CTRLSOFT*/
    END.
    ASSIGN
        ttEncdatdetficha.sCallerProcedure = sCallerProcedure
        ttEncdatdetficha.sPk              = sPk
        ttEncdatdetficha.NavRowid         = NavRowid
        ttEncdatdetficha.Navigate         = Navigate
        ttEncdatdetficha.sWhere           = sWhere
        ttEncdatdetficha.sWhereAux        = sWhereAux
        ttEncdatdetficha.sFilter          = sFilter
        ttEncdatdetficha.sFilterAux       = sFilterAux
        ttEncdatdetficha.sOrder           = sOrder
        .
        /*
    ASSIGN
        ttEncdatdetficha.respuesta = "SUCCESS1".
        */    
/*CTRLSOFT*
{&out} "<input type='hidden' name='sCallerProcedure' VALUE='" + sCallerProcedure + "'>" +
       "<input type='hidden' name='sPk' VALUE='" + sPk + "'>" +
       "<input type='hidden' name='NavRowid' VALUE='" + NavRowid + "'>" +
       "<input type='hidden' name='Navigate' VALUE='" + Navigate + "'>" +
       "<input type='hidden' name='sWhere' VALUE='" + sWhere + "'>" +
       "<input type='hidden' name='sWhereAux' VALUE='" + sWhereAux + "'>" +
       "<input type='hidden' name='sFilter' VALUE='" + sFilter + "'>" +
       "<input type='hidden' name='sFilterAux' VALUE='" + sFilterAux + "'>" +
       "<input type='hidden' name='sOrder' VALUE='" + sOrder + "'>".
-->
</script>
<script language="SpeedScript">
*CTRLSOFT*/
END. /*... Termina Browser */
ELSE 
DO:
    /*CTRLSOFT*
 </script>
 <!-- *********************** CREACION DE LA FORMA TIPO MODAL ***************************** -->
 <!-- Inicia la creaci�n de la forma de la pagina tipo MODAL.
      Esta forma es configurada de acuerdo a la cantidad de campos que se requiere mostrar -->
 <!--WSS IF sMode = "INS":U OR sMode = "CON":U THEN 
      ASSIGN sTitle = "ANUR9801/INFORM�TICA " + string(year(today),"9999") + " - Insertar movimiento".
   ELSE IF sMode = "MTO":U THEN do:
      ASSIGN sTitle = "ANUR9801/INFORM�TICA "  + string(year(today),"9999") +  " - Modificar movimiento".
      
      end.
   IF sMode = "FIL":U THEN
      ASSIGN sTitle = "ANUR9801/INFORM�TICA  "  + string(year(today),"9999") +  " Filtrar movimiento".
 -->
 <br><br>
 *CTRLSOFT*/
    vintidDet = vintidDet + 1.
    CREATE ttDetdatdetficha.
    ASSIGN
        ttDetdatdetficha.INid              = ttEncdatdetficha.INid
        ttDetdatdetficha.INidDet           = vintidDet
        /**/ 
        ttDetdatdetficha.IDDetalleFicha    = STRING(tmDetalleFichaTemp.IDDetalleFicha)       /*{&TABLE}.{&PK}- Mov*/
        /**/ 
        ttDetdatdetficha.IDServicio        = STRING(tmDetalleFichaTemp.IDServicio)   /*{&TABLE}.{&FK21}- Servicio*/
        ttDetdatdetficha.IDServicioForaneo = tmServicio.Descripcion    /*{&FIELD1FK2}- */
        /**/ 
        ttDetdatdetficha.Concepto          = tmDetalleFichaTemp.Concepto             /*{&FIELD6}- Comcepto*/
        /**/ 
        ttDetdatdetficha.SaldoAnterior     = STRING(tmDetalleFichaTemp.SaldoAnterior,"->,>>>,>>>,>>9.99") /*{&FIELD9}- Saldo Anterior*/
      
        /*ttDetdatdetficha.Cargo               = DetalleFichaTemp.Cargo                /*{&FIELD7}- */*/
        ttDetdatdetficha.Abono             = STRING(tmDetalleFichaTemp.Abono)                /*{&FIELD8}- IMPORTE*/
        /* ttDetdatdetficha.nSaldoActual        = nSaldoActual                          /*         -SaldoActual*/*/
        .
      
           
    /*CTRLSOFT*
     
<form action="`sURLApp`" method="GET" name="DetailFrame">
<table  border="1"  WIDTH="95%">
 <tr><th colspan="2" align = "left">
     <font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">`sTitle`</font><th></tr>             
 <tr><th valign = "middle" align = "right" ><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
      No. de movimiento:</font></th>         
     <td><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
        `html-encode({&tmTABLE}.{&PK}:SCREEN-VALUE IN FRAME DetailFrame)`
        <input type="hidden" name="DetalleFicha"
               value="`html-encode({&tmTABLE}.{&PK}:SCREEN-VALUE IN FRAME DetailFrame)`" width="5">
 </font></td></tr>         
 <tr><th valign = "middle" align = "right" ><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
      Servicio:</font></th>         
     <td><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
        `html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame)`
        *CTRLSOFT*/
        
        /*CTRLSOFT* <input type="hidden" name="Servicio"
               value="`html-encode({&tmTABLE}.{&FK21}:SCREEN-VALUE IN FRAME DetailFrame)`" width="5"> *CTRLSOFT*/
        ASSIGN ttEncdatdetficha.INsFolioFicha = STRING({&tmTABLE}.{&FK21}).
               /*CTRLSOFT*
        <font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
         `html-encode({&tmTABLE2}.{&FIELD1FK2}:SCREEN-VALUE IN FRAME DetailFrame)`
        <input type="hidden" name="`'{&tmTABLE2}.{&FIELD1FK2}'`"
               value="`html-encode({&tmTABLE2}.{&FIELD1FK2}:SCREEN-VALUE IN FRAME DetailFrame)`" width="50">
 </font></td></tr>
 <tr><th valign = "top" align = "right" >
         <font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">Concepto:</font></th>
     <td><font face ="`'{&FONT-TYPE}'`"  size="`'{&FONT-SIZE}'`">
         <input type="text" name="Concepto" SIZE="40" MAXLENGHT="40"
               value="`html-encode({&tmTABLE}.{&FIELD6}:SCREEN-VALUE IN FRAME DetailFrame)`">
 </font></td></tr>
 <tr><th valign = "top" align = "right" ><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
        `html-encode({&tmTABLE}.{&FIELD9}:LABEL IN FRAME DetailFrame + ':':U)`</font></th>
      <td align="rigth"><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
          <input type="hidden" name="`'{&tmTABLE}.{&FIELD9}'`"
                 value="`html-encode({&tmTABLE}.{&FIELD9}:SCREEN-VALUE IN FRAME DetailFrame)`" size="14">
                 `string({&tmTABLE}.{&FIELD9},"->,>>>,>>>,>>9.99")`
 </font></td></tr>
 <tr><th valign = "top" align="right" ><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
         `html-encode({&tmTABLE}.{&FIELD8}:LABEL IN FRAME DetailFrame + ':':U)`</font></th>   
     <td><font face ="`'{&FONT-TYPE}'`" size="`'{&FONT-SIZE}'`">
         <input type="text" name="Importe"
                value="`html-encode({&tmTABLE}.{&FIELD8}:SCREEN-VALUE IN FRAME DetailFrame)`" size="14">
 </font></td></tr>
</table>
*CTRLSOFT*/
    ASSIGN 
        ttEncdatdetficha.sCallerProcedure = sCallerProcedure
        ttEncdatdetficha.sPk              = sPk
        ttEncdatdetficha.sType            = "dialog"
        ttEncdatdetficha.sMode            = "CON"
        ttEncdatdetficha.sPageStatus      = "OUT"
        ttEncdatdetficha.NavRowid         = NavRowid
        ttEncdatdetficha.Navigate         = Navigate
        ttEncdatdetficha.sWhere           = sWhere
        ttEncdatdetficha.sWhereAux        = sWhereAux
        ttEncdatdetficha.sFilter          = sFilter
        ttEncdatdetficha.sFilterAux       = sFilterAux
        ttEncdatdetficha.sOrder           = sOrder
        .
    /*CTRLSOFT*
    <!--WSS
    {&out}
    "<input type='hidden' name='sCallerProcedure' VALUE='" + sCallerProcedure + "'>" +
    "<input type='hidden' name='sPk' VALUE='" + sPk + "'>" +
    "<input type='hidden' name='sType' VALUE='dialog'>" +
    "<input type='hidden' name='sMode' VALUE='CON'>" +
    "<input type='hidden' name='sPageStatus' VALUE='OUT'>" +
    "<input type='hidden' name='NavRowid' VALUE='" + NavRowid + "'>" +
    "<input type='hidden' name='Navigate' VALUE='" + Navigate + "'>" +
    "<input type='hidden' name='sWhere' VALUE='" + sWhere + "'>" +
    "<input type='hidden' name='sWhereAux' VALUE='" + sWhereAux + "'>" +
    "<input type='hidden' name='sFilter' VALUE='" + sFilter + "'>" +
    "<input type='hidden' name='sFilterAux' VALUE='" + sFilterAux + "'>" +
    "<input type='hidden' name='sOrder' VALUE='" + sOrder + "'>".
   -->
    <script language="JavaScript">
              window.document.DetailFrame.Concepto.focus();
    </script>
    <center><table>
     <tr>
          <!--wss run x.
          procedure x:
              *CTRLSOFT*/
    if NOT ({&tmTABLE}.{&FIELD10} matches("*&DIP,*")) and NOT ({&tmTABLE}.{&FIELD10} matches("*&ADIP,*")) then 
    do:
        ASSIGN 
            ttEncdatdetficha.BTNModificar = "SI"
            ttEncdatdetficha.BTNBorrar    = "SI".
    /*CTRLSOFT* 
{&out} "<td><input type='submit' name='MaintOption' value='Modificar'></td>". -->
<td><input type="submit" name="MaintOption" value="  Borrar "
      onClick="if ( ! window.confirm(&quot;�Est� usted seguro de querer borrar el registro?&quot;)) {return false;}">
</td>
<!--wss *CTRLSOFT*/ 
    end. 
    ASSIGN 
        ttEncdatdetficha.BTNCancelar = "SI"
        ttEncdatdetficha.BTNLimpiar  = "SI".
/*CTRLSOFT*end. 
{&out} "<td><input type='submit' name='MaintOption' value='Cancelar'></td>" +
        "<td><input type='reset' name='MaintOption' value='Limpiar'></td>".
--> </tr>
</table></center>
<Script Language ="SpeedScript">
*CTRLSOFT*/
END.

ASSIGN
    ttEncdatdetficha.nOcultarBoton    = string(nOcultarBoton)
    /*ttEncdatdetficha.*/
    ttEncdatdetficha.INsIDUsuario     = sUsuario
    ttEncdatdetficha.INsCajaAsignada  = STRING(nCajaAsignada)
    ttEncdatdetficha.INsTransaccion   = sTransaccion
    ttEncdatdetficha.INsFolioFicha    = STRING(nFolioFicha) 
    ttEncdatdetficha.INsUnionSocio    = sUnionSocio
    ttEncdatdetficha.INsCajaSocio     = sCajaSocio
    ttEncdatdetficha.INsSucursalSocio = sSucursalSocio
    ttEncdatdetficha.INsNumeroSocio   = sNumeroSocio
    ttEncdatdetficha.INImporteTotal   = STRING(nImporteTotal)
    /*ttEncdatdetficha.AceptaServicio*/
    ttEncdatdetficha.INACajero        = ""
    ttEncdatdetficha.INANomCajero     = ""
    ttEncdatdetficha.INAUCSCajero     = ""
    .
    /*
    ASSIGN
        ttEncdatdetficha.respuesta = "SUCCESS2".
        */     
/*CTRLSOFT*
  </script>
<!--WSS

{&out} "<input type='hidden' name='nOcultarBoton' VALUE='" + string(nOcultarBoton) + "'>   " +
 "<input type='hidden' name='UnionFicha' VALUE='" + sUnionFicha + "'>" +
 "<input type='hidden' name='CajaPopularFicha' VALUE='" + sCajaPopularFicha + "'>" +
 "<input type='hidden' name='SucursalFicha' VALUE='" + sSucursalFicha + "'>" +
 "<input type='hidden' name='Ejercicio' VALUE='" + STRING(nEjercicio) + "'>" +
 "<input type='hidden' name='Usuario' VALUE='" + sUsuario + "'>" +
 "<input type='hidden' name='CajaAsignada' VALUE='" + STRING(nCajaAsignada) + "'>" +
 "<input type='hidden' name='Transaccion' VALUE='" + sTransaccion + "'>" +
 "<input type='hidden' name='FolioFicha' VALUE='" + STRING(nFolioFicha) + "'>" +
 "<input type='hidden' name='UnionSocio' VALUE='" + sUnionSocio + "'>" +
 "<input type='hidden' name='CajaSocio' VALUE='" + sCajaSocio + "'>" +
 "<input type='hidden' name='SucursalSocio' VALUE='" + sSucursalSocio + "'>" +
 "<input type='hidden' name='NumeroSocio' VALUE='" + sNumeroSocio + "'>" +
 "<input type='hidden' name='ImporteTotal' VALUE='" + STRING(nImporteTotal) + "'>" +
 "<input type='hidden' name='AceptaServicio' VALUE='" + STRING(bContinua) + "'>" +
 "<input type='hidden' name='Cajero' VALUE=''>" +
 "<input type='hidden' name='NomCajero' VALUE=''>" +
 "<input type='hidden' name='UCSCajero' VALUE=''>".
 -->
 <script language="SpeedScript">
 
 IF bRequiereCajero=yes THEN DO:
     
    </script><SCRIPT language="JavaScript">
    window.document.forma.NomCajero.value=eval("document.formacja.NomCajero.value");
    window.document.forma.Cajero.value=eval("document.formacja.Cajero.value");
    window.document.forma.UCSCajero.value=eval("document.formacja.UCSCajero.value");
    </script><script language="SpeedScript">
    
  END.
  
  </SCRIPT>
 </form>
<script language="SpeedScript">
  IF sCallerApp <> "" THEN DO:
  </script>
  <br>
  <br>
  <a href="`sCallerApp%20+%20url-field('sPk':U," ",'?')%20+%20url-field(sApp,'Cancel',?)%20+%20url-field('sCallerProcedure',sCallerProcedure,?)`">
   Regresar sin asignar clave ...</a>
<script language="SpeedScript">
  END.
/*  END. */ /* Es del IF sType = "" OR ... */

IF AVAILABLE-MESSAGES(?) THEN DO:
</script>
   <hr width="`'{&TOTAL-WIDTH}'`" size="2">
<script language = "SpeedScript">
   output-messages("group","informacion","Informaci�n sobre el registro:").
   output-messages("group","bloqueo","Bloqueo de registros:").
   output-messages("group","error","Mensajes de error").
   output-messages("group","Depura","Mensajes de depuaraci�n"). /***/
END.
</script>
<script language = "SpeedScript">
*CTRLSOFT*/

/* ********* FIN MAINBLOCK *********************************** */

/* ********* DEFINICION DE PROCEDIMIENTOS ******************* */

PROCEDURE AsignaValores:
    RUN Login(INPUT-OUTPUT nUnionFicha, INPUT-OUTPUT nCajaFicha, INPUT-OUTPUT nSucursalFicha, INPUT-OUTPUT sUsuario).
    /*CTRLSOFT*
    sNuevoUsuario = TRIM(CAPS(cookie-value('NuevoUsuario',"UsuarioOrigen"))).
    IF sNuevoUsuario <> sUsuario  AND sNuevoUsuario<>"" AND sNuevoUsuario<>? THEN
       sUsuario = TRIM(CAPS(cookie-value('NuevoUsuario',"UsuarioOrigen"))).
    ASSIGN sUnionSocio = cookie-value('cookie',"UnionSocio":U)
           sCajaSocio = cookie-value('cookie',"CajaSocio":U)
           sSucursalSocio = cookie-value('cookie',"SucursalSocio":U)
           sNumeroSocio = cookie-value('cookie',"NumeroSocio")
           sTransaccion = cookie-value('cookieTran',"Transaccion")
           sTransaccion1 = sTransaccion.
    *CTRLSOFT*/
    sNuevoUsuario = TRIM(CAPS(ttEncdatdetficha.INUsuarioOrigen)).
    IF sNuevoUsuario <> sUsuario  AND sNuevoUsuario<>"" AND sNuevoUsuario<>? THEN
        sUsuario = TRIM(CAPS(ttEncdatdetficha.INUsuarioOrigen)).
    ASSIGN 
        sUnionSocio    = ttEncdatdetficha.INsUnionSocio
        sCajaSocio     = ttEncdatdetficha.INsCajaSocio
        sSucursalSocio = ttEncdatdetficha.INsSucursalSocio
        sNumeroSocio   = ttEncdatdetficha.INsNumeroSocio
        sTransaccion   = ttEncdatdetficha.INsTransaccion
        sTransaccion1  = sTransaccion.
                  
    RUN ValidaExistenciaCorreo(INPUT INT(sUnionSocio),INT(sCajaSocio),INT(sSucursalSocio),INT(sNumeroSocio)).
    IF bExisteFichaCorreo=yes THEN 
    DO:
        ttEncdatdetficha.respuesta = "Existe una Ficha Correo pendiente de aplicar para este socio. Si desea realizar otro movimiento primero debe de aplicar o cancelar la ficha correo.".
        RETURN.
    /*CTRLSOFT*
   </script><SCRIPT language="JavaScript">
      alert("Existe una Ficha Correo pendiente de aplicar para este socio. Si desea realizar otro movimiento primero debe de aplicar o cancelar la ficha correo.");
      window.open("buscasocio.r?sTransaccion=D&sIrPagina=detalleframed","detalle");
    </script> <script language="SpeedScript">
    *CTRLSOFT*/
    END.
    RUN AsignaLimiteCapitalSocial.
    IF bErrorParametro=yes THEN 
    DO:
        ttEncdatdetficha.respuesta = "No se existe en parametros generales el valor para " + sNombreparametro.
        RETURN.
    /*CTRLSOFT*
    </script><SCRIPT language="JavaScript">
       alert("No se existe en parametros generales el valor para `sNombreparametro`");
       window.open("buscasocio.r?sTransaccion=D&sIrPagina=detalleframed","detalle");  
     </script> <script language="SpeedScript">
     *CTRLSOFT*/
    END.
    FIND TipoTransaccion WHERE TipoTransaccion.IDTipoTransaccion = sTransaccion NO-LOCK NO-ERROR.
    IF AVAILABLE (TipoTransaccion) THEN
        ASSIGN sDescripcion1 = TRIM(CAPS(TipoTransaccion.Descripcion)).
    RUN NombreSocio.
    IF nUnionFicha <> INT(sUnionSocio) OR nCajaFicha<>INT(sCajaSocio) OR
       nSucursalFicha<>INT(sSucursalSocio) THEN DO:
    ASSIGN 
        bSocioForaneo = YES.
END.

END PROCEDURE.


PROCEDURE CreaArreglosJS:
    /*CTRLSOFT*
</script><SCRIPT language="JavaScript">
 // Conjunto de c�digos 
   servicios1= new Array();
   servicios1[0] = new Array(3);
   servicios1[0][0]= "-";
   servicios1[0][1]= "";
   servicios1[0][2]= "";
   servicios1[1] = new Array(3);
   servicios1[1][0]= "-";
   servicios1[1][1]= "";
   servicios1[1][2]= "";
   var s=2;
   </script><Script Language="SpeedScript">
   *CTRLSOFT*/
    DEFINE VARIABLE vintID AS INTEGER NO-UNDO.
    vintID = 0.
    FOR EACH ServicioTipoSocio WHERE ServicioTipoSocio.IDTipoSocio=sTipoSocio
        AND ServicioTipoSocio.Activo=YES USE-INDEX Activo NO-LOCK:
        if ServicioTipoSocio.IDServicio <> "REMB" and ServicioTipoSocio.IDServicio <> "RENB" then
        do:
            FIND Servicio WHERE Servicio.IDServicio=ServicioTipoSocio.IDServicio NO-LOCK NO-ERROR.
            IF available(servicio) then 
            do:   
                vintID = vintID + 1.
                CREATE ttservicios.
                ASSIGN
                    ttservicios.ID                   = vintID
                    ttservicios.sServicio            = Servicio.IDServicio
                    ttservicios.sDescripcionServicio = Servicio.Descripcion
                    ttservicios.sTipoServicio        = Servicio.IDTipoServicio.
	               
                ASSIGN 
                    sServicio = Servicio.IDServicio.
                ASSIGN 
                    sDescripcionServicio = Servicio.Descripcion.
                ASSIGN 
                    sTipoServicio = Servicio.IDTipoServicio.
            /*CTRLSOFT*
            </script><script language="JavaScript">
            servicios1[s] =  new Array(3);
            servicios1[s][0]= "`sServicio`";
            servicios1[s][1]= "`sDescripcionServicio`";
            servicios1[s][2]= "`sTipoServicio`";     
            s++
            </script> <script language="SpeedScript">
            *CTRLSOFT*/
            END.
        end.
    END.
END PROCEDURE.

PROCEDURE FichasAcumuladas:
    nCobroAcumulado = 0.
    FOR EACH Ficha Where Ficha.IDUnion = nUnionFicha AND
        Ficha.IDCajaPopular = nCajaFicha AND
        Ficha.IDSucursal = nSucursalFicha AND
        Ficha.Ejercicio = nEjercicio AND
        Ficha.IDCajaAsignada = nCajaAsignada AND
        Ficha.FormaPago = "M"  AND
        Ficha.FichaCancelada= NO USE-INDEX FichaPago NO-LOCK:
        FIND DetalleFicha OF Ficha WHERE DetalleFicha.IDServicio = "CAJA" NO-LOCK NO-ERROR.
        IF NOT ERROR-STATUS:ERROR AND AVAILABLE (DetalleFicha) THEN 
        DO:
            ASSIGN 
                nCobroAcumulado = nCobroAcumulado + DetalleFicha.Cargo - DetalleFicha.Abono.            
        END.
        sFichasAcumuladas= sFichasAcumuladas + "// " + Ficha.IDTipoTransaccion + " " + STRING(Ficha.IDFicha,"999999") + " $"
            + STRING(nCobroAcumulado,"->>>,>>>,>>9.99").
    END.
END PROCEDURE.
/********************************************/
PROCEDURE periodoIngresos:
    DEF INPUT PARAMETER Ingreso         AS DECIMAL.
    DEF INPUT PARAMETER Periodo         AS CHAR.
    DEF OUTPUT PARAMETER MontoIngreso   AS DECIMAL.
    
    DEF VAR MontoPorDia AS DECIMAL.
    CASE caps(periodo):
        WHEN "ANU" THEN 
            MontoIngreso=Ingreso / 12.      
        WHEN "MEN" THEN 
            MontoIngreso=Ingreso.        
        WHEN "P28" THEN 
            DO:
                MontoPorDia=Ingreso / 28.
                MontoIngreso=MontoPorDia * 30.
            END.
        WHEN "QUI" THEN 
            MontoIngreso = Ingreso * 2.        
        WHEN "SEM" THEN 
            MontoIngreso = Ingreso * 4.
        WHEN "SMT" THEN          
            MontoIngreso = Ingreso / 6.        
    END CASE.
    
END PROCEDURE.

PROCEDURE ValidaPago:
    def var bCobroAcum AS LOGICAL INIT NO.
    ASSIGN 
        sType = "".

    def var dCompromisoAhorro  as decimal initial 0 no-undo.
    def var dImporteAH         as decimal initial 0 no-undo.
    def var EntraDIP           as char    initial '' no-undo.
    DEF VAR IngresosOrdinarios AS DECIMAL DECIMALS 1.
    DEF VAR monto              AS DECIMAL INITIAL 0 NO-UNDO.
    def var lImporteTotalVP    as logical.
    /*CTRLSOFT* DECIMAL(get-value("ImporteTotal")) NO-ERROR. *CTRLSOFT*/
    DECIMAL(ttEncdatdetficha.INImporteTotal) NO-ERROR.
    IF NOT ERROR-STATUS:ERROR THEN lImporteTotalVP = YES.
    ELSE lImporteTotalVP = NO.

    
    IF saplicadescuentoTP = 'SI' THEN 
    do:

        FIND FIRST DetalleFichaTemp WHERE DetalleFichaTemp.IDUnion = nUnionFicha AND
            DetalleFichaTemp.IDCajaPopular =nCajaFicha AND
            DetalleFichaTemp.IDSucursal = nSucursalFicha AND
            DetalleFichaTemp.Ejercicio = nEjercicio AND
            DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
            DetalleFichaTemp.IDTipoTransaccion = sTransaccion AND
            DetalleFichaTemp.IDFicha = nFolioFicha  and
            (detallefichatemp.referencia MATCHES("*&DIP,*") or 
            detallefichatemp.referencia MATCHES("*&ADIP,*")) NO-LOCK NO-ERROR.
        IF AVAILABLE(DetalleFichaTemp) THEN EntraDIP = 'SI'.

        IF EntraDIP = 'SI' THEN 
        do:
            
            for each DetalleFichaTemp  WHERE DetalleFichaTemp.IDUnion = nUnionFicha AND
                DetalleFichaTemp.IDCajaPopular = nCajaFicha AND
                DetalleFichaTemp.IDSucursal = nSucursalFicha AND
                DetalleFichaTemp.Ejercicio = nEjercicio AND
                DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
                DetalleFichaTemp.IDTipoTransaccion = sTransaccion and
                DetalleFichaTemp.IDFicha = nFolioFicha AND
                DetallefichaTemp.abono > 0 AND
                NOT(DetallefichaTemp.referencia MATCHES('*&ADIP,*')), 
                EACH servicio WHERE servicio.idservicio = detallefichatemp.idservicio 
                AND servicio.idtiposervicio = 'aho' NO-LOCK:
                                                          
                dImporteAH = dImporteAH + detallefichatemp.abono.

            END.  /*detallefichatemp*/

            IF truncate(dImporteAH,2) <= 0 and bCompromisoAHORRO = "SI" THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.pregunta  = "�El compromiso de ahorro NO puede ser cero...la ficha NO podr� ser concluida, Desea omitir el descuento?"
                    ttEncdatdetficha.aviso     = "Capture el compromiso de ahorro" 
                    ttEncdatdetficha.respuesta = "detalleficha.r?validapago=NO|detalleficha.r?validapago=SI".
            /*CTRLSOFT*
            </script><script language="JavaScript">
                 if(window.confirm('�El compromiso de ahorro NO puede ser cero...la ficha NO podr� ser concluida, Desea omitir el descuento?')){ window.open('detalleficha.r?validapago=NO','CuerpoDetalle'); }
                 else { alert("Capture el compromiso de ahorro");
                    window.open('detalleficha.r?validapago=SI','CuerpoDetalle'); }                     
             </script><script language="SpeedScript">
             *CTRLSOFT*/
            END. /*if*/
            

        end.  /*for each*/
    end.  /*EntraDIP*/

    IF nCobroAcumulado <> 0 THEN 
    DO:
        DEFINE VARIABLE vlgcY AS LOGICAL NO-UNDO.
        RUN fichasacumuladas.r (OUTPUT vlgcY).
        /*CTRLSOFT*
       </script><script language="JavaScript">
       y=eval("showModalDialog('fichasacumuladas.r?Ficha=`nFolioFicha`','','dialogWidth:40;dialogHeight:20;center:yes')");         	    
       </script> <script language="SpeedScript">
       *CTRLSOFT*/
        if lImporteTotalVP = YES then
        do:
            /**CTRLSOFT* IF nCobroAcumulado > 0 OR DECIMAL(get-value("ImporteTotal")) > nCobroAcumulado THEN DO:*CTRLSOFT*/       
            IF nCobroAcumulado > 0 OR DECIMAL(ttEncdatdetficha.INImporteTotal) > nCobroAcumulado THEN 
            DO:
                IF vlgcY THEN
                    RUN pagoficha.r.
            /*CTRLSOFT*
           </script><script language="JavaScript">
           if (y=="SI")
              {window.open("pagoficha.r?CobroAcumulado=SI&TotalAcumulado=`nCobroAcumulado`","CuerpoDetalle");
              }
           </script> <script language="SpeedScript">
           *CTRLSOFT*/
            END.
            IF nCobroAcumulado < 0  OR DECIMAL(ttEncdatdetficha.INImporteTotal) < nCobroAcumulado THEN 
            DO:
                IF vlgcY THEN
                    RUN entregacambio.r.
            /*CTRLSOFT*
              </script><script language="JavaScript">
              if (y=="SI")
                 {window.open("entregacambio.r?CobroAcumulado=SI&TotalAcumulado=`nCobroAcumulado`","CuerpoDetalle");              
              }
              </script> <script language="SpeedScript">
              *CTRLSOFT*/
            END.
        end.
    END.
    IF nCobroAcumulado = 0 THEN 
    DO:
        RUN pagoficha.r.
    /*CTRLSOFT*
   </script><script language="JavaScript">
      window.open("pagoficha.r?CobroAcumulado=NO&cImporteRecibo=`cImporteRecibo`","CuerpoDetalle");
   </script> <script language="SpeedScript">
   *CTRLSOFT*/
    END.
    if lImporteTotalVP = YES then
    do:
        IF DECIMAL(ttEncdatdetficha.INImporteTotal) = nCobroAcumulado THEN 
        DO:
            /*CTRLSOFT*
             </script><script language="JavaScript">
              alert("El total de la ficha es igual al importe acumulado.").
             </script> <script language="SpeedScript">
             *CTRLSOFT*/
            ASSIGN 
                ttEncdatdetficha.respuesta = "El total de la ficha es igual al importe acumulado.".
            RUN CreaFichaDeposito.
        END.
    end.
END PROCEDURE.


PROCEDURE ContinuaPagina:
    ASSIGN /*CTRLSOFT* sServicio = TRIM(CAPS(get-value("Servicio"))) *CTRLSOFT*/
        sServicio       = TRIM(CAPS(ttEncdatdetficha.INServicio))
        sTipoMovimiento = "A".
    ASSIGN 
        nUnionSocio    = INT(sUnionSocio)
        nCajasocio     = INT(sCajaSocio)
        nSucursalSocio = INT(sSucursalSocio)
        nNumeroSocio   = INT(sNumeroSocio) NO-ERROR.   
   
    RUN validamovimientoservicio (nUnionSocio, nCajaSocio, nSucursalSocio, nNumeroSocio, sServicio,
                                  INPUT  ttEncdatdetficha.INEntregoCheque,
                                  INPUT  ttEncdatdetficha.INIrPagina     ,
                                  OUTPUT vchrMensaje   ).
MESSAGE 
    "OK1"
    "nUnionSocio: " nUnionSocio SKIP
    "nCajaSocio : " nCajaSocio SKIP
    "nSucursalSocio: " nSucursalSocio SKIP
    "nNumeroSocio: " nNumeroSocio SKIP
    "sServicio: " sServicio SKIP
    "vchrMensaje: " vchrMensaje SKIP
    "sAbrePagina: " sAbrePagina SKIP
    "bRequiereCajero: " bRequiereCajero
VIEW-AS ALERT-BOX.                                  
    IF vchrMensaje <> "" AND vchrMensaje <> ? THEN
        ASSIGN 
            ttEncdatdetficha.respuesta = vchrMensaje.
               
    IF sMensajeError <> "" THEN 
    DO:
        ASSIGN 
            ttEncdatdetficha.respuesta = sMensajeError.
    /*CTRLSOFT*
  </script><script language="JavaScript">
  alert("`sMensajeError`");               
  </script><SCRIPT language="SpeedScript">
  *CTRLSOFT*/   
    END.
    IF sAbrePagina<>"" THEN 
    DO:
        RUN VALUE(sAbrePagina).
    /*CTRLSOFT* 
    </script><script language="JavaScript">
    window.open("`sAbrePagina`","CuerpoDetalle");              
    </script><SCRIPT language="SpeedScript">
    *CTRLSOFT*/ 
    END.
    IF bRequiereCajero=yes THEN 
    DO:
        RUN validacajero.R.
    /*CTRLSOFT*
   </script><Script language= "JavaScript">
    validacajero();
      </script><script language = "SpeedScript">
      *CTRLSOFT*/
    END.       
END PROCEDURE.

PROCEDURE ContinuaPagina2:
    ASSIGN 
        sServicio = TRIM(CAPS(ttEncdatdetficha.INServicio)).
    /*CTRLSOFT * ASSIGN sServicio = TRIM(CAPS(get-value("Servicio"))). *CTRLSOFT*/
    RUN SaldoServicioSocio.
    CASE sTipoServicio:
        WHEN "PRE" THEN 
            DO:
                FIND FIRST DetalleFichaTemp  WHERE DetalleFichaTemp.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio)) AND 
                    /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) AND *CTRLSOFT*/
                    DetalleFichaTemp.IDUnion = nUnionFicha AND
                    DetalleFichaTemp.IDCajaPopular = nCajaFicha AND
                    DetalleFichaTemp.IDSucursal = nSucursalFicha AND
                    DetalleFichaTemp.Ejercicio = nEjercicio AND
                    DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
                    DetalleFichaTemp.IDTipoTransaccion = sTransaccion NO-LOCK NO-ERROR.
                IF AVAILABLE(DetalleFichaTemp) THEN 
                DO:
                    ASSIGN 
                        ttEncdatdetficha.respuesta = "Ya se ha realizado un abono en esta misma ficha para el pr�stamo " + sServicio.
                    /*CTRLSOFT*
                          </script><script language="JavaScript">
                           alert("Ya se ha realizado un abono en esta misma ficha para el pr�stamo "+"`sServicio`");
                           </script><script language = "SpeedScript">
                    *CTRLSOFT*/       
                    ASSIGN 
                        bContinua = NO.
                END.
                ELSE 
                DO:
                    RUN datosprestamocja.r.
                /*CTRLSOFT*
                </script><SCRIPT language="JavaScript">
                  window.open("datosprestamocja.r?sMode=INS&Servicio=`sServicio`","CuerpoDetalle");
                </script><script language="SpeedScript">
                *CTRLSOFT*/
                END.
            END.
        WHEN "INT" THEN 
            DO:
                FIND FIRST DetalleFichaTemp WHERE DetalleFichaTemp.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio)) AND
                    /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) AND *CTRLSOFT*/
                    DetalleFichaTemp.IDUnion = nUnionFicha AND
                    DetalleFichaTemp.IDCajaPopular = nCajaFicha AND
                    DetalleFichaTemp.IDSucursal = nSucursalFicha AND
                    DetalleFichaTemp.Ejercicio = nEjercicio AND
                    DetalleFichaTemp.IDCajaAsignada = nCajaAsignada AND
                    DetalleFichaTemp.IDTipoTransaccion = sTransaccion NO-LOCK NO-ERROR.
                IF AVAILABLE(DetalleFichaTemp) THEN 
                DO:
                    /*CTRLSOFT*
                  </script><script language="JavaScript">
                  alert("Ya se ha realizado un abono en esta misma ficha para el interes "+"`sServicio`");
                  </script><script language = "SpeedScript">
                  *CTRLSOFT*/
                    ASSIGN 
                        ttEncdatdetficha.respuesta = "Ya se ha realizado un abono en esta misma ficha para el interes " + sServicio.
                    ASSIGN 
                        bContinua = NO.
                END.
            END.
        WHEN "INV" THEN 
            DO:
                IF sServicio <> "IVGA" THEN 
                DO:
                    RUN datosinversionframe.r.
                /*CTRLSOFT*
              </script><Script language= "JavaScript">
               window.open("datosinversionframe.r?ClaseInversion=P","CuerpoDetalle");
               </script><script language = "SpeedScript">
               *CTRLSOFT*/
                END.
                ELSE 
                DO:
                    RUN datosinversionframe.r.
                /*CTRLSOFT*
              </script><Script language= "JavaScript">
               window.open("datosinversionframe.r?ClaseInversion=A","CuerpoDetalle");
                  </script><script language = "SpeedScript">
                  *CTRLSOFT*/
                END.
            END.
        WHEN "PSO" THEN 
            DO:
                IF sServicio = sServicioCapitalSocial AND nSaldoServicio = nLimiteCapitalSocial THEN 
                DO:
                    /*CTRLSOFT*
                      </script><script language="JavaScript">
                      alert("El Servicio "+"`sServicio`"+" tiene el saldo l�mite permitido, por lo que no puede seguir depositando en el");
                      </script><script language = "SpeedScript">
                      *CTRLSOFT*/
                    ASSIGN 
                        bContinua = NO.
                    ASSIGN 
                        ttEncdatdetficha.respuesta = "El Servicio " + sServicio + " tiene el saldo l�mite permitido, por lo que no puede seguir depositando en el".
                END.
            END.
        WHEN "DEU" THEN 
            DO:
                IF sServicio = sServicioChequeDevuelto OR sServicio = sServicioComisionBancaria OR sServicio = sServicioIndemnizacion THEN 
                DO:
                    RUN eligechequedevuelto.r.
                /*CTRLSOFT*
                 </script><Script language= "JavaScript">
                    window.open("eligechequedevuelto.r?sMode=INS&Servicio=`sServicio`","CuerpoDetalle");
                     </script><script language = "SpeedScript">
                     *CTRLSOFT*/
                END.
                ELSE IF sServicio = sServicioFaltanteCajero THEN 
                    DO:
                        bRequiereCajero=yes.
                        RUN validacajero.R.
                    /*CTRLSOFT*
                    </script><Script language= "JavaScript">
                     validacajero();
                        </script><script language = "SpeedScript">
                        *CTRLSOFT*/
                    END.
                    ELSE IF sServicio = sServicioInteresDocumentado THEN 
                        DO:
                            RUN cobranzapagareinteresdocumentado.r.
                        /*CTRLSOFT*
                        </script><Script language= "JavaScript">
                            window.open("cobranzapagareinteresdocumentado.r?Union=`sUnionSocio`&Caja=`sCajaSocio`&Sucursal=`sSucursalSocio`&Socio=`sNumeroSocio`","CuerpoDetalle");
                             </script><script language = "SpeedScript">
                             *CTRLSOFT*/
                        END.
                        ELSE
                            ASSIGN bContinua = yes.
            END.
        WHEN "CXP" THEN 
            DO:
                bRequiereCajero=yes.
                IF sServicio = sServicioSobranteCajero THEN 
                DO:
                    RUN validacajero.R.
                /*CTRLSOFT*
               </script><Script language= "JavaScript">
                 validacajero();
                   </script><script language = "SpeedScript">
                   *CTRLSOFT*/
                END.
                ELSE
                    ASSIGN bContinua = yes.
            END.
    END CASE.
END PROCEDURE.

PROCEDURE CreaMovimientosTemporales:
    /*--------------Servicios adiconales que generan IVA----------------------*/
    
    lGeneraIVA = no.
    sServGeneraIVA = ''.
    dIVAGenerado = 0.
    sDescIVAGenerado = ''.
    lPremiteMovto = YES.
    FIND FIRST ServicioGeneraIVA WHERE trim(caps(ServicioGeneraIVA.IDServicio)) = TRIM(CAPS(ttEncdatdetficha.INServicio)) AND
        /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) AND *CTRLSOFT*/
        ServicioGeneraIVA.CausaIVA = yes NO-LOCK NO-ERROR.
    IF AVAILABLE(ServicioGeneraIVA) THEN 
    DO:
        FIND FIRST servicio WHERE Servicio.IDServicio = ServicioGeneraIVA.IDServicioIVA NO-LOCK NO-ERROR.
        IF AVAILABLE(Servicio) THEN 
        do:
            sServGeneraIVA = ServicioGeneraIVA.IDServicioIVA.
            sDescIVAGenerado = "IVA POR PRESTACION DE SERVICIO".
            lGeneraIVA = yes.
        END.
        else 
        do:
            /*CTRLSOFT*
            </script><Script language= "JavaScript">
                 alert("No existe el servicio de IVA `caps(ServicioGeneraIVA.IDServicioIVA)` en el cat�logo de servicios para el servicio `caps(ServicioGeneraIVA.IDServicio)`");
            </script><script language = "SpeedScript">
            *CTRLSOFT*/
            ttEncdatdetficha.respuesta = "No existe el servicio de IVA " + caps(ServicioGeneraIVA.IDServicioIVA) + 
                " en el cat�logo de servicios para el servicio " + caps(ServicioGeneraIVA.IDServicio).
            lGeneraIVA = no.
            sServGeneraIVA = ''.
            sDescIVAGenerado = ''.
            dIVAGenerado = 0.
            lPremiteMovto = NO.                       
        END. /*else*/                    

    END. /*find ServicioGeneraIVA*/
    /*------------------------------------------------------------------------*/

    /*CTRLSOFT* IF get-value("cancelar") = "Cancelar" OR MaintOption  = "Cancelar" THEN DO: *CTRLSOFT*/
    IF ttEncdatdetficha.INcancelar = "Cancelar" OR MaintOption  = "Cancelar" THEN 
    DO:
        ASSIGN 
            Search-value = ""
            Navigate     = "Cancelar".
    END.
    /*CTRLSOFT* ELSE IF MaintOption = "Aceptar" or get-value("acepta") = "Aceptar" THEN DO: *CTRLSOFT*/     
    ELSE IF MaintOption = "Aceptar" or ttEncdatdetficha.INacepta = "Aceptar" THEN 
        DO:     
            /* {&out} "sMode = " sMode '<BR>'.*/
            CASE sMode:
                WHEN "CON" THEN 
                    DO:
                        MESSAGE "7.- ACEPTAR DO TRANSACTION"
                        VIEW-AS ALERT-BOX.
                        insertar:
                        DO TRANSACTION:         
                            /*CTRLSOFT*
                            ASSIGN sConcepto = TRIM(CAPS(get-value("Concepto":U))).
                            if get-value('hRNDepositante') <> "" then sConcepto = CAPS(get-value('hRNDepositante')).
                            if get-value('hRNFolio') <> "" then sConcepto = sConcepto + " " + CAPS(get-value('hRNFolio')).
                            IF get-value("Referencia") ="GA" THEN
                                sReferencia1="&" + get-value("Referencia") + "," +  STRING(INT(get-value("Contrato")),"99999").
                            IF INT(get-value("Cajero"))>0 THEN DO:
                                sReferencia1= "&CJ," + get-value("UCSCajero") + STRING(INT(get-value("Cajero")),"999").
                                sConcepto = "DEP.CJA." + get-value("Cajero") + "-" + get-value("NomCajero").
                            END.
                            *CTRLSOFT*/
                            ASSIGN 
                                sConcepto = TRIM(CAPS(ttEncdatdetficha.INConcepto)).
                            if ttEncdatdetficha.INAhRNDepositante <> "" then sConcepto = CAPS(ttEncdatdetficha.INAhRNDepositante).
                            if ttEncdatdetficha.INAhRNFolio <> "" then sConcepto = sConcepto + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                            IF ttEncdatdetficha.INAReferencia ="GA" THEN
                                sReferencia1="&" + ttEncdatdetficha.INAReferencia + "," +  STRING(INT(ttEncdatdetficha.INAContrato),"99999").
                            IF INT(ttEncdatdetficha.INACajero)>0 THEN 
                            DO:
                                sReferencia1= "&CJ," + ttEncdatdetficha.INAUCSCajero + STRING(INT(ttEncdatdetficha.INACajero),"999").
                                sConcepto = "DEP.CJA." + ttEncdatdetficha.INACajero + "-" + ttEncdatdetficha.INANomCajero.
                            END.
                            FIND FIRST Servicio WHERE Servicio.IDServicio= TRIM(CAPS(ttEncdatdetficha.INServicio)) 
                            /*CTRLSOFT*TRIM(CAPS(get-value("Servicio")))*CTRLSOFT*/ NO-LOCK NO-ERROR.
                            IF AVAILABLE (Servicio) THEN 
                            DO:
                                if Servicio.IDServicio = "CSBC" then
                                do:
                                    for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
                                        bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
                                        bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
                                        bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
                                        bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
                                        bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
                                        bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha no-lock:
                                        if bfDetalleFichaTemp_Validar.IDServicio = "CSBC" then
                                            ASSIGN ttEncdatdetficha.respuesta = "Ya existe un Movimiento a CSBC en la Ficha.".
                                        UNDO insertar, RETURN ERROR 'Ya existe un Movimiento a CSBC en la Ficha.'.
                                    end.
                                end.
                                ASSIGN 
                                    sServicio = Servicio.IDServicio.
                                IF sConcepto = "" THEN
                                    ASSIGN sConcepto = "DEP. " + Servicio.Descripcion.
                                /*CTRLSOFT* ASSIGN nImporte = DECIMAL(get-value("Importe")). *CTRLSOFT*/
                                ASSIGN 
                                    nImporte = DECIMAL(ttEncdatdetficha.INImporte).
                                IF Servicio.IDTipoServicio="PRE" THEN 
                                DO:
                                    RUN CreaMovimientosPrestamo NO-ERROR.
                                    /* rvargass, 20130809: Agrega Movto de Honorarios por LITIGIO*/
                                    for first bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
                                        bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
                                        bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
                                        bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
                                        bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
                                        bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
                                        bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha 
                                        and INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') <> 0
                                        no-lock:
                                        FIND FIRST PrestamoBloqueadoAbonoXComision WHERE PrestamoBloqueadoAbonoXComision.IDUnion = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 4,2))
                                            AND PrestamoBloqueadoAbonoXComision.IDCajaPopular = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 6,2))
                                            AND PrestamoBloqueadoAbonoXComision.IDSucursal = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 8,2))
                                            AND PrestamoBloqueadoAbonoXComision.IDPrestamo = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 10,6)) 
                                            and PrestamoBloqueadoAbonoXComision.FechaAutorizada = TODAY and PrestamoBloqueadoAbonoXComision.Activo = YES NO-LOCK NO-ERROR.
                                        if available(PrestamoBloqueadoAbonoXComision) then
                                        do:
                                            if PrestamoBloqueadoAbonoXComision.ImporteComision > 0 then
                                            do:
                                                nDetalleFicha = nDetalleFicha + 1.
                                                RUN GuardaDatos (INPUT "AHCC","DEPOSITO DE HONORARIOS POR LITIGIO",0,PrestamoBloqueadoAbonoXComision.ImporteComision,"","") NO-ERROR.
                                                IF ERROR-STATUS:ERROR THEN 
                                                DO:
                                                    ASSIGN 
                                                        ttEncdatdetficha.respuesta = "Error al Crear DetalleFichaTemp de HONORARIOS POR LITIGIO".
                                                    UNDO insertar, RETURN ERROR 'Error al Crear DetalleFichaTemp de HONORARIOS POR LITIGIO'.
                                                END.
                                            end.
                                        end.
                                    end.
                                    /*************************************************************/
                                    nCompletaConAHO_V = 0.
                                    for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
                                        bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
                                        bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
                                        bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
                                        bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
                                        bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
                                        bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha no-lock:
                                        nCompletaConAHO_V = nCompletaConAHO_V + bfDetalleFichaTemp_Validar.Abono.
                                    end.
                                    /*{&out} "nCompletaConAHO_V = " nCompletaConAHO_V '<BR>'.*/
                                    nCompletaConAHO_V = 1 - (nCompletaConAHO_V - TRUNCATE(nCompletaConAHO_V,0)).
                                    /*{&out} "nCompletaConAHO_V = " nCompletaConAHO_V '<BR>'.*/
                                    if nCompletaConAHO_V > 0 and nCompletaConAHO_V < 1 then
                                    do:
                                        nDetalleFicha = nDetalleFicha + 1.
                                        RUN GuardaDatos (INPUT "AHSO","DEPOSITO AL AHORRO",0,nCompletaConAHO_V,"","") NO-ERROR.
                                        IF ERROR-STATUS:ERROR THEN 
                                        DO:
                                            ASSIGN 
                                                ttEncdatdetficha.respuesta = "Error al Crear DetalleFichaTemp de nCompletaConAHO_V".
                                            UNDO insertar, RETURN ERROR 'Error al Crear DetalleFichaTemp de nCompletaConAHO_V'.
                                        END.
                                    end.
                                END.
                                ELSE IF Servicio.IDTipoServicio="PSO" AND nImporte <> 0 THEN 
                                    DO:
                                        nCajaSocio = int(sCajaSocio).
                                        nSucursalSocio = int(sSucursalSocio).
                                        RUN ValidaCapitalSocial NO-ERROR.
                                        IF ERROR-STATUS:ERROR THEN 
                                        DO:
                                            IF RETURN-VALUE <> '' THEN
                                                sMsgError = RETURN-VALUE.
                                            ELSE
                                                sMsgError = ' Al Validar el Capital'.
                                            ASSIGN 
                                                ttEncdatdetficha.respuesta = sMsgError.   
                                            /*CTRLSOFT*
                                            </script><SCRIPT language="JavaScript">
                                              alert("`sMsgError`");
                                            </script> <script language="SpeedScript">
                                            *CTRLSOFT*/
                                            UNDO insertar, RETURN ERROR.
                                        END.
                                        /*                {&out} "Servicio.IDServicio = " Servicio.IDServicio "|" lPromo80-20 "|" nImporte "|" nMontoCaso_Validar '<BR>'.*/
                                        /* /* lPromo80-20 */
                                        if /*Servicio.IDServicio = "CAFO" and*/ lPromo80-20 = YES then
                                        do:
                                            if nImporte >= nMontoCaso_Validar then
                                            do:
                                                nImporte_80-20 = ROUND((nMontoCaso_Validar * .20), 2).
                                                nDetalleFicha = nDetalleFicha + 1.
                                                RUN GuardaDatos (INPUT "GAGE","PROMO 80-20",nImporte_80-20,0,"","&P-PROMO8020") NO-ERROR.
                                                IF ERROR-STATUS:ERROR THEN DO:
                                                    UNDO insertar, RETURN ERROR 'Error al Crear DetalleFichaTemp para GAGE de PROMO_80-20'.
                                                END.
                                            end.
                                        end. /* if Servicio.IDServicio = "CAFO" and lPromo80-20 = YES then */
                                        */
                                        /*Promo70-30 solo Empleados.*/
                                        /*CTRLSOFT* if /*Servicio.IDServicio = "CAFO" and*/ lPromo80-20 = YES and get-value('jsFolioPromo') <> "NO" and get-value('jsFolioPromo') <> "" then *CTRLSOFT*/
                                        if /*Servicio.IDServicio = "CAFO" and*/ lPromo80-20 = YES and ttEncdatdetficha.jsFolioPromo <> "NO" 
                                            and ttEncdatdetficha.jsFolioPromo <> "" then
                                        do:
                                            if nImporte >= nMontoCaso_Validar then
                                            do:
                                                /*CTRLSOFT*
                                                sCodigoPromo7030-8020 = get-value('jsFolioPromo').
                                                *CTRLSOFT*/
                                                sCodigoPromo7030-8020 = ttEncdatdetficha.jsFolioPromo.
                                                nLine_Promo7030-8020 = INDEX(sCodigoPromo7030-8020,"|").
                                                sA_Promo7030-8020 = SUBSTRING(sCodigoPromo7030-8020,1,nLine_Promo7030-8020 - 1).
                                                sB_Promo7030-8020 = SUBSTRING(sCodigoPromo7030-8020,nLine_Promo7030-8020 + 1,LENGTH(sCodigoPromo7030-8020)).
                                                if sA_Promo7030-8020 = "70" then assign sS_Promo7030-8020   = "GA73" sC_Promo7030-8020   = "PROMO 70-30" nImporte_80-20      = ROUND((nMontoCaso_Validar * .30), 2) sPromo80-20_SoloEmp = "&P-PROMO7030" + "_" + sB_Promo7030-8020.
                                                if sA_Promo7030-8020 = "80" then assign sS_Promo7030-8020   = "GA82" sC_Promo7030-8020   = "PROMO 80-20" nImporte_80-20      = ROUND((nMontoCaso_Validar * .20), 2) sPromo80-20_SoloEmp = "&P-PROMO8020" + "_" + sB_Promo7030-8020.
                                                /*nImporte_80-20 = ROUND((nMontoCaso_Validar * .30), 2).*/
                                                nDetalleFicha = nDetalleFicha + 1.
                                                RUN GuardaDatos (INPUT sS_Promo7030-8020,sC_Promo7030-8020,nImporte_80-20,0,"",sPromo80-20_SoloEmp) NO-ERROR.
                                                IF ERROR-STATUS:ERROR THEN 
                                                DO:
                                                    ASSIGN 
                                                        ttEncdatdetficha.respuesta = "Error al Crear DetalleFichaTemp para GA73-82 de PROMO_7030-8020".
                                                    UNDO insertar, RETURN ERROR 'Error al Crear DetalleFichaTemp para GA73-82 de PROMO_7030-8020'.
                                                END.
                                            end.
                                        end. /* if Servicio.IDServicio = "CAFO" and lPromo80-20 = YES then */
                                    END.
                                    ELSE IF Servicio.IDTipoServicio="DEU" THEN 
                                        DO:
                                            /****************************************************************/
                                            /*
                                            if (sServicio = "CTIN" OR sServicio = "CTMD") then 
                                            do:
                                                nImporte = nImporte * (1 + (nTasaIVA_DF / 100)).
                                            end.
                                            */
                                            IF (sServicio = sServicioChequeDevuelto OR sServicio = sServicioComisionBancaria OR sServicio = sServicioIndemnizacion) THEN 
                                            DO:
                                                RUN CreaPagoChequeDevuelto.
                                            END.
                                            ELSE IF sServicio= sServicioInteresDocumentado THEN 
                                                DO:
                                                    /*CTRLSOFT* RUN GuardaDatos (INPUT sServicio,sConcepto,0,get-value("Abono"),"",sReferencia1) NO-ERROR. *CTRLSOFT*/
                                                    RUN GuardaDatos (INPUT sServicio,sConcepto,0,ttEncdatdetficha.INAbono,"",sReferencia1) NO-ERROR.
                                                    IF ERROR-STATUS:ERROR THEN 
                                                    DO:
                                                        IF RETURN-VALUE <> '' THEN 
                                                        DO:
                                                            ASSIGN 
                                                                ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                            UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                        END.
                                                        ELSE 
                                                        DO:
                                                            ASSIGN 
                                                                ttEncdatdetficha.respuesta = "Error devuelto por el interes documentado".
                                                            UNDO insertar, RETURN ERROR 'Error devuelto por el interes documentado'.
                                                        END.
                                                    END.
                                                /*Aqui no se define el llamado al calculo de iva porque este if es para servicios de prestamo*/
                                                END.
                                                ELSE 
                                                DO:
                                                    IF nImporte <> 0 and lPremiteMovto = yes THEN 
                                                    DO:
                                                        /*-----lGeneraIVA = yes -----*/
                                                        IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'DESGLOSE' THEN 
                                                        do:
                                                            run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nImporte, output dIVAGenerado) no-error.
                                                            IF dIVAGenerado > 0 and trim(sServGeneraIVA) <> '' THEN 
                                                            do:
                                                                /*nDetalleFicha = nDetalleFicha + 1.*/
                                                                RUN GuardaDatos (INPUT sServGeneraIVA,sDescIVAGenerado,0,dIVAGenerado,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha)) NO-ERROR.
                                                                IF ERROR-STATUS:ERROR THEN 
                                                                DO:
                                                                    IF RETURN-VALUE <> '' THEN 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                        UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                    END.
                                                                    ELSE 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos genera iva".
                                                                        UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos genera iva'.
                                                                    END.
                                                                END.
                                                                else 
                                                                do:
                                                                    nDetalleFicha = nDetalleFicha + 1.
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.aviso = "REVISA QUE EL CALCULO DE IVA ESTE CORRECTO".
                                                                /*CTRLSOFT* {&out} "REVISA QUE EL CALCULO DE IVA ESTE CORRECTO". *CTRLSOFT*/
                                                                END.


                                                            END. /*dIVAGenerado > 0*/                
                                                        END. /*lGeneraIVA*/

                                                        RUN GuardaDatos (INPUT sServicio,sConcepto,0,nImporte,"",sReferencia1) NO-ERROR.
                                                        IF ERROR-STATUS:ERROR THEN 
                                                        DO:
                                                            IF RETURN-VALUE <> '' THEN 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                            END.
                                                            ELSE 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos".
                                                                UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos'.
                                                            END.
                                                        END.

                                                    /***Verificar
                                                                                      /*-----lGeneraIVA = yes -----*/
                                                                                        IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'CALCULO' THEN do:
                                                                                                run CalculoIVAServicio(input nunion, input ncaja, input nsucursal, input nImporte, output dIVAGenerado) no-error.
                                                                                                IF dIVAGenerado > 0 and trim(sServGeneraIVA) <> '' THEN do:
                                                                                                    nDetalleFicha = nDetalleFicha + 1.
                                                                                                    RUN GuardaDatos (INPUT sServGeneraIVA,sDescIVAGenerado,0,dIVAGenerado,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha - 1)) NO-ERROR.
                                                                                                    IF ERROR-STATUS:ERROR THEN DO:
                                                                                                      IF RETURN-VALUE <> '' THEN
                                                                                                         UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                                                      ELSE DO:
                                                                                                         UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos genera iva'.
                                                                                                      END.
                                                                                                    END.
                                                                                                END. /*dIVAGenerado > 0*/                
                                                                                        END. /*lGeneraIVA*/
                                                    Verificar******/

                                                    END.
                                                END.
                                        END.
                                        ELSE 
                                        DO:
                                            IF nImporte <> 0 and lPremiteMovto = yes THEN 
                                            DO:
                                                /*-----lGeneraIVA = yes -----*/
                                                IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'DESGLOSE' THEN 
                                                do:
                        
                                                    run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, input-output nImporte, output dIVAGenerado) no-error.
                                                    IF dIVAGenerado > 0 and trim(sServGeneraIVA) <> '' THEN 
                                                    do:
                                                        /*nDetalleFicha = nDetalleFicha + 1.*/
                                                        RUN GuardaDatos (INPUT sServGeneraIVA,sDescIVAGenerado,0,dIVAGenerado,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha)) NO-ERROR.
                                                        IF ERROR-STATUS:ERROR THEN 
                                                        DO:
                                                            IF RETURN-VALUE <> '' THEN 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                            END.
                                                            ELSE 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos genera iva".
                                                                UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos genera iva'.
                                                            END.
                                                        END.
                                                        else 
                                                        do:
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                        /*{&out} "CALCULO DE IVA EN DEPOSITOS DE CAJAS".*/
                                                        END.
                                                    END. /*dIVAGenerado > 0*/
                            
                                                END. /*lGeneraIVA*/

                                                if sServicio = "CSEF" then
                                                do:
                                                    /*CTRLSOFT*sReferencia1 = "&F-CSEF," + CAPS(get-value('hRNFolio')).*CTRLSOFT*/
                                                    sReferencia1 = "&F-CSEF," + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                    /* Valida duplicidad en CSEF (con o sin el mismo folio). */
                                                    for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
                                                        bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
                                                        bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
                                                        bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
                                                        bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
                                                        bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
                                                        bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha no-lock:
                                                        if bfDetalleFichaTemp_Validar.IDServicio = "CSEF" then
                                                        do:
                                                            ASSIGN 
                                                                ttEncdatdetficha.respuesta = "Duplicidad de Servicio CSEF".
                                                            UNDO insertar, RETURN ERROR 'Duplicidad de Servicio CSEF'.
                                                        end.
                                                    end.
                    	
                                                end.
                                                RUN GuardaDatos (INPUT sServicio,sConcepto,0,nImporte,"",sReferencia1) NO-ERROR.
                                                IF ERROR-STATUS:ERROR THEN 
                                                DO:
                                                    IF RETURN-VALUE <> '' THEN
                                                        UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                    ELSE 
                                                    DO:
                                                        UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos'.
                                                    END.
                                                END.
                                                /*-----lGeneraIVA = yes -----*/
                                                IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'CALCULO' THEN 
                                                do:
                                                    run CalculoIVAServicio(input nunion, input ncaja, input nsucursal, input nImporte, output dIVAGenerado) no-error.
                                                    IF dIVAGenerado > 0 and trim(sServGeneraIVA) <> '' THEN 
                                                    do:
                                                        nDetalleFicha = nDetalleFicha + 1.
                                                        RUN GuardaDatos (INPUT sServGeneraIVA,sDescIVAGenerado,0,dIVAGenerado,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha - 1)) NO-ERROR.
                                                        IF ERROR-STATUS:ERROR THEN 
                                                        DO:
                                                            IF RETURN-VALUE <> '' THEN 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                            END.
                                                            ELSE 
                                                            DO:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos genera iva".
                                                                UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos genera iva'.
                                                            END.
                                                        END.
                                                    END. /*dIVAGenerado > 0*/

                                                END. /*lGeneraIVA*/
                                                /* 20130305, rvargass: Socio sin CASO completo no puede abonar al Ahorro. */
                                                if Servicio.IDTipoServicio = "AHO" and Servicio.IDServicio MATCHES ('AH*') then
                                                do:
                                                    find first Socio where Socio.IDUnion = nUnion and Socio.IDCajaPopular = nCaja and Socio.IDSucursal = int(sSucursalSocio)
                                                        and Socio.IDSocio = INT(sNumeroSocio) and (Socio.IDTipoSocio = "SO" or Socio.IDTipoSocio = "AS") no-lock no-error.
                                                    if available(Socio) then
                                                    do:
                                                        find first SaldoSocio where SaldoSocio.IDUnion = Socio.IDUnion and SaldoSocio.IDCajaPopular = Socio.IDCajaPopular and SaldoSocio.IDSucursal = Socio.IDSucursal
                                                            and SaldoSocio.IDSocio = Socio.IDSocio and SaldoSocio.Anio = YEAR(TODAY) and SaldoSocio.IDServicio = "CASO" no-lock no-error.
                                                        if available(SaldoSocio) then nSaldoCASO_V = SaldoSocio.Saldo.
                                                        if nSaldoCASO_V < nMontoCaso_Validar then
                                                        do:
                                                            ASSIGN 
                                                                ttEncdatdetficha.respuesta = "Socio SIN su Parte Social Competa. No puede Abonar al Ahorro.".
                                                            /*CTRLSOFT*
                                                            </script> <script language="JavaScript">
                                                            alert("Socio SIN su Parte Social Competa. No puede Abonar al Ahorro.");
                                                            </script> <script language="SpeedScript">
                                                            *CTRLSOFT*/
                                                            UNDO insertar, RETURN ERROR 'Socio SIN su Parte Social Competa. No puede Abonar al Ahorro.'.
                                                        end.
                                                    end.
                                                end.
                                                /* 20130808, rvargass: SocioBloqueadoMovHaberes. */
                                                if (Servicio.IDTipoServicio = "AHO" and Servicio.IDServicio MATCHES ('AH*')) or Servicio.IDServicio = "CSBC" then
                                                do:
                                                    find first SocioBloqueadoMovHaberes where SocioBloqueadoMovHaberes.IDUnion = nUnion and SocioBloqueadoMovHaberes.IDCajaPopular = nCaja
                                                        and SocioBloqueadoMovHaberes.IDSucursal = int(sSucursalSocio) and SocioBloqueadoMovHaberes.IDSocio = INT(sNumeroSocio)
                                                        and SocioBloqueadoMovHaberes.Activo = YES no-lock no-error.
                                                    if available(SocioBloqueadoMovHaberes) then
                                                    do:
                                                        ASSIGN 
                                                            ttEncdatdetficha.respuesta = "Socio Bloqueado por Juridico. No puede Abonar al Ahorro.".
                                                        /*CTRLSOFT*
                                                        </script> <script language="JavaScript">
                                                        alert("Socio Bloqueado por Juridico. No puede Abonar al Ahorro.");
                                                        </script> <script language="SpeedScript">
                                                        *CTRLSOFT*/
                                                        UNDO insertar, RETURN ERROR 'Socio Bloqueado por Juridico. No puede Abonar al Ahorro.'.
                                                    end.
                                                end.
                                                /* 20130304, rvargass: Mayoria de Edad para AHME. */
                                                if sServicio = "AHME" then
                                                do:
                                                    find first Socio where Socio.IDUnion = nUnion and Socio.IDCajaPopular = nCaja and Socio.IDSucursal = int(sSucursalSocio)
                                                        and Socio.IDSocio = INT(sNumeroSocio) and Socio.IDTipoSocio = "AM" no-lock no-error.
                                                    if available(Socio) then
                                                    do:
                                                        nEdadV = 0.
                                                        if Socio.DiscapacidadIntelectual = YES then
                                                        do:
                                                            find first SocioTutor where SocioTutor.IDUnion = Socio.IDUnion and SocioTutor.IDCajaPopular = Socio.IDCajaPopular and SocioTutor.IDSucursal = Socio.IDSucursal 
                                                                and SocioTutor.IDSocio = Socio.IDSocio no-lock no-error.
                                                            if available(SocioTutor) then nEdadV = 1.
                                                        end.
                                                        if Socio.FechaNacimiento <> ? and YEAR(Socio.FechaNacimiento) < YEAR(TODAY) and nEdadV = 0 then
                                                        do:
                                                            nEdadV = year(today) - year(Socio.FechaNacimiento).
                                                            if month(today) < month(Socio.FechaNacimiento) then nEdadV = nEdadV - 1.
                                                            if month(today) = month(Socio.FechaNacimiento) then
                                                            do:
                                                                if day(today) < day(Socio.FechaNacimiento) then nEdadV = nEdadV - 1.
                                                            end.
                                                            if nEdadV >= 18 then 
                                                            do:
                                                                ASSIGN 
                                                                    ttEncdatdetficha.respuesta = "Socio con Mayor�a de Edad. Ya no puede depositar a AHME. Favor de traspasarlo a SOCIO.".
                                                                /*CTRLSOFT*
                                                                </script> <script language="JavaScript">
                                                                alert("Socio con Mayor�a de Edad. Ya no puede depositar a AHME. Favor de traspasarlo a SOCIO.");
                                                                </script> <script language="SpeedScript">
                                                                *CTRLSOFT*/
                                                                UNDO insertar, RETURN ERROR 'Socio con Mayor�a de Edad. Ya no puede depositar a AHME. Favor de traspasarlo a SOCIO'.
                                                            end.
                                                        end.
                                                    end.
                                                end.

                                                /* 20111116, rvargass: Valida si el servicio requiere comision. */
                                                find first ServicioComision where ServicioComision.IDServicio = sServicio no-lock no-error.
                                                if available(ServicioComision) then
                                                do:
                                                    if INTEGER(sNumeroSocio) = 0 and ServicioComision.ImporteComNoSocio > 0 then
                                                    do:
                                                        sIDServicioVCom = ServicioComision.IDServicioComNoSocio.
                                                        sConceptoVCom = "COMISION POR ABONO A " + ServicioComision.IDServicio.	
						
                                                        /* INICIO: PROVI-3 */
                                                        FIND FIRST ServicioGeneraIVA where ServicioGeneraIVA.IDServicio = sIDServicioVCom
                                                            AND ServicioGeneraIVA.CausaIVA = YES 
                                                            NO-LOCK NO-ERROR.
                                                        IF AVAILABLE(ServicioGeneraIVA) THEN
                                                        DO:
                 
                                                            nImporteVCom = ServicioComision.ImporteComNoSocio.
                                                            RUN CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nImporteVCom, output IVAComisionQseCobra) NO-ERROR.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.
                                                            sConcepto = "IVA " + ServicioGeneraIVA.IDServicio.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT ServicioGeneraIVA.IDServicioIVA,sConcepto,0,IVAComisionQseCobra,"","").
                                                        END.
                                                        ElSE
                                                        DO:
                                                            nImporteVCom = ServicioComision.ImporteComNoSocio.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.

                                                        END.
                                                    end.  
                                                    /*FIN: PROVI-3 */
                                                    if INTEGER(sNumeroSocio) > 0 and ServicioComision.ImporteComSocio > 0 then
                                                    do:
                                                        sIDServicioVCom = ServicioComision.IDServicioComSocio.
                                                        sConceptoVCom = "COMISION POR ABONO A " + ServicioComision.IDServicio.
              
                                                        /* INICIO: PROVI-3 */
                                                        FIND FIRST ServicioGeneraIVA where ServicioGeneraIVA.IDServicio = sIDServicioVCom
                                                            AND ServicioGeneraIVA.CausaIVA = YES 
                                                            NO-LOCK NO-ERROR.
                                                        IF AVAILABLE(ServicioGeneraIVA) THEN
                                                        DO:
                                                            nImporteVCom = ServicioComision.ImporteComSocio.
                                                            RUN CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nImporteVCom, output IVAComisionQseCobra) NO-ERROR.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.
                                                            sConcepto = "IVA " + ServicioGeneraIVA.IDServicio.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT ServicioGeneraIVA.IDServicioIVA,sConcepto,0,IVAComisionQseCobra,"","").
                                                        END.
                                                        ElSE
                                                        DO:
                                                            nImporteVCom = ServicioComision.ImporteComSocio.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.

                                                        END.

                                                    /*FIN: PROVI-3 */  
                                                    end.
                                                end.
                                                /****************************************************************/
                                                /* 20130107, rvargass: Comision para Remesas Nacionales. */
                                                /*							<input type="hidden" name="hRNDepositante" id="hRNDepositante" size="14">
                                                                            <input type="hidden" name="hRNComision" id="hRNComision" size="14">
                                                                            <input type="hidden" name="hRNFolio" id="hRNFolio" size="14"> */
                                                /*CTRLSOFT* if get-value('hRNComision') <> "" and (sServicio = "RENA" or sServicio = "REMB") then *CTRLSOFT*/
                                                if ttEncdatdetficha.INAhRNComision <> "" and (sServicio = "RENA" or sServicio = "REMB") then
                                                do:
                                                    decimal(ttEncdatdetficha.INAhRNComision) no-error.
                                                    /*CTRLSOFT* decimal(get-value('hRNComision')) no-error. *CTRLSOFT*/
                                                    IF NOT ERROR-STATUS:ERROR THEN 
                                                    DO:
                                                        nImporteVCom = decimal(ttEncdatdetficha.INAhRNComision) no-error.
                                                        /*CTRLSOFT* nImporteVCom = decimal(get-value('hRNComision')) no-error. *CTRLSOFT*/
                                                        if nImporteVCom > 0 then
                                                        do:
                                                            nImporteVCom = ROUND(nImporteVCom,2).
                                                            if sServicio = "RENA" then
                                                            do:
                                                                sIDServicioVCom = "CRRN".
                                                                /*CTRLSOFT* sConceptoVCom = "COMISION REMESA NACIONAL " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                                sConceptoVCom = "COMISION REMESA NACIONAL " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                            end.
                                                            if sServicio = "REMB" then
                                                            do:
                                                                sIDServicioVCom = "CRRI".
                                                                /*CTRLSOFT* sConceptoVCom = "COMISION REMESA INTERNACIONAL " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                                sConceptoVCom = "COMISION REMESA INTERNACIONAL " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                            end.
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.
                                                            nImporteVCom = ROUND(nImporteVCom * nTasaIVA_DF / 100,2).
                                                            if nImporteVCom > 0 then
                                                            do:
                                                                if sServicio = "RENA" then
                                                                do:
                                                                    sIDServicioVCom = "IVRN".
                                                                    /*CTRLSOFT* sConceptoVCom = "IVA POR COMISION REMESA NACIONAL " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                                    sConceptoVCom = "IVA POR COMISION REMESA NACIONAL " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                                end.
                                                                if sServicio = "REMB" then
                                                                do:
                                                                    sIDServicioVCom = "IVRI".
                                                                    /*CTRLSOFT* sConceptoVCom = "IVA POR COMISION REMESA INTERNACIONAL " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                                    sConceptoVCom = "IVA POR COMISION REMESA INTERNACIONAL " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                                end.
                                                                nDetalleFicha = nDetalleFicha + 1.
                                                                RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                                IF ERROR-STATUS:ERROR THEN 
                                                                DO:
                                                                    IF RETURN-VALUE <> '' THEN 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                        UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                    END.
                                                                    ELSE 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                        UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                    END.
                                                                END.
                                                            end.
                                                        end.
                                                    END.
                                                end. /* if get-value('hRNComision') <> "" and (sServicio = "RENA" or sServicio = "REMB") then */
                                                /****************************************************************/
                                                /*CTRLSOFT* if get-value('hRNComision') <> "" and (sServicio = "CSEF") then *CTRLSOFT*/
                                                if ttEncdatdetficha.INAhRNComision <> "" and (sServicio = "CSEF") then
                                                do:
                                                    /*CTRLSOFT* decimal(get-value('hRNComision')) no-error. *CTRLSOFT*/
                                                    decimal(ttEncdatdetficha.INAhRNComision) no-error.
                                                    IF NOT ERROR-STATUS:ERROR THEN 
                                                    DO:
                                                        /*CTRLSOFT* nImporteVCom = decimal(get-value('hRNComision')) no-error. *CTRLSOFT*/
                                                        nImporteVCom = decimal(ttEncdatdetficha.INAhRNComision) no-error.
                                                        if nImporteVCom > 0 then
                                                        do:
                                                            nImporteVCom = ROUND(nImporteVCom,2).
                                                            nImporteVComAux = nImporteVCom.
                                                            nImporteVCom = nImporteVCom / (1 + (nTasaIVA_DF / 100)).
                                                            nImporteVCom = ROUND(nImporteVCom,2).
                                                            nImporteVComAux = nImporteVComAux - nImporteVCom.
                                                            sIDServicioVCom = "CCSE".
                                                            /*CTRLSOFT* sConceptoVCom = "COMISION COBRO " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                            sConceptoVCom = "COMISION COBRO " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                            nDetalleFicha = nDetalleFicha + 1.
                                                            RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                            IF ERROR-STATUS:ERROR THEN 
                                                            DO:
                                                                IF RETURN-VALUE <> '' THEN 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                    UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                END.
                                                                ELSE 
                                                                DO:
                                                                    ASSIGN 
                                                                        ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                    UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                END.
                                                            END.
                                                            /*nImporteVCom = ROUND(nImporteVCom * nTasaIVA_DF / 100,2).*/
                                                            nImporteVCom = nImporteVComAux.
                                                            if nImporteVCom > 0 then
                                                            do:
                                                                sIDServicioVCom = "ICCS".
                                                                sConceptoVCom = "IVA POR COMISION " + CAPS(ttEncdatdetficha.INAhRNDepositante) + " " + CAPS(ttEncdatdetficha.INAhRNFolio).
                                                                /*CTRLSOFT* sConceptoVCom = "IVA POR COMISION " + CAPS(get-value('hRNDepositante')) + " " + CAPS(get-value('hRNFolio')). *CTRLSOFT*/
                                                                nDetalleFicha = nDetalleFicha + 1.
                                                                RUN GuardaDatos (INPUT sIDServicioVCom,sConceptoVCom,0,nImporteVCom,"","") NO-ERROR.
                                                                IF ERROR-STATUS:ERROR THEN 
                                                                DO:
                                                                    IF RETURN-VALUE <> '' THEN 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = RETURN-VALUE.
                                                                        UNDO insertar, RETURN ERROR RETURN-VALUE.
                                                                    END.
                                                                    ELSE 
                                                                    DO:
                                                                        ASSIGN 
                                                                            ttEncdatdetficha.respuesta = "Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.".
                                                                        UNDO insertar, RETURN ERROR 'Error devuelto por el procedimiento GuardaDatos al Insertar una Comision.'.
                                                                    END.
                                                                END.
                                                            end.
                                                        end.
                                                    END.
                                                end. /* if get-value('hRNComision') <> "" and (sServicio = "CSEF") then */
                                            /****************************************************************/
                                            END.
                                        END.
                            END.
                            
                            MESSAGE "FIN ACEPTAR " SKIP
                            "bHuboError1: " bHuboError1
                            VIEW-AS ALERT-BOX.
          
     
                            IF bHuboError1 = yes THEN 
                            DO:
                                UNDO insertar, LEAVE insertar.
                            END.
                            ELSE 
                            DO:
                                IF sWhereAux <> "" THEN 
                                DO:
                                    ASSIGN 
                                        ttEncdatdetficha.aviso = "El registro insertado puede no cumplir con el filtro activo.El filtro ser� desactivado.".
                                /*CTRLSOFT*
                                    QUEUE-MESSAGE("informacion","El registro insertado puede no cumplir con el filtro activo.El filtro ser� desactivado.").
                                 *CTRLSOFT*/   
                                END.
                                ASSIGN 
                                    sWhere     = ""
                                    sWhereAux  = ""
                                    sFilter    = ""
                                    sFilterAux = "".   
                            END.
                        END.
                    END.
                WHEN "FIL" THEN 
                    DO:
                    /*CTRLSOFT*PENDIENTE*
                        ASSIGN ValueField[1] = TRIM(CAPS(GET-VALUE("{&tmTABLE}.{&PK}")))
                               ValueField[2] = TRIM(CAPS(GET-VALUE("{&tmTABLE}.{&FIELD1}")))
                               ValueField[3] = TRIM(CAPS(GET-VALUE("{&tmTABLE}.{&FIELD2}"))).
                        ASSIGN sFilter = "DES"
                               sFilterAux="".
                        RUN ConvierteCadena.
                        /* La variable sWhere ya se encuentra asignada desde el proc Convierte asignada
                           pero no se le concatena la clausula WHERE */
                        IF sWhere <> "" THEN DO:
                           ASSIGN sWhere = "WHERE " + sWhere
                                  sWhereAux= "WHERE " + sWhereAux.
                        END.
                        Navigate="Filtrar".
                        *CTRLSOFT*PENDIENTE*/
                    END.
            END CASE.
        END.
        /*CTRLSOFT* ELSE IF MaintOption = "Modificar" or get-value("Modificar")="Modificar" THEN DO: *CTRLSOFT*/
        ELSE IF MaintOption = "Modificar" or ttEncdatdetficha.INModificar = "Modificar" THEN 
            DO:
                dImporteMto = DECIMAL(ttEncdatdetficha.INImporte).
                /*CTRLSOFT* dImporteMto = DECIMAL(GET-VALUE("Importe")). *CTRLSOFT*/
                /*------------------Si modifica calculo-----------------------*/
                IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'DESGLOSE' THEN 
                do:
                    ASSIGN 
                        ttEncdatdetficha.aviso = "El importe seleccionado para el servicio " + TRIM(CAPS(ttEncdatdetficha.INServicio)) +
                                                   " desglosa automaticamente el iva calculado en el servicio " + sServGeneraIVA.
                    /*CTRLSOFT*
                    </script><SCRIPT language="JavaScript">
                     alert("El importe seleccionado para el servicio `TRIM(CAPS(get-value('Servicio')))` desglosa automaticamente el iva calculado en el servicio `sServGeneraIVA`");
                    </script> <script language="SpeedScript">
                    *CTRLSOFT*/
                    modificar:
                    DO TRANSACTION:
                        FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                            {&TABLE}.{&FK12} = nCajaFicha AND
                            {&TABLE}.{&FK13} = nSucursalFicha AND
                            {&TABLE}.{&FK16} = nEjercicio AND
                            {&TABLE}.{&FK18} = nCajaAsignada AND
                            {&TABLE}.{&FK14} = sTransaccion AND
                            {&TABLE}.{&FK15} = nFolioFicha AND
                            trim({&TABLE}.referencia) = trim("IVA," + string(nUnionFicha) + 
                            "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + 
                            string(nEjercicio) + "," + string(nCajaAsignada) + "," + 
                            sTransaccion + "," + string(nFolioFicha) + "," +
                            string(int(ttEncdatdetficha.INDetalleFicha) - 1)) 
                            /*CTRLSOFT* string(int(get-value("DetalleFicha")) - 1)) *CTRLSOFT*/
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
                        IF NOT ERROR-STATUS:ERROR THEN 
                        DO:          
                            dIVAGenerado = 0.
                            run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, input-output dImporteMto, output dIVAGenerado) no-error.   
                            IF dIVAGenerado > 0 THEN
                                ASSIGN  {&TABLE}.{&FIELD8} = dIVAGenerado.                                                          
                        END.   
                        IF bHuboError1 = yes THEN 
                        DO:
                            UNDO modificar, LEAVE modificar.
                        END.
                    END. /*transaccion*/
                end.  /*lgeneraIVA*/
                /*-------------------------------------------------------------*/

                modificar:
                DO TRANSACTION:
                    FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                        {&TABLE}.{&FK12} = nCajaFicha AND
                        {&TABLE}.{&FK13} = nSucursalFicha AND
                        {&TABLE}.{&FK16} = nEjercicio AND
                        {&TABLE}.{&FK18} = nCajaAsignada AND
                        {&TABLE}.{&FK14} = sTransaccion AND
                        {&TABLE}.{&FK15} = nFolioFicha AND
                        {&TABLE}.{&PK}   = INT(ttEncdatdetficha.INDetalleFicha) /*CTRLSOFT* INT(get-value("DetalleFicha")) *CTRLSOFT*/ 
                        EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
                    IF NOT ERROR-STATUS:ERROR THEN 
                    DO:           
                        FIND FIRST Servicio WHERE Servicio.IDServicio= DetalleFichaTemp.IDServicio  NO-LOCK NO-ERROR.
                        IF AVAILABLE (Servicio) THEN 
                        DO:
                            IF Servicio.IDTipoServicio="PRE" THEN 
                            DO:
                                ASSIGN 
                                    nInteresNormal = DECIMAL(ttEncdatdetficha.INInteresNormal)
                                    nInteresMora   = DECIMAL(ttEncdatdetficha.INInteresMora)
                                    nDescuento     = DECIMAL(ttEncdatdetficha.INDescuento).
                                /*CTRLSOFT*
                               ASSIGN nInteresNormal = DECIMAL(get-value("InteresNormal"))
                                      nInteresMora   = DECIMAL(get-value("InteresMora"))
                                      nDescuento     = DECIMAL(get-value("Descuento")).
                                      *CTRLSOFT*/
                                ASSIGN  
                                    {&TABLE}.{&FIELD8} = dImporteMto.
                                RUN ModificaDatos (INPUT (ttEncdatdetficha.INServicioIntNormal),0,nInteresNormal).
                                /*CTRLSOFT* RUN ModificaDatos (INPUT (get-value("ServicioIntNormal")),0,nInteresNormal). *CTRLSOFT*/
                                /*IF nDescuento<>0 THEN
                                  RUN ModificaDatos (INPUT sServicioDescuentoSC,nDescuento,0).*/
                                IF nInteresMora>0 THEN 
                                DO:
                                    ASSIGN 
                                        sServicio = ttEncdatdetficha.INServicioIntMora.
                                    /*CTRLSOFT* ASSIGN sServicio = get-value("ServicioIntMora"). *CTRLSOFT*/
                                    RUN ModificaDatos (INPUT sServicio,0,nInteresMora).
                                END.
                            END.
                            ELSE IF Servicio.IDTipoServicio="PSO" AND dImporteMto<> 0 THEN 
                                DO:
                                    sServicio = DetalleFichaTemp.IDServicio.
                                    nImporte=dImporteMto.
                                    /*CTRLSOFT* sConcepto=TRIM(CAPS(GET-VALUE("Concepto"))). *CTRLSOFT*/
                                    sConcepto=TRIM(CAPS(ttEncdatdetficha.INConcepto)).
                                    nDetalleFicha = DetalleFichaTemp.IDDetalleFicha.
                                    delete DetalleFichaTemp NO-ERROR.
                                    RUN ValidaCapitalSocial.
                                END.
                                ELSE 
                                DO:                                                   
                                    IF DetalleFichaTemp.IDServicio = sServicioInteresDocumentado THEN 
                                    DO:
                                        ASSIGN  
                                            {&TABLE}.{&FIELD8} = DECIMAL(ttEncdatdetficha.INAbono).
                                    /*CTRLSOFT* ASSIGN  {&TABLE}.{&FIELD8} = DECIMAL(GET-VALUE("Abono")). *CTRLSOFT*/                    
                                    END.
                                    ELSE 
                                    DO:
                                        /*
                                        if (Servicio.IDServicio = "CTIN" OR Servicio.IDServicio = "CTMD") then 
                                        do:
                                            dImporteMto = dImporteMto * (1 + (nTasaIVA_DF / 100)).
                                            ASSIGN {&TABLE}.{&FIELD6} = TRIM(CAPS(GET-VALUE("Concepto"))).
                                            ASSIGN  {&TABLE}.{&FIELD8} = dImporteMto.
                                        end.
                                        else
                                        do:
                                        */
                                        ASSIGN 
                                            {&TABLE}.{&FIELD6} = TRIM(CAPS(ttEncdatdetficha.INConcepto)).
                                        /*CTRLSOFT* ASSIGN {&TABLE}.{&FIELD6} = TRIM(CAPS(GET-VALUE("Concepto"))). *CTRLSOFT*/
                                        ASSIGN  
                                            {&TABLE}.{&FIELD8} = dImporteMto.
                                    /*end.*/
                                    END.    
                                END.
                        END.
                        if DetalleFichaTemp.Abono > 0 then
                        do:
                            find first ServicioLimitaMovimiento where ServicioLimitaMovimiento.idservicio = DetalleFichaTemp.IDServicio
                                and ServicioLimitaMovimiento.IDTipoMovimiento = "A"
                                no-lock no-error.
                            if available(ServicioLimitaMovimiento) then
                                if DetalleFichaTemp.Abono < ServicioLimitaMovimiento.LimiteInferior or DetalleFichaTemp.Abono > ServicioLimitaMovimiento.LimiteSuperior then
                                do:
                                    ASSIGN 
                                        ttEncdatdetficha.respuesta = "Importe Fuera de los L�mites para el Servicio.".
                                    /*CTRLSOFT*
                                    </script> <script language="JavaScript">
                                    alert("Importe Fuera de los L�mites para el Servicio.");
                                    </script> <script language="SpeedScript">
                                    *CTRLSOFT*/
                                    UNDO modificar, LEAVE modificar.
                                end.
                        end. /* if DetalleFichaTemp.Abono > 0 then */
                        find first ServicioLimitaSaldo where ServicioLimitaSaldo.IDServicio = DetalleFichaTemp.IDServicio
                            no-lock no-error.
                        if available(ServicioLimitaSaldo) then
                        do:
                            lOmiteValidacionS = NO.
                            if ServicioLimitaSaldo.IDServicio = "AHME" then
                            do:
                                find first SocioOmiteValidacion where SocioOmiteValidacion.IDValidacion = 1 and SocioOmiteValidacion.IDUnion = DetalleFichaTemp.IDUnionSocio and SocioOmiteValidacion.IDCajaPopular = DetalleFichaTemp.IDCajaPopularSocio
                                    and SocioOmiteValidacion.IDSucursal = DetalleFichaTemp.IDSucursalSocio and SocioOmiteValidacion.IDSocio = DetalleFichaTemp.IDSocio and SocioOmiteValidacion.Activo = YES no-lock no-error.
                                if available(SocioOmiteValidacion) then lOmiteValidacionS = YES.
                            end.
                            if lOmiteValidacionS = NO then
                            do:
                                nSaldoValida = 0.
                                find first SaldoSocio where SaldoSocio.IDUnion = DetalleFichaTemp.IDUnionSocio
                                    and SaldoSocio.IDCajaPopular = DetalleFichaTemp.IDCajaPopularSocio
                                    and SaldoSocio.IDSucursal = DetalleFichaTemp.IDSucursalSocio
                                    and SaldoSocio.IDSocio = DetalleFichaTemp.IDSocio
                                    and SaldoSocio.Anio = YEAR(TODAY)
                                    and SaldoSocio.IDServicio = ServicioLimitaSaldo.IDServicio
                                    no-lock no-error.
                                if available(SaldoSocio) then nSaldoValida = SaldoSocio.Saldo.
                                for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = DetalleFichaTemp.IDUnion
                                    and bfDetalleFichaTemp_Validar.IDCajaPopular = DetalleFichaTemp.IDCajaPopular
                                    and bfDetalleFichaTemp_Validar.IDSucursal = DetalleFichaTemp.IDSucursal
                                    and bfDetalleFichaTemp_Validar.Ejercicio = DetalleFichaTemp.Ejercicio
                                    and bfDetalleFichaTemp_Validar.IDCajaAsignada = DetalleFichaTemp.IDCajaAsignada
                                    and bfDetalleFichaTemp_Validar.IDTipoTransaccion = DetalleFichaTemp.IDTipoTransaccion
                                    and bfDetalleFichaTemp_Validar.IDFicha = DetalleFichaTemp.IDFicha
                                    and bfDetalleFichaTemp_Validar.IDServicio = ServicioLimitaSaldo.IDServicio
                                    no-lock:
                                    find first bfServicio_Validar where bfServicio_Validar.IDServicio = bfDetalleFichaTemp_Validar.IDServicio no-lock no-error.
                                    if available(bfServicio_Validar) then
                                    do:
                                        if bfServicio_Validar.Clasificacion = "ACR" then nSaldoValida = nSaldoValida - bfDetalleFichaTemp_Validar.Cargo + bfDetalleFichaTemp_Validar.Abono.
                                        else nSaldoValida = nSaldoValida + bfDetalleFichaTemp_Validar.Cargo - bfDetalleFichaTemp_Validar.Abono.
                                    end.
                                end.
                                if (nSaldoValida < ServicioLimitaSaldo.LimiteInferior) or (nSaldoValida > ServicioLimitaSaldo.LimiteSuperior) then
                                do:
                                    ASSIGN 
                                        ttEncdatdetficha.respuesta = "SALDO Fuera de los L�mites para el Servicio. Limite Inferior " + STRING(ServicioLimitaSaldo.LimiteInferior) + 
										                           ". Limite Superior " + STRING(ServicioLimitaSaldo.LimiteSuperior).
                                    /*CTRLSOFT*
                                    </script> <script language="JavaScript">
                                    alert("SALDO Fuera de los L�mites para el Servicio. Limite Inferior `ServicioLimitaSaldo.LimiteInferior`. Limite Superior `ServicioLimitaSaldo.LimiteSuperior`");
                                    </script> <script language="SpeedScript">
                                    *CTRLSOFT*/
                                    UNDO modificar, LEAVE modificar.
                                end.
                            end. /* if lOmiteValidacionS = NO then */
                        end. /* if available(ServicioLimitaSaldo) then */
                    END.   
                    IF bHuboError1 = yes THEN 
                    DO:
                        UNDO modificar, LEAVE modificar.
                    END.
                END. /*de la transaccion*/
                            
                /*------------------Si modifica calculo-----------------------*/
                IF lGeneraIVA = yes AND trim(caps(stipocalciva)) = 'CALCULO' THEN 
                do:
                    modificar:
                    DO TRANSACTION:
                        FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                            {&TABLE}.{&FK12} = nCajaFicha AND
                            {&TABLE}.{&FK13} = nSucursalFicha AND
                            {&TABLE}.{&FK16} = nEjercicio AND
                            {&TABLE}.{&FK18} = nCajaAsignada AND
                            {&TABLE}.{&FK14} = sTransaccion AND
                            {&TABLE}.{&FK15} = nFolioFicha AND
                            trim({&TABLE}.referencia) = trim("IVA," + string(nUnionFicha) + 
                            "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + 
                            string(nEjercicio) + "," + string(nCajaAsignada) + "," + 
                            sTransaccion + "," + string(nFolioFicha) + "," + 
                            ttEncdatdetficha.INDetalleFicha) 
                            /*CTRLSOFT* get-value("DetalleFicha")) *CTRLSOFT*/
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
                        IF NOT ERROR-STATUS:ERROR THEN 
                        DO:           
                                                      
                            dIVAGenerado = 0.
                            /*CTRLSOFT* run CalculoIVAServicio(input nunion, input ncaja, input nsucursal, input DECIMAL(GET-VALUE("Importe")), output dIVAGenerado) no-error. *CTRLSOFT*/   
                            run CalculoIVAServicio(input nunion, input ncaja, input nsucursal, 
                                input DECIMAL(ttEncdatdetficha.INImporte), output dIVAGenerado) no-error.
                            IF dIVAGenerado > 0 THEN
                                ASSIGN  {&TABLE}.{&FIELD8} = dIVAGenerado.                                                          
                        END.   
                        IF bHuboError1 = yes THEN 
                        DO:
                            UNDO modificar, LEAVE modificar.
                        END.
                    END. /*transaccion*/
                end.  /*lgeneraIVA*/
            /*-------------------------------------------------------------*/
    
            END.
            /*CTRLSOFT* ELSE IF MaintOption = "Borrar" or get-value("Borrar")="Borrar" THEN DO: *CTRLSOFT*/
            ELSE IF MaintOption = "Borrar" or ttEncdatdetficha.INBBorrar ="Borrar" THEN 
                DO:     
                    Borrar:
                    DO TRANSACTION:
                        FIND {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                            {&TABLE}.{&FK12} = nCajaFicha AND
                            {&TABLE}.{&FK13} = nSucursalFicha AND
                            {&TABLE}.{&FK16} = nEjercicio AND
                            {&TABLE}.{&FK18} = nCajaAsignada AND
                            {&TABLE}.{&FK14} = sTransaccion AND
                            {&TABLE}.{&FK15} = nFolioFicha AND
                            {&TABLE}.{&PK} = INT(ttEncdatdetficha.INDetalleFicha)
                            /*CTRLSOFT* INT(get-value("DetalleFicha")) *CTRLSOFT*/ 
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
                        IF AVAILABLE({&TABLE}) THEN 
                        DO:
                            ASSIGN 
                                sServicio = {&TABLE}.{&FK21}.
                            RUN BorraDatos (INPUT sServicio,{&TABLE}.{&PK}).
                            FIND Servicio WHERE Servicio.IDServicio= sServicio NO-LOCK NO-ERROR.
                            IF AVAILABLE (Servicio) THEN 
                            DO:
                                IF Servicio.IDTipoServicio="PRE" THEN 
                                DO:
                                    ASSIGN 
                                        nInteresNormal = DECIMAL(ttEncdatdetficha.INInteresNormal)
                                        nInteresMora   = DECIMAL(ttEncdatdetficha.INInteresMora)
                                        nDescuento     = DECIMAL(ttEncdatdetficha.INDescuento).
                                    ASSIGN 
                                        sServicio = ttEncdatdetficha.INServicioIntNormal.
                                    /*CTRLSOFT*
                                   ASSIGN nInteresNormal = DECIMAL(get-value("InteresNormal"))
                                          nInteresMora   = DECIMAL(get-value("InteresMora"))
                                          nDescuento     = DECIMAL(get-value("Descuento")).
                                   ASSIGN sServicio = get-value("ServicioIntNormal"). *CTRLSOFT*/                            
                                    RUN BorraDatos (INPUT sServicio,0).
                                    IF nDescuento<>0 THEN
                                        RUN BorraDatos (INPUT sServicioDescuentoSC,0).
                                    IF nInteresMora>0 THEN 
                                    DO:
                                        ASSIGN 
                                            sServicio = ttEncdatdetficha.INServicioIntMora.
                                        /*CTRLSOFT* ASSIGN sServicio = get-value("ServicioIntMora"). *CTRLSOFT*/
                                        RUN BorraDatos (INPUT sServicio,0).
                                    END.
                                END.
                            END.
                        END.
                        IF bHuboError1 = yes THEN 
                        DO:
                            UNDO Borrar, LEAVE Borrar.
                        END. 
                    END. /* Del DO TRANSACTION ... */
                    /*------------------Si modifica calculo-----------------------*/
                    IF lGeneraIVA = yes THEN 
                    do:
                        Borrar:
                        DO TRANSACTION:
                            FIND {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                                {&TABLE}.{&FK12} = nCajaFicha AND
                                {&TABLE}.{&FK13} = nSucursalFicha AND
                                {&TABLE}.{&FK16} = nEjercicio AND
                                {&TABLE}.{&FK18} = nCajaAsignada AND
                                {&TABLE}.{&FK14} = sTransaccion AND
                                {&TABLE}.{&FK15} = nFolioFicha AND
                                trim({&TABLE}.referencia) = trim("IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + 
                                string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + 
                                sTransaccion + "," + string(nFolioFicha) + "," + ttEncdatdetficha.INDetalleFicha)  
                                /*CTRLSOFT* get-value("DetalleFicha")) *CTRLSOFT*/
                                EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
                            IF AVAILABLE({&TABLE}) THEN 
                            DO:
                                RUN BorraDatos(INPUT sServicio,{&TABLE}.{&PK}) no-error.
                            END.
                            IF bHuboError1 = yes THEN 
                            DO:
                                UNDO Borrar, LEAVE Borrar.
                            END. 
                        END. /* Del DO TRANSACTION ... */
                    end.  /*lgeneraIVA*/
                /*-------------------------------------------------------------*/
                END. /* Del ELSE IF ... */
    lValidaSocioUsuario = NO.
    FOR EACH Usuario WHERE Usuario.IDUsuarioEncode = sIDUsuario no-lock:
        /*
        FOR EACH Empleado WHERE Empleado.IDUnionEmpleadoSocio       =  DetalleFichaTemp.IDUnionSocio
                            AND Empleado.IDCajaPopularEmpleadoSocio =  DetalleFichaTemp.IDCajaPopularSocio
                            AND Empleado.IDSucursalEmpleadoSocio    =  DetalleFichaTemp.IDSucursalSocio
                            AND Empleado.IDSocio                    =  DetalleFichaTemp.IDSocio no-lock:
            IF Empleado.Nombre = Usuario.NombreUsuario 
               AND Empleado.PrimerApellido = Usuario.PrimerApellido
               AND Empleado.SegundoApellido = Usuario.SegundoApellido  THEN
            DO:
                lValidaSocioUsuario = YES.
            END. /*IF Empleado.Nombre = Usuario.NombreUsuario */
        END. /*FOR EACH Empleado WHERE Empleado.IDUnionEmpleadoSocio       =  {&TABLE}.{&FK1}*/
        */
        find first Empleado where Empleado.RFCEmpleado = Usuario.RFCEmpleado no-lock no-error.
        if available(Empleado) then
        do:
            if Empleado.IDUnionEmpleadoSocio = DetalleFichaTemp.IDUnionSocio
                and Empleado.IDCajaPopularEmpleadoSocio = DetalleFichaTemp.IDCajaPopularSocio
                and Empleado.IDSucursalEmpleadoSocio = DetalleFichaTemp.IDSucursalSocio
                and Empleado.IDSocio = DetalleFichaTemp.IDSocio then
            do:
                lValidaSocioUsuario = YES.
                sMotivoVal = "No puede Realizar Movimientos Por ser Usuario y Socio.".
            end.
            find first SocioNoAplicarMovto where SocioNoAplicarMovto.IDUnionEmp = Empleado.IDUnionEmpleadoSocio and SocioNoAplicarMovto.IDCajaPopularEmp = Empleado.IDCajaPopularEmpleadoSocio 
                and SocioNoAplicarMovto.IDSucursalEmp = Empleado.IDSucursalEmpleadoSocio and SocioNoAplicarMovto.IDSocioEmp = Empleado.IDSocio
                and SocioNoAplicarMovto.IDUnionSocio = DetalleFichaTemp.IDUnionSocio and SocioNoAplicarMovto.IDCajaPopularSocio = DetalleFichaTemp.IDCajaPopularSocio 
                and SocioNoAplicarMovto.IDSucursalSocio = DetalleFichaTemp.IDSucursalSocio and SocioNoAplicarMovto.IDSocio = DetalleFichaTemp.IDSocio and SocioNoAplicarMovto.Activo = YES no-lock no-error.
            if available(SocioNoAplicarMovto) then
            do:
                lValidaSocioUsuario = YES.
                sMotivoVal = "No puede Realizar Movimientos. --" + SocioNoAplicarMovto.Motivo + "-- ".
            end.
        end.
    END. /*FOR EACH Usuario WHERE Usuario.IDUsuarioEncode = sUsuario:*/

    if lValidaSocioUsuario = YES and DetalleFichaTemp.IDSocio <> 0 then
    do:
        ASSIGN
            ttEncdatdetficha.respuesta = sMotivoVal.
        /*CTRLSOFT*
        </script> <script language="JavaScript">
        alert("`sMotivoVal`");
        </script> <script language="SpeedScript">
        *CTRLSOFT*/
        RUN EliminaMovimientos no-error.
    end.
    /*************************************/

    find first SocioBloqueado where SocioBloqueado.IDUnion = INT(sUnionSocio)
        and SocioBloqueado.IDCajaPopular = INT(sCajaSocio)
        and SocioBloqueado.IDSucursal = INT(sSucursalSocio)
        and SocioBloqueado.IDSocio = INT(sNumeroSocio)
        no-lock no-error.
    if available(SocioBloqueado) then
    do:
        /*CTRLSOFT*
    </script> <script language="JavaScript">
    alert("Error de Sistema. Favor de comunicarse al Departemento de Sistemas.");
    </script> <script language="SpeedScript">
    *CTRLSOFT*/
        RUN EliminaMovimientos no-error.
    end.
	

    find first Socio where Socio.IDUnion = INT(sUnionSocio)
        and Socio.IDCajaPopular = INT(sCajaSocio)
        and Socio.IDSucursal = INT(sSucursalSocio)
        and Socio.IDSocio = INT(sNumeroSocio)
        and Socio.IDTipoSocio = "AS"
        no-lock no-error.
    if available(Socio) then
    do:
        lDocumentosCompletos = YES.
        for each DocumentoAfiliacion where DocumentoAfiliacion.IDUnion = nUnion
            and DocumentoAfiliacion.IDCajaPopular = nCaja
            and DocumentoAfiliacion.TipoRequisito = "OB"
            no-lock:
            find first SocioDocumento where SocioDocumento.IDUnion = Socio.IDUnion
                and SocioDocumento.IDCajaPopular = Socio.IDCajaPopular
                and SocioDocumento.IDSucursal = Socio.IDSucursal
                and SocioDocumento.IDSocio = Socio.IDSocio
                and SocioDocumento.IDDocumentoAfiliacion = DocumentoAfiliacion.IDDocumentoAfiliacion
                no-lock no-error.
            if not available(SocioDocumento) then
                lDocumentosCompletos = NO.
        end.
        /*
        find first SaldoSocio where SaldoSocio.IDUnion = Socio.IDUnion
                and SaldoSocio.IDCajaPopular = Socio.IDCajaPopular
                and SaldoSocio.IDSucursal = Socio.IDSucursal
                and SaldoSocio.IDSocio = Socio.IDSocio
                and SaldoSocio.Anio = YEAR(TODAY)
                and SaldoSocio.IDServicio = "CAFO"
                no-lock no-error.
        if available(SaldoSocio) then
            nMontoCaso_Validar = nMontoCaso_Validar - SaldoSocio.Saldo.
        */
        /*
        for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
                bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
                bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
                bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
                bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
                bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
                bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha no-lock:
            if bfDetalleFichaTemp_Validar.IDServicio = "CAFO" then
            do.
                nMontoCaso_Validar = nMontoCaso_Validar - bfDetalleFichaTemp_Validar.Abono.
            end.
        end.
        */
        /*
        if nMontoCaso_Validar <= 0 then
        do:
        */
        if lDocumentosCompletos = NO then
        do:
            ASSIGN
                ttEncdatdetficha.respuesta = "ASPIRANTE NO tiene Completos los Documentos para su Expediente, Favor de Validar.".
            /*CTRLSOFT*
            </script> <script language="JavaScript">
            alert("ASPIRANTE NO tiene Completos los Documentos para su Expediente, Favor de Validar.");
            </script> <script language="SpeedScript">
            *CTRLSOFT*/
            RUN EliminaMovimientos no-error.
        end.
    /*
    end.
    */
    end.
    /* Servcio Bloqueado */
    /*CTRLSOFT* if TRIM(CAPS(get-value("Servicio"))) = "SPHI" then *CTRLSOFT*/
    if TRIM(CAPS(ttEncdatdetficha.INServicio)) = "SPHI" then
    do:
        ASSIGN 
            ttEncdatdetficha.respuesta = "SERVICIO BLOQUEADO.". 
        /*CTRLSOFT*
        </script> <script language="JavaScript">
        alert("SERVICIO BLOQUEADO.");
        </script> <script language="SpeedScript">
        *CTRLSOFT*/
        RUN EliminaMovimientos no-error.
    end.
    /* Cantidad m�nima a Completar Abono */
    for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha AND
        bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha AND
        bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha AND
        bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio AND
        bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada AND
        bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion AND
        bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha 
        AND bfDetalleFichaTemp_Validar.Abono > 0
        no-lock:
        IF INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') <> 0 THEN 
        DO:
            FIND FIRST Prestamo WHERE Prestamo.IDUnion = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 4,2))
                AND Prestamo.IDCajaPopular = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 6,2))
                AND Prestamo.IDSucursal = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 8,2))
                AND Prestamo.IDPrestamo = INT(SUBSTRING(bfDetalleFichaTemp_Validar.Referencia,INDEX(bfDetalleFichaTemp_Validar.Referencia,'&PR') + 10,6)) NO-ERROR.
            IF AVAILABLE(Prestamo) THEN
            DO:
                FOR FIRST ServicioSituacionTipoPrestamo WHERE ServicioSituacionTipoPrestamo.IDServicio = Prestamo.IDServicio
                    AND ServicioSituacionTipoPrestamo.IDServicioCapital = bfDetalleFichaTemp_Validar.IDServicio NO-LOCK:
                    nMontoAbono_Validar = bfDetalleFichaTemp_Validar.Abono.
                    nAbonosCompleto_Validar = 0.
                    for each PlanPago of Prestamo where PlanPago.AbonoPagado = NO no-lock:
                        /*{&out} "nMontoAbono_Validar = " nMontoAbono_Validar '<BR>'.
                        {&out} "(PlanPago.MontoAbono - PlanPago.MontoPagado) = " (PlanPago.MontoAbono - PlanPago.MontoPagado) '<BR>'.*/
                        if nMontoAbono_Validar > 0 then
                        do:
                            if (PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar < 10 and (PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar > 0 then
                            do:
                                if nAbonosCompleto_Validar = 0 then
                                do:
                                    ASSIGN 
                                        ttEncdatdetficha.respuesta = "NO Completo su Abono. Faltan �nicamente " + string(((PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar),"$->>>.99").
                                /*CTRLSOFT*
                                </script> <script language="JavaScript">
                                alert("NO Completo su Abono. Faltan �nicamente `string(((PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar),'$->>>.99')`");
                                </script> <script language="SpeedScript">
                                *CTRLSOFT*/
                                end.
                                if nAbonosCompleto_Validar > 0 then
                                do:
                                    ASSIGN 
                                        ttEncdatdetficha.respuesta = "Cubre " + STRING(nAbonosCompleto_Validar) + " abono(s). Faltan �nicamente " + 
								                                        STRING(((PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar),"$->>>.99") + 
								                                        " para completar uno m�s. FAVOR DE CONSIDERAR.".
                                /*CTRLSOFT*
                                </script> <script language="JavaScript">
                                alert("Cubre `nAbonosCompleto_Validar` abono(s). Faltan �nicamente `string(((PlanPago.MontoAbono - PlanPago.MontoPagado) - nMontoAbono_Validar),'$->>>.99')` para completar uno m�s. FAVOR DE CONSIDERAR.");
                                </script> <script language="SpeedScript">
                                *CTRLSOFT*/
                                end.
                            end.
                            nMontoAbono_Validar = nMontoAbono_Validar - (PlanPago.MontoAbono - PlanPago.MontoPagado).
                            nAbonosCompleto_Validar = nAbonosCompleto_Validar + 1.
                        end.
                    end.
                END.
            END.
        END.
    end.
    /*************************************/
    lValidaSocDepGob = NO.
    lEntroDepGob = NO.
    for each DependenciaGobiernoServicio where DependenciaGobiernoServicio.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio))
    /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) *CTRLSOFT*/ no-lock:
        lEntroDepGob = YES.
        find first SocioDependenciaGobierno where SocioDependenciaGobierno.IDUnion = INT(sUnionSocio)
            and SocioDependenciaGobierno.IDCajaPopular = INT(sCajaSocio)
            and SocioDependenciaGobierno.IDSucursal = INT(sSucursalSocio)
            and SocioDependenciaGobierno.IDSocio = INT(sNumeroSocio)
            and SocioDependenciaGobierno.IDDependenciaGobierno = DependenciaGobiernoServicio.IDDependenciaGobierno
            and SocioDependenciaGobierno.EsActivo = YES
            no-lock no-error.
        if available(SocioDependenciaGobierno) then lValidaSocDepGob = YES.
    end.
    if lEntroDepGob = YES then
    do:
        if lValidaSocDepGob = NO then
        do:
            ASSIGN 
                ttEncdatdetficha.respuesta = "Servicio perteneciente a una relaci�n con Dependencia de Gobierno, el Socio NO tiene esa Dependencia.".
            /*CTRLSOFT*
            </script> <script language="JavaScript">
            alert("Servicio perteneciente a una relaci�n con Dependencia de Gobierno, el Socio NO tiene esa Dependencia.");
            </script> <script language="SpeedScript">
            *CTRLSOFT*/
            RUN EliminaMovimientos no-error.
        end.
        else
        do:
            find first PlanPagoAhorro where PlanPagoAhorro.IDUnion = INT(sUnionSocio)
                and PlanPagoAhorro.IDCajaPopular = INT(sCajaSocio)
                and PlanPagoAhorro.IDSucursal = INT(sSucursalSocio)
                and PlanPagoAhorro.IDSocio = INT(sNumeroSocio)
                and PlanPagoAhorro.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio))
                /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) *CTRLSOFT*/
                no-lock no-error.
            if not available(PlanPagoAhorro) then
            do:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "Socio Necesita un Plan de Pago para el Ahorro.".
                /*CTRLSOFT*
                </script> <script language="JavaScript">
                alert("Socio Necesita un Plan de Pago para el Ahorro.");
                </script> <script language="SpeedScript">
                *CTRLSOFT*/
                RUN EliminaMovimientos no-error.
            end.
        end.
    end.
/*CTRLSOFT* PENDIENTE
  </script><SCRIPT language="JavaScript">
     window.open("deberessociocja.r?CajaAsignada=`nCajaAsignada`&FolioFicha=`nFolioFicha`","DeberesSocio");
  </script> <script language="SpeedScript">
  *CTRLSOFT*/
END PROCEDURE.

PROCEDURE GuardaDatos:
    DEFINE INPUT PARAMETER sServicio1 AS CHAR.
    DEFINE INPUT PARAMETER sConcepto1 AS CHAR.
    DEFINE INPUT PARAMETER nCargo AS DECIMAL.
    DEFINE INPUT PARAMETER nAbono AS DECIMAL.
    DEFINE INPUT PARAMETER sServicioF AS CHAR.
    DEFINE INPUT PARAMETER sReferencia AS CHAR.
    nSaldoAcumulado=0.
    banError = NO.

            
    /*CTRLSOFT* IF sServicio = 'IVPF' and nAbono > 0 and trim(get-value('sref')) <> '' THEN *CTRLSOFT*/
    IF sServicio = 'IVPF' and nAbono > 0 and trim(ttEncdatdetficha.INBsref) <> '' THEN
        sReferencia = sReferencia + '&IV,' + string(nUnionFicha,'99') + string(nCajaFicha,'99') + string(nSucursalFicha,'99') +
            ttEncdatdetficha.INBsref 
            /*CTRLSOFT* get-value('sref') *CTRLSOFT*/ .
    FOR EACH {&TABLE} WHERE DetalleFichaTemp.IDServicio = sServicio1 AND
        {&TABLE}.{&FK11} = nUnionFicha AND
        {&TABLE}.{&FK12} = nCajaFicha AND
        {&TABLE}.{&FK13} = nSucursalFicha AND
        {&TABLE}.{&FK16} = nEjercicio AND
        {&TABLE}.{&FK18} = nCajaAsignada AND
        {&TABLE}.{&FK14} = sTransaccion NO-LOCK:
  
        IF sClasificacion = "ACR"  AND bSaldoSocio = yes THEN
            nSaldoAcumulado = nSaldoAcumulado - {&TABLE}.{&FIELD8} + {&TABLE}.{&FIELD7}.
        ELSE
            nSaldoAcumulado = nSaldoAcumulado + {&TABLE}.{&FIELD8} - {&TABLE}.{&FIELD7}.
         
    END.

    if nAbono > 0 then
    do:
        find first ServicioLimitaMovimiento where ServicioLimitaMovimiento.idservicio = sServicio
            and ServicioLimitaMovimiento.IDTipoMovimiento = "A"
            no-lock no-error.
        if available(ServicioLimitaMovimiento) then
            if nAbono < ServicioLimitaMovimiento.LimiteInferior or nAbono > ServicioLimitaMovimiento.LimiteSuperior then
            do:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "Importe Fuera de los L�mites para el Servicio.".
                /*CTRLSOFT*
                </script> <script language="JavaScript">
                alert("Importe Fuera de los L�mites para el Servicio.");
                </script> <script language="SpeedScript">
                *CTRLSOFT*/
                RETURN ERROR 'Abono Fuera del Limite Establecido'.
            end.
    end.
    CREATE {&TABLE} NO-ERROR.
    IF ERROR-STATUS:ERROR THEN 
    DO:
        ASSIGN 
            bHuboError = yes.
        RETURN ERROR 'Hubo un error al crear el registro en detallefichatemporal'.
    END.
    ELSE 
    DO:
  
        IF nCargo = 0 AND nAbono = 0 THEN
        DO:
            ASSIGN 
                bHuboError = yes.
        END.
        ELSE 
        DO:
     
            ASSIGN 
                {&TABLE}.{&FK11} = nUnionFicha
                {&TABLE}.{&FK12} = nCajaFicha
                {&TABLE}.{&FK13} = nSucursalFicha
                {&TABLE}.{&FK16} = nEjercicio
                {&TABLE}.{&FK18} = nCajaAsignada
                {&TABLE}.{&FK14} = sTransaccion
                {&TABLE}.{&FK15} = nFolioFicha
                {&TABLE}.{&PK}   = nDetalleFicha
                {&TABLE}.{&PK2}  = TODAY NO-ERROR.
            ASSIGN 
                {&TABLE}.{&FIELD1} = INT(sUnionSocio)
                {&TABLE}.{&FIELD2} = INT(sCajaSocio)
                {&TABLE}.{&FIELD3} = INT(sSucursalSocio)
                {&TABLE}.{&FIELD4} = INT(sNumeroSocio)
                {&TABLE}.{&FIELD6} = sConcepto1
                {&TABLE}.{&FIELD7} = nCargo
                {&TABLE}.{&FIELD8} = nAbono NO-ERROR.
            ASSIGN 
                {&TABLE}.{&FK21} = sServicio1 NO-ERROR.
        
        
            IF bSocioForaneo = YES THEN
                ASSIGN {&TABLE}.{&FIELD5} = sInversionComun.
            ELSE
                ASSIGN {&TABLE}.{&FIELD5} = "".
            IF ERROR-STATUS:ERROR THEN
                RETURN ERROR 'Error de asignaci�n de valores en detallefichatemp'.
            FIND Servicio WHERE Servicio.IDServicio = sServicio1 NO-LOCK NO-ERROR.
            IF AVAILABLE(Servicio) THEN
                ASSIGN sClasificacion = Servicio.Clasificacion.
           
           
            FIND SaldoSocio Where SaldoSocio.IDUnion = INT(sUnionSocio) AND
                SaldoSocio.IDCajaPopular = INT(sCajaSocio) AND
                SaldoSocio.IDSucursal = INT(sSucursalSocio) AND
                SaldoSocio.IDSocio = INT(sNumeroSocio) AND
                SaldoSocio.Anio = nEjercicio AND
                SaldoSocio.IDServicio=sServicio1 NO-LOCK no-error.
            IF ERROR-STATUS:ERROR THEN 
            DO:
                banError = yes.
                ASSIGN 
                    {&TABLE}.{&FIELD9} = 0.
            END.
            IF AVAILABLE (SaldoSocio) THEN
                ASSIGN {&TABLE}.{&FIELD9} = SaldoSocio.Saldo + nSaldoAcumulado.
            ELSE
                ASSIGN {&TABLE}.{&FIELD9} = 0.
            ASSIGN 
                {&TABLE}.{&FIELD9} = 0.
            ASSIGN 
                DetalleFichaTemp.Referencia = sReferencia.
        END.
    END.
    IF ERROR-STATUS:ERROR AND banError=NO THEN 
    DO:
        ASSIGN 
            ttEncdatdetficha.respuesta = "Hubo un error al intentar crear el registro. El registro no ser� creado".
        /*CTRLSOFT* QUEUE-MESSAGE("error","Hubo un error al intentar crear el registro.~\n
                     El registro no ser� creado").
        *CTRLSOFT*/             
        ASSIGN 
            bHuboError1 = yes.
    END.
    ELSE
        ASSIGN bHuboError1 = NO.
    find first ServicioLimitaSaldo where ServicioLimitaSaldo.IDServicio = sServicio
        no-lock no-error.
    if available(ServicioLimitaSaldo) then
    do:
        lOmiteValidacionS = NO.
        if ServicioLimitaSaldo.IDServicio = "AHME" then
        do:
            find first SocioOmiteValidacion where SocioOmiteValidacion.IDValidacion = 1 and SocioOmiteValidacion.IDUnion = INT(sUnionSocio) and SocioOmiteValidacion.IDCajaPopular = INT(sCajaSocio)
                and SocioOmiteValidacion.IDSucursal = INT(sSucursalSocio) and SocioOmiteValidacion.IDSocio = INT(sNumeroSocio) and SocioOmiteValidacion.Activo = YES no-lock no-error.
            if available(SocioOmiteValidacion) then lOmiteValidacionS = YES.
        end.
        if lOmiteValidacionS = NO then
        do:
            nSaldoValida = 0.
            find first SaldoSocio where SaldoSocio.IDUnion = INT(sUnionSocio)
                and SaldoSocio.IDCajaPopular = INT(sCajaSocio)
                and SaldoSocio.IDSucursal = INT(sSucursalSocio)
                and SaldoSocio.IDSocio = INT(sNumeroSocio)
                and SaldoSocio.Anio = YEAR(TODAY)
                and SaldoSocio.IDServicio = ServicioLimitaSaldo.IDServicio
                no-lock no-error.
            if available(SaldoSocio) then nSaldoValida = SaldoSocio.Saldo.
            for each bfDetalleFichaTemp_Validar where bfDetalleFichaTemp_Validar.IDUnion = nUnionFicha
                and bfDetalleFichaTemp_Validar.IDCajaPopular = nCajaFicha
                and bfDetalleFichaTemp_Validar.IDSucursal = nSucursalFicha
                and bfDetalleFichaTemp_Validar.Ejercicio = nEjercicio
                and bfDetalleFichaTemp_Validar.IDCajaAsignada = nCajaAsignada
                and bfDetalleFichaTemp_Validar.IDTipoTransaccion = sTransaccion
                and bfDetalleFichaTemp_Validar.IDFicha = nFolioFicha
                and bfDetalleFichaTemp_Validar.IDServicio = ServicioLimitaSaldo.IDServicio
                no-lock:
                find first bfServicio_Validar where bfServicio_Validar.IDServicio = bfDetalleFichaTemp_Validar.IDServicio no-lock no-error.
                if available(bfServicio_Validar) then
                do:
                    if bfServicio_Validar.Clasificacion = "ACR" then nSaldoValida = nSaldoValida - bfDetalleFichaTemp_Validar.Cargo + bfDetalleFichaTemp_Validar.Abono.
                    else nSaldoValida = nSaldoValida + bfDetalleFichaTemp_Validar.Cargo - bfDetalleFichaTemp_Validar.Abono.
                end.
            end.
            if (nSaldoValida < ServicioLimitaSaldo.LimiteInferior) or (nSaldoValida > ServicioLimitaSaldo.LimiteSuperior) then
            do:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "SALDO Fuera de los L�mites para el Servicio.".
                /*CTRLSOFT*
                </script> <script language="JavaScript">
                alert("SALDO Fuera de los L�mites para el Servicio.");
                </script> <script language="SpeedScript">
                *CTRLSOFT*/
                RETURN ERROR 'Abono Fuera del Limite Establecido'.
            end.
        end. /* if lOmiteValidacionS = NO then */
    end. /* if available(ServicioLimitaSaldo) then */
     
END PROCEDURE.


PROCEDURE ModificaDatos:
    DEFINE INPUT PARAMETER sServicio1 AS CHAR.
    DEFINE INPUT PARAMETER nCargo AS DECIMAL.
    DEFINE INPUT PARAMETER nAbono AS DECIMAL.
    DO Transaction:
        IF nCargo <> 0 THEN
            FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                {&TABLE}.{&FK12} = nCajaFicha AND
                {&TABLE}.{&FK13} = nSucursalFicha AND
                {&TABLE}.{&FK16} = nEjercicio AND
                {&TABLE}.{&FK18} = nCajaAsignada AND
                {&TABLE}.{&FK14} = sTransaccion AND
                {&TABLE}.{&FK15} = nFolioFicha AND                     
                {&TABLE}.{&FK21} = sServicio1 AND
                {&TABLE}.{&FIELD8}= 0 EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
        ELSE
            FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                {&TABLE}.{&FK12} = nCajaFicha AND
                {&TABLE}.{&FK13} = nSucursalFicha AND
                {&TABLE}.{&FK16} = nEjercicio AND
                {&TABLE}.{&FK18} = nCajaAsignada AND
                {&TABLE}.{&FK14} = sTransaccion AND
                {&TABLE}.{&FK15} = nFolioFicha AND                                          
                {&TABLE}.{&FK21} = sServicio1 AND
                {&TABLE}.{&FIELD7}= 0 EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
        IF AVAILABLE {&TABLE} THEN 
        DO:
            ASSIGN  
                {&TABLE}.{&FIELD7} = nCargo
                {&TABLE}.{&FIELD8} = nAbono NO-ERROR.
            IF ERROR-STATUS:ERROR THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "Hubo un error al intentar actualizar el registro.El registro no ser� actualizado".
                /*CTRLSOFT* QUEUE-MESSAGE("error","Hubo un error al intentar actualizar el registro.El registro no ser� actualizado"). *CTRLSOFT*/
                RELEASE {&TABLE}.
                ASSIGN 
                    bHuboError1 = yes.
                RETURN ERROR 'Hubo un error al intentar actualizar el registro'. 
            END.
            ELSE 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro fue modificado exitosamente.".
                /*CTRLSOFT* QUEUE-MESSAGE("informacion","El registro fue modificado exitosamente."). *CTRLSOFT*/
                ASSIGN 
                    bHuboError1 = NO.
            END.
        END.
        ELSE 
        DO:
            IF LOCKED({&TABLE}) THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro est� siendo usado por otro usuario en la red.El registro no ser� actualizado".
                /*CTRLSOFT* QUEUE-MESSAGE("bloqueo","El registro est� siendo usado por otro ~
                               usuario en la red.El registro no ser� actualizado"). *CTRLSOFT*/
                ASSIGN 
                    bHuboError1 = yes.
                RETURN ERROR 'El registro est� siendo usado por otro usuario'.
            END.
            ELSE 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro no existe.".
                /*CTRLSOFT* QUEUE-MESSAGE("error","El registro no existe."). *CTRLSOFT*/
                ASSIGN 
                    bHuboError1 = yes.
                RETURN ERROR 'Hubo un error al localizar el registro, no existe'.
            END.
        END.
    END. /* ...del transaction */
END PROCEDURE.

PROCEDURE BorraDatos:
    DO TRANSACTION:
        DEFINE INPUT PARAMETER sServicio1 AS CHAR.
        DEFINE INPUT PARAMETER nFolioDetalle AS INT.
        IF nFolioDetalle =0 THEN 
        DO:
            FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                {&TABLE}.{&FK12} = nCajaFicha AND
                {&TABLE}.{&FK13} = nSucursalFicha AND
                {&TABLE}.{&FK16} = nEjercicio AND
                {&TABLE}.{&FK18} = nCajaAsignada AND
                {&TABLE}.{&FK14} = sTransaccion AND
                {&TABLE}.{&FK15} = nFolioFicha AND                                          
                {&TABLE}.{&FK21} = sServicio1 EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
        END.
        ELSE 
        DO:
            FIND FIRST {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
                {&TABLE}.{&FK12} = nCajaFicha AND
                {&TABLE}.{&FK13} = nSucursalFicha AND
                {&TABLE}.{&FK16} = nEjercicio AND
                {&TABLE}.{&FK18} = nCajaAsignada AND
                {&TABLE}.{&FK14} = sTransaccion AND
                {&TABLE}.{&FK15} = nFolioFicha AND                                          
                {&TABLE}.{&PK} = nFolioDetalle EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
        END.
        IF AVAILABLE {&TABLE} THEN 
        DO:
            DELETE {&TABLE} NO-ERROR.
            IF ERROR-STATUS:ERROR THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "Hubo un error al intentar borrar el registro. El registro no ser� borrado".
                /*CTRLSOFT* QUEUE-MESSAGE("error","Hubo un error al intentar borrar el registro.~
                                              El registro no ser� borrado"). *CTRLSOFT*/
                RELEASE {&TABLE}.
                ASSIGN 
                    bHuboError1 = yes.
            END.
            ELSE 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro fue borrado de la base de datos.".
                /*CTRLSOFT* QUEUE-MESSAGE("informacion","El registro fue borrado de la base de datos."). *CTRLSOFT*/
                ASSIGN 
                    bHuboError1 = NO.
            END.
        END.
        ELSE 
        DO:
            IF LOCKED({&TABLE}) THEN 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro est� siendo usado por otro usuario en la red.~nEl registro no ser� borrado.".
            /*CTRLSOFT*
               QUEUE-MESSAGE("bloqueo","El registro est� siendo usado por otro ~
                                           usuario en la red.~nEl registro no ser� borrado.").
            *CTRLSOFT*/                               
            END.
            ELSE 
            DO:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "El registro no existe.".
            /*CTRLSOFT*
                QUEUE-MESSAGE("error","El registro no existe.").
            *CTRLSOFT*/
            END.    
            ASSIGN 
                bHuboError1 = yes.
        END.
    END.
END PROCEDURE.


PROCEDURE CreaFichaDeposito:
    DEFINE VAR sMensaje AS CHAR.
    creafichadeposito:
    DO TRANSACTION:
        RUN CreaDetalleFicha NO-ERROR.
        IF ERROR-STATUS:ERROR THEN 
        DO:
            IF RETURN-VALUE <> '' THEN
                sMensaje = RETURN-VALUE.
            ELSE
                sMensaje = 'Hubo un error en el procedimiento creadetalleficha'.
            ASSIGN 
                ttEncdatdetficha.respuesta = sMensaje. 
            UNDO creafichadeposito, RETURN ERROR sMensaje.
        END.
    /*CTRLSOFT*
    </script><SCRIPT language="JavaScript">
       window.open("haberessociocja.r?AnclarSaldo=NO","HaberesSocio");
       window.open("deberessociocja.r?AnclarSaldo=NO","DeberesSocio");
       window.open("despliegaficha.r?CobroAcumulado=`get-value('CobroAcumulado')`&TotalAcumulado=`get-value('TotalAcumulado')`&NumeroFichaPuntos=`nNumeroFichaPuntos`","CuerpoDetalle");
       window.close();
   </script> <script language="SpeedScript">
   *CTRLSOFT*/
    END.
END PROCEDURE.


PROCEDURE CreaMovtoTempIN:
    IF sMode = "MTO" THEN 
    DO:
        FIND {&TABLE} WHERE {&TABLE}.{&FK11} = nUnionFicha AND
            {&TABLE}.{&FK12} = nCajaFicha AND
            {&TABLE}.{&FK13} = nSucursalFicha AND
            {&TABLE}.{&FK16} = nEjercicio AND
            {&TABLE}.{&FK18} = nCajaAsignada AND
            {&TABLE}.{&FK14} = sTransaccion AND
            {&TABLE}.{&FK15} = nFolioFicha AND
            {&TABLE}.{&PK}   = INT(ttEncdatdetficha.INDetalleFicha) /*CTRLSOFT* INT(get-value("DetalleFicha")) *CTRLSOFT*/
                            
            NO-LOCK NO-ERROR.
        IF AVAILABLE({&TABLE}) THEN 
        DO:
            ASSIGN 
                {&tmTABLE}.{&FK11} = {&TABLE}.{&FK11}.
            ASSIGN 
                {&tmTABLE}.{&FK12} = {&TABLE}.{&FK12}.
            ASSIGN 
                {&tmTABLE}.{&FK13} = {&TABLE}.{&FK13}.
            ASSIGN 
                {&tmTABLE}.{&FK16} = {&TABLE}.{&FK16}.
            ASSIGN 
                {&tmTABLE}.{&FK18} = {&TABLE}.{&FK18}.
            ASSIGN 
                {&tmTABLE}.{&FK14} = {&TABLE}.{&FK14}.
            ASSIGN 
                {&tmTABLE}.{&FK15} = {&TABLE}.{&FK15}.
            ASSIGN 
                {&tmTABLE}.{&PK} = {&TABLE}.{&PK}.
            ASSIGN 
                {&tmTABLE}.{&PK2} = {&TABLE}.{&PK2}.
            ASSIGN 
                {&tmTABLE}.{&FK21} = {&TABLE}.{&FK21}.
            ASSIGN 
                {&tmTABLE}.{&FIELD1}  = {&TABLE}.{&FIELD1}
                {&tmTABLE}.{&FIELD2}  = {&TABLE}.{&FIELD2}
                {&tmTABLE}.{&FIELD3}  = {&TABLE}.{&FIELD3}
                {&tmTABLE}.{&FIELD4}  = {&TABLE}.{&FIELD4}
                {&tmTABLE}.{&FIELD5}  = {&TABLE}.{&FIELD5}
                {&tmTABLE}.{&FIELD6}  = {&TABLE}.{&FIELD6}
                {&tmTABLE}.{&FIELD7}  = {&TABLE}.{&FIELD7}
                {&tmTABLE}.{&FIELD8}  = {&TABLE}.{&FIELD8}
                {&tmTABLE}.{&FIELD9}  = {&TABLE}.{&FIELD9}
                {&tmTABLE}.{&FIELD10} = {&TABLE}.{&FIELD10}.
            FIND {&TABLE2} WHERE {&TABLE2}.{&FK21} = {&tmTABLE}.{&FK21} NO-LOCK NO-ERROR.
            IF AVAILABLE({&TABLE2}) THEN.
            ASSIGN 
                {&tmTABLE2}.{&FIELD1FK2} = {&TABLE2}.{&FIELD1FK2}.          
        END.
    END. 
    ELSE 
    DO: /* 1 */
        IF sMode = "CON" THEN 
        DO:
            MESSAGE "5.- CreaMovtoTempIN aqui entro antes de ASSIGN"
            VIEW-AS ALERT-BOX.
            /*CTRLSOFT* ASSIGN {&tmTABLE}.{&FK21} =  TRIM(CAPS(get-value("Servicio"))) no-error. *CTRLSOFT*/
            ASSIGN 
                {&tmTABLE}.{&FK21} = TRIM(CAPS(ttEncdatdetficha.INServicio)) no-error.
            IF bSocioForaneo = YES THEN
                ASSIGN {&tmTABLE}.{&FIELD5} = sInversionComun.
            ELSE
                ASSIGN  {&tmTABLE}.{&FIELD5} = "".
            /*CTRLSOFT* ASSIGN {&tmTABLE}.{&FIELD6} = get-value("Concepto":U). *CTRLSOFT*/
            ASSIGN 
                {&tmTABLE}.{&FIELD6} = ttEncdatdetficha.INConcepto.
            FIND Servicio WHERE Servicio.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio))
            /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) *CTRLSOFT*/ NO-LOCK NO-ERROR.
            IF AVAILABLE (Servicio) THEN 
            DO:
                /*CTRLSOFT* IF TRIM(get-value("Concepto":U))="" THEN *CTRLSOFT*/
                IF TRIM(ttEncdatdetficha.INConcepto) = "" THEN
                    ASSIGN {&tmTABLE}.{&FIELD6} = sDescripcion1 + " " + Servicio.Descripcion.
                ASSIGN 
                    sClasificacion = Servicio.Clasificacion
                    bSaldoSocio    = Servicio.SaldoSocio.
                ASSIGN 
                    {&tmTABLE2}.{&FIELD1FK2} = Servicio.Descripcion.
            END.
            FOR EACH {&TABLE} WHERE DetalleFichaTemp.IDServicio = TRIM(CAPS(ttEncdatdetficha.INServicio)) AND
                            /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) AND *CTRLSOFT*/
                {&TABLE}.{&FK11} = nUnionFicha AND
                {&TABLE}.{&FK12} = nCajaFicha AND
                {&TABLE}.{&FK13} = nSucursalFicha AND
                {&TABLE}.{&FK16} = nEjercicio AND
                {&TABLE}.{&FK18} = nCajaAsignada AND
                {&TABLE}.{&FK14} = sTransaccion NO-LOCK:
                IF sClasificacion = "ACR"  AND bSaldoSocio = yes THEN
                    nSaldoAcumulado = nSaldoAcumulado + {&TABLE}.{&FIELD8} - {&TABLE}.{&FIELD7}.
                ELSE
                    nSaldoAcumulado = nSaldoAcumulado - {&TABLE}.{&FIELD8} + {&TABLE}.{&FIELD7}.
            END.
            FIND SaldoSocio Where SaldoSocio.IDUnion       = INT(sUnionSocio) AND~
                SaldoSocio.IDCajaPopular = INT(sCajaSocio) AND~
                SaldoSocio.IDSucursal    = INT(sSucursalSocio) AND~
                SaldoSocio.IDSocio       = INT(sNumeroSocio) AND~
                SaldoSocio.Anio          = nEjercicio AND~
                SaldoSocio.IDServicio    = TRIM(CAPS(ttEncdatdetficha.INServicio))
                /*CTRLSOFT* TRIM(CAPS(get-value("Servicio"))) *CTRLSOFT*/
                NO-LOCK NO-ERROR.
            IF AVAILABLE (SaldoSocio) THEN 
            DO:
                ASSIGN 
                    {&tmTABLE}.{&FIELD9} = SaldoSocio.Saldo + nSaldoAcumulado NO-ERROR.
                ASSIGN 
                    {&tmTABLE}.{&FIELD9} = 0.
                dtUltCargo = SaldoSocio.FechaUltimoCargo.
                dtUltAbono = SaldoSocio.FechaUltimoAbono.
            END.
            ELSE
                ASSIGN {&tmTABLE}.{&FIELD9} = 0 NO-ERROR.
                
            MESSAGE "5.1 TERMINO"
            VIEW-AS ALERT-BOX.    
        END.
        ELSE 
        DO: /* 2 */ 
            ASSIGN 
                {&tmTABLE2}.{&FIELD1FK2} = ""
                {&tmTABLE}.{&FIELD5}     = ""
                {&tmTABLE}.{&FIELD6}     = ""
                {&tmTABLE}.{&FIELD9}     = 0
                {&tmTABLE}.{&FIELD10}    = "" NO-ERROR.
        END.
        ASSIGN 
            {&tmTABLE}.{&FK11} = nUnionFicha NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK12} = nCajaFicha NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK13} = nSucursalFicha NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK16} = nEjercicio NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK18} = nCajaAsignada NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK14} = sTransaccion NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FK15} = nFolioFicha NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&PK} = nDetalleFicha NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&PK2} = TODAY NO-ERROR.
        /*CTRLSOFT*
        ASSIGN {&tmTABLE}.{&FIELD1} = INT(cookie-value('cookie',"UnionSocio")) NO-ERROR.
        ASSIGN {&tmTABLE}.{&FIELD2} = INT(cookie-value('cookie',"CajaSocio")) NO-ERROR.
        ASSIGN {&tmTABLE}.{&FIELD3} = INT(cookie-value('cookie',"SucursalSocio")) NO-ERROR.
        ASSIGN {&tmTABLE}.{&FIELD4} = INT(cookie-value('cookie',"NumeroSocio")) NO-ERROR.
        *CTRLSOFT*/
        ASSIGN 
            {&tmTABLE}.{&FIELD1} = INT(ttEncdatdetficha.INsUnionSocio) NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FIELD2} = INT(ttEncdatdetficha.INsCajaSocio) NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FIELD3} = INT(ttEncdatdetficha.INsSucursalSocio) NO-ERROR.
        ASSIGN 
            {&tmTABLE}.{&FIELD4} = INT(ttEncdatdetficha.INsNumeroSocio) NO-ERROR.
       
        ASSIGN 
            {&tmTABLE}.{&FIELD7} = 0 no-error.
        ASSIGN 
            {&tmTABLE}.{&FIELD8} = 0 NO-ERROR.
    END.  /* DEL ELSE DO 1 */           
/*CTRLSOFT*
DISPLAY {&tmTABLE}.{&FK11}
        {&tmTABLE}.{&FK12}
        {&tmTABLE}.{&FK13}
        {&tmTABLE}.{&FK16}
        STRING({&tmTABLE}.{&FK18},'999')
        {&tmTABLE}.{&FK14}
        {&tmTABLE}.{&FK15}
        {&tmTABLE}.{&PK}
        {&tmTABLE}.{&PK2}
        {&tmTABLE}.{&FK21}
        {&tmTABLE2}.{&FIELD1FK2}
        {&tmTABLE}.{&FIELD1}
        {&tmTABLE}.{&FIELD2}
        {&tmTABLE}.{&FIELD3}
        {&tmTABLE}.{&FIELD4}
        {&tmTABLE}.{&FIELD5}
        sServicioForaneo
        {&tmTABLE}.{&FIELD6}
        {&tmTABLE}.{&FIELD7}
        {&tmTABLE}.{&FIELD8}
        {&tmTABLE}.{&FIELD9}      
        {&tmTABLE}.{&FIELD10}
        nFolioFicha
        WITH FRAME DetailFrame.
        *CTRLSOFT*/
END PROCEDURE.


CREATE ttEncmensasoc.
BUFFER-COPY ttEncdatdetficha TO ttEncmensasoc NO-ERROR.

RUN DespliegaMensajeSocio (INPUT ttEncdatdetficha.INDespliegaMensaje,
                           INPUT ttEncdatdetficha.INsUnionSocio,
                           INPUT ttEncdatdetficha.INsCajaSocio,
                           INPUT ttEncdatdetficha.INsSucursalSocio,
                           INPUT ttEncdatdetficha.INsNumeroSocio,
                           INPUT-OUTPUT TABLE ttEncmensasoc,
                                 OUTPUT TABLE ttdetmensajesoc).

PROCEDURE CreaPagoChequeDevuelto:
    DEF VAR sCheque           AS CHAR.
    DEF VAR sDatoCheque       AS CHAR.
    DEF VAR sReferencia       AS CHAR.

    /*--------Variables:Servicios adiconales que generan IVA------------------*/
    DEF VAR lGeneraIVA1       AS LOGICAL   INITIAL NO.
    DEF VAR sServGeneraIVA1   AS CHAR      INITIAL ''.
    DEF VAR dIVAGenerado1     AS DECIMAL   INITIAL 0 FORMAT "->>>,>>>,>>9.99".
    DEF VAR sDescIVAGenerado1 AS CHAR      INITIAL ''.
    DEF VAR lPremiteMovto1    AS LOGICAL   INITIAL YES.
    DEF VAR dImporteMto1      AS DECIMAL   INITIAL 0 FORMAT "->>>,>>>,>>9.99".
    DEF VAR parametroiva      AS CHARACTER INITIAL ''.

    FIND FIRST parametrosgenerales WHERE parametrosgenerales.idunion = nUnion AND
        parametrosgenerales.idcajapopular = nCaja AND
        parametrosgenerales.nombre = 'TIPOCALCIVA' NO-LOCK NO-ERROR.
    IF AVAILABLE(parametrosgenerales) THEN 
    DO:
        parametroiva = parametrosgenerales.valor.
    END.
    /*------------------------------------------------------------------------*/




    DEF VAR ImporteCheque AS DECIMAL FORMAT "->>>,>>>,>>>,>>9.99".
    
    /*CTRLSOFT* sCheque = STRING(INT(get-value("Banco")),"999") + "-"  +  STRING(DEC(get-value("Cuenta")),"99999999999") +  "-"  +  STRING(INT(get-value("Cheque")),"9999999").
    sDatoCheque =  get-value("Banco") + "-" + get-value("Cuenta") + "-" + get-value("Cheque").
    *CTRLSOFT*/
    sCheque = STRING(INT(ttEncdatdetficha.INBanco),"999") + "-"  +  STRING(DEC(ttEncdatdetficha.INCuenta),"99999999999") +  "-"  +  STRING(INT(ttEncdatdetficha.INCheque),"9999999").
    sDatoCheque =  ttEncdatdetficha.INBanco + "-" + ttEncdatdetficha.INCuenta + "-" + ttEncdatdetficha.INCheque.
    sReferencia= "&CH," + sCheque.
    nCargo=0.

    /*CTRLSOFT* IF DECIMAL(get-value("Importe")) <> 0 THEN DO: *CTRLSOFT*/
    IF DECIMAL(ttEncdatdetficha.INImporte) <> 0 THEN 
    DO:   

        /*--------------susana: Servicios adiconales que generan IVA----------------------*/
        lGeneraIVA1 = no.
        sServGeneraIVA1 = ''.
        dIVAGenerado1 = 0.
        sDescIVAGenerado1 = ''.
        lPremiteMovto1 = YES.
        FIND FIRST ServicioGeneraIVA WHERE trim(caps(ServicioGeneraIVA.IDServicio)) = TRIM(CAPS(sServicioChequeDevuelto)) AND~
            ServicioGeneraIVA.CausaIVA = yes NO-LOCK NO-ERROR.
        IF AVAILABLE(ServicioGeneraIVA) THEN 
        DO:
            FIND FIRST servicio WHERE Servicio.IDServicio = ServicioGeneraIVA.IDServicioIVA NO-LOCK NO-ERROR.
            IF AVAILABLE(Servicio) THEN 
            do:
                sServGeneraIVA1 = ServicioGeneraIVA.IDServicioIVA.
                sDescIVAGenerado1 = "IVA POR PRESTACION DE SERVICIO".
                lGeneraIVA1 = yes.
            END.
            else 
            do:
                ASSIGN 
                    ttEncdatdetficha.respuesta = "No existe el servicio de IVA " + caps(ServicioGeneraIVA.IDServicioIVA) + 
                                       " en el cat�logo de servicios para el servicio " + caps(ServicioGeneraIVA.IDServicio).
                /*CTRLSOFT*
</script>
<Script language= "JavaScript">
                alert("No existe el servicio de IVA `caps(ServicioGeneraIVA.IDServicioIVA)` en el cat�logo de servicios para el servicio `caps(ServicioGeneraIVA.IDServicio)`");
</script>
<script language="SpeedScript">
*CTRLSOFT*/
                lGeneraIVA1 = no.
                sServGeneraIVA1 = ''.
                sDescIVAGenerado1 = ''.
                dIVAGenerado1 = 0.
                lPremiteMovto1 = NO.                       
            END. /*else*/                    

        END. /*find ServicioGeneraIVA*/
        /*------------------------------------------------------------------------*/
    
        /*CTRLSOFT* nAbono= DECIMAL(get-value("Importe")). *CTRLSOFT*/
        nAbono= DECIMAL(ttEncdatdetficha.INImporte).

        /*-----lGeneraIVA1 = yes -----*/
        IF lGeneraIVA1 = yes AND trim(caps(parametroiva)) = 'DESGLOSE' THEN 
        do:
            run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nAbono, output dIVAGenerado1) no-error.
            IF dIVAGenerado1 > 0 and trim(sServGeneraIVA1) <> '' THEN 
            do:
                RUN GuardaDatos (INPUT sServGeneraIVA1,sDescIVAGenerado1,0,dIVAGenerado1,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha)) NO-ERROR.
                IF ERROR-STATUS:ERROR THEN 
                DO:
                    /*CTRLSOFT* {&out} "<br> Error al guardar el calculo de iva para " sServicioIndemnizacion. *CTRLSOFT*/
                    ASSIGN
                        ttEncdatdetficha.respuesta = "Error al guardar el calculo de iva para " + sServicioIndemnizacion.
                END.
                else 
                do:
                    nDetalleFicha = nDetalleFicha + 1.                                                   
                END.


            END. /*dIVAGenerado1 > 0*/                
        END. /*lGeneraIVA1*/  


        sConcepto= "ABONO A CHQ.DEV." + sDatoCheque.    
        RUN GuardaDatos(sServicioChequeDevuelto,sConcepto,nCargo,nAbono,"",sReferencia).
        nDetalleFicha = nDetalleFicha + 1.
    END.  /*fin de cheque devuelto*/
  
    /*CTRLSOFT* IF DECIMAL(get-value("ComisionBancaria")) <> 0 THEN DO: *CTRLSOFT*/
    IF DECIMAL(ttEncdatdetficha.INComisionBancaria) <> 0 THEN 
    DO:

        /*--------------susana: Servicios adiconales que generan IVA----------------------*/
        lGeneraIVA1 = no.
        sServGeneraIVA1 = ''.
        dIVAGenerado1 = 0.
        sDescIVAGenerado1 = ''.
        lPremiteMovto1 = YES.
        FIND FIRST ServicioGeneraIVA WHERE trim(caps(ServicioGeneraIVA.IDServicio)) = TRIM(CAPS(sServicioComisionBancaria)) AND~
            ServicioGeneraIVA.CausaIVA = yes NO-LOCK NO-ERROR.
        IF AVAILABLE(ServicioGeneraIVA) THEN 
        DO:
            FIND FIRST servicio WHERE Servicio.IDServicio = ServicioGeneraIVA.IDServicioIVA NO-LOCK NO-ERROR.
            IF AVAILABLE(Servicio) THEN 
            do:
                sServGeneraIVA1 = ServicioGeneraIVA.IDServicioIVA.
                sDescIVAGenerado1 = "IVA POR PRESTACION DE SERVICIO".
                lGeneraIVA1 = yes.
            END.
            else 
            do:
                ASSIGN
                    ttEncdatdetficha.respuesta = "No existe el servicio de IVA " + caps(ServicioGeneraIVA.IDServicioIVA) + " en el cat�logo de servicios para el servicio " + caps(ServicioGeneraIVA.IDServicio).
                /*CTRLSOFT*
                </script>
                <Script language= "JavaScript">
                                alert("No existe el servicio de IVA `caps(ServicioGeneraIVA.IDServicioIVA)` en el cat�logo de servicios para el servicio `caps(ServicioGeneraIVA.IDServicio)`");
                </script>
                <script language="SpeedScript">
                *CTRLSOFT*/
                lGeneraIVA1 = no.
                sServGeneraIVA1 = ''.
                sDescIVAGenerado1 = ''.
                dIVAGenerado1 = 0.
                lPremiteMovto1 = NO.                       
            END. /*else*/                    

        END. /*find ServicioGeneraIVA*/
        /*------------------------------------------------------------------------*/
        /*CTRLSOFT* nAbono= DECIMAL(get-value("ComisionBancaria")). *CTRLSOFT*/
        nAbono= DECIMAL(ttEncdatdetficha.INComisionBancaria).

        /*-----lGeneraIVA1 = yes -----*/
        IF lGeneraIVA1 = yes AND trim(caps(parametroiva)) = 'DESGLOSE' THEN 
        do:
            run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nAbono, output dIVAGenerado1) no-error.
            IF dIVAGenerado1 > 0 and trim(sServGeneraIVA1) <> '' THEN 
            do:
                RUN GuardaDatos (INPUT sServGeneraIVA1,sDescIVAGenerado1,0,dIVAGenerado1,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha)) NO-ERROR.
                IF ERROR-STATUS:ERROR THEN 
                DO:
                    ASSIGN
                        ttEncdatdetficha.respuesta = "Error al guardar el calculo de iva para " + sServicioIndemnizacion.
                    RETURN.
                /*CTRLSOFT*
              {&out} "<br> Error al guardar el calculo de iva para " sServicioIndemnizacion.
              *CTRLSOFT*/
                END.
                else 
                do:
                    nDetalleFicha = nDetalleFicha + 1.                                                   
                END.


            END. /*dIVAGenerado1 > 0*/                
        END. /*lGeneraIVA1*/  

        sConcepto= "ABONO COMISION X CHQ.DEV." +  sDatoCheque.    
        RUN GuardaDatos(sServicioComisionBancaria,sConcepto,nCargo,nAbono,"",sReferencia).
        nDetalleFicha = nDetalleFicha + 1.
        /*-----lGeneraIVA = yes -----*/
        IF lGeneraIVA1 = yes AND trim(caps(parametroiva)) = 'CALCULO' THEN 
        do:
            run CalculoIVAServicio(input nunion, input ncaja, input nsucursal, input nabono, output dIVAGenerado1) no-error.
            /***********DEPURAR*************
            dImporteIvaComision = dIVAGeneradocom.
            *******************************/                            
            IF dIVAGenerado1 > 0 and trim(sServGeneraIVA1) <> '' THEN 
            do:
                nDetalleFicha = nDetalleFicha + 1.
                RUN GuardaDatos (INPUT sServGeneraIVA1,sDescIVAGenerado1,0,dIVAGenerado1,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha - 1)) NO-ERROR.
                                                
            END. /*dIVAGenerado > 0*/ 
                                                          
        END. /*lGeneraIVA*/
    /**************************************/

    END. /*fin de comision bancaria*/
  
    /*CTRLSOFT* IF DECIMAL(get-value("Indemnizacion")) <> 0 THEN DO: *CTRLSOFT*/
    IF DECIMAL(ttEncdatdetficha.INIndemnizacion) <> 0 THEN 
    DO:    
        /*--------------susana: Servicios adiconales que generan IVA----------------------*/
        lGeneraIVA1 = no.
        sServGeneraIVA1 = ''.
        dIVAGenerado1 = 0.
        sDescIVAGenerado1 = ''.
        lPremiteMovto1 = YES.
        FIND FIRST ServicioGeneraIVA WHERE trim(caps(ServicioGeneraIVA.IDServicio)) = TRIM(CAPS(sServicioIndemnizacion)) AND~
            ServicioGeneraIVA.CausaIVA = yes NO-LOCK NO-ERROR.
        IF AVAILABLE(ServicioGeneraIVA) THEN 
        DO:
            FIND FIRST servicio WHERE Servicio.IDServicio = ServicioGeneraIVA.IDServicioIVA NO-LOCK NO-ERROR.
            IF AVAILABLE(Servicio) THEN 
            do:
                sServGeneraIVA1 = ServicioGeneraIVA.IDServicioIVA.
                sDescIVAGenerado1 = "IVA POR PRESTACION DE SERVICIO".
                lGeneraIVA1 = yes.
            END.
            else 
            do:
                ASSIGN
                    ttEncdatdetficha.respuesta = "No existe el servicio de IVA " + caps(ServicioGeneraIVA.IDServicioIVA) + 
                        " en el cat�logo de servicios para el servicio " + CAPS(ServicioGeneraIVA.IDServicio).
                /*CTRLSOFT*
</script>
<Script language= "JavaScript">
        alert("No existe el servicio de IVA `caps(ServicioGeneraIVA.IDServicioIVA)` en el cat�logo de servicios para el servicio `caps(ServicioGeneraIVA.IDServicio)`");
</script>
<script language="SpeedScript">
                *CTRLSOFT*/
                lGeneraIVA1 = no.
                sServGeneraIVA1 = ''.
                sDescIVAGenerado1 = ''.
                dIVAGenerado1 = 0.
                lPremiteMovto1 = NO.                       
            END. /*else*/                    

        END. /*find ServicioGeneraIVA*/
        /*------------------------------------------------------------------------*/

        /*CTRLSOFT* nAbono= DECIMAL(get-value("Indemnizacion")). *CTRLSOFT*/
        nAbono= DECIMAL(ttEncdatdetficha.INIndemnizacion).
    
        /*-----lGeneraIVA1 = yes -----*/
        IF lGeneraIVA1 = yes AND trim(caps(parametroiva)) = 'DESGLOSE' THEN 
        do:
            run CalculoIVAPrestacionServicios(input nunion, input ncaja, input nsucursal, INPUT-OUTPUT nAbono, output dIVAGenerado1) no-error.
            IF dIVAGenerado1 > 0 and trim(sServGeneraIVA1) <> '' THEN 
            do:
                RUN GuardaDatos (INPUT sServGeneraIVA1,sDescIVAGenerado1,0,dIVAGenerado1,"","IVA," + string(nUnionFicha) + "," + string(nCajaFicha) + "," + string(nSucursalFicha) + "," + string(nEjercicio) + "," + string(nCajaAsignada) + "," + sTransaccion + "," + string(nFolioFicha) + "," + string(nDetalleFicha)) NO-ERROR.
                IF ERROR-STATUS:ERROR THEN 
                DO:
                    ttEncdatdetficha.respuesta = "Error al guardar el calculo de iva para " + sServicioIndemnizacion.
                /*CTRLSOFT* {&out} "<br> Error al guardar el calculo de iva para " sServicioIndemnizacion.*CTRLSOFT*/
                END.
                else 
                do:
                    nDetalleFicha = nDetalleFicha + 1.                                                   
                END.


            END. /*dIVAGenerado1 > 0*/                
        END. /*lGeneraIVA1*/                             

        sConcepto= "PAGO INDEMNIZACION A CAJA X CHQ.DEV." + sDatoCheque.    
        RUN GuardaDatos( sServicioIndemnizacion,sConcepto,nCargo,nAbono,"",sReferencia).
        nDetalleFicha = nDetalleFicha + 1.

    END.   /*fin de indemnizacion*/     
END PROCEDURE.

PROCEDURE TipoSociosComision: 
    IF sIVATrasladados[12] = "Pago_Terceros" THEN
    DO:
        nSocioPT         = 0.
        nSocioMorosoPT   = 0.
        nNoSocioPT       = 0.
        nServicioPT      = 0.

        FOR EACH SerPagoTerceros WHERE SerPagoTerceros.IDServicioVinculado = DetalleFichaTemp.IDServicio
            AND INT(SerPagoTerceros.Concepto) > 1
            NO-LOCK:                               
            sPagTercerosSocCom[1] = SUBSTRING(SerPagoTerceros.SociosComision,1,1).                   
            nServicioPT = nServicioPT + 1.         
            IF INT(SerPagoTerceros.Concepto) = 2 THEN 
            DO:          
                sNomServicioPT[1] =  SerPagoTerceros.IDServicio.
            END.   
            nSerVinculados = SerPagoTerceros.SerVinculados.
            IF nServicioPT = 1 THEN  
            DO:
                sPorcentaje           =   ".0" + SUBSTRING(STRING(SerPagoTerceros.ComisionPorcentual),1,2).
                dTotalComCajaIVAT     = (SerPagoTerceros.ComisionCaja / 1.15) * .15.
                dComisionGRIMIVAT     = (SerPagoTerceros.ComisionGRIM / 1.15) * .15. 
                dComisionGRIM         = SerPagoTerceros.ComisionGRIM.
                dTotalComCaja         = SerPagoTerceros.ComisionCaja.
                sReferenciaPT         = SerPagoTerceros.Referencia.
                sPagTercerosSocCom[1] = SerPagoTerceros.SociosComision.
            END. 
        END. /*FOR EACH SerPagoTerceros WHERE SerPagoTerceros.IDServicioVinculado = DetalleFichaTemp.IDServicio*/
        nServicioPT = 0.

        IF sReferenciaPT = "Servicio" THEN
        DO:
            ASSIGN ttEncdatdetficha.nYaSeAgregoComision = "1".
            /*CTRLSOFT*
              </script>
            <input type="hidden" name="nYaSeAgregoComision" value="1">
            <script language="SpeedScript">
            nYaSeAgregoComision    = INT(GET-VALUE("nYaSeAgregoComision")).   
            *CTRLSOFT*/
            nYaSeAgregoComision    = INT(ttEncdatdetficha.nYaSeAgregoComision).
            IF nYaSeAgregoComision = 0 THEN       
                ASSIGN DetalleFichaTemp.abono = DetalleFichaTemp.abono + dComisionGRIM. /*Agrega la comision al monto q se capturo*/    
            IF (DetalleFichaTemp.IDSocio <> 00 AND (sPagTercerosSocCom[1] = "1" OR sPagTercerosSocCom[1] = "3")) 
                OR (DetalleFichaTemp.IDSocio = 00 AND (sPagTercerosSocCom[1] = "2" OR sPagTercerosSocCom[1] = "3")) THEN
            DO:  
                  
                IF DetalleFichaTemp.IDSocio <> 00 AND sPagTercerosSocCom[1] = "3" THEN /*Cuando se le cobra comision al socio diferente*/
                DO:
                    FOR EACH ParametrosGenerales WHERE /* ParametrosGenerales.IDUnion = SerPagoTerceros.IDUnion
                                              AND ParametrosGenerales.IDCajaPopular = SerPagoTerceros.IDCajaPopular  
                                              AND */ ParametrosGenerales.NombreParametro = "PorcentajeComSoc"
                        AND DetalleFichaTemp.IDSocio > 0
                        NO-LOCK: 
                        sPorcentaje =  ParametrosGenerales.Valor.             
                    END. /*FOR EACH ParametrosGenerales WHERE ParametrosGenerales.NombreParametro = "ComisionQseCobra": */
                    IF sPorcentaje = "100" OR DetalleFichaTemp.IDSocio = 0 OR INT(sPorcentaje) = 0 THEN
                    DO:
                        dTotalComCaja = dTotalComCaja.
                    END. /*IF sPorcentaje = "100" OR DetalleFichaTemp.IDSocio = 0 OR INT(sPorcentaje) = 0 THEN*/
                    ELSE
                    DO:   
                        sPorcentaje = "." + sPorcentaje .
                        dTotalComCaja = DECIMAL(sPorcentaje) * dTotalComCaja.
                        dTotalComCajaIVAT = (dTotalComCaja / 1.15) * .15.
                    END. /*ElSE*/   
                END. /*IF DetalleFichaTemp.IDSocio <> 00 THEN*/	           
                /**Detalle Comision**/
                sCreaDetallesServicio[1]   = sNomServicioPT[1].
                sCreaDetallesCargo[1]      = "0".
                sCreaDetallesAbono[1]      = STRING((dTotalComCaja - ((dTotalComCaja / 1.15) * .15))).
                sCreaDetallesReferencia[1] = "&PT1".
                /**Detalle IVA Trasladado**/
                sCreaDetallesServicio[2]   = sIVATrasladado.
                sCreaDetallesCargo[2]      = "0".
                sCreaDetallesAbono[2]      = STRING((dTotalComCaja / 1.15) * .15 ).
                sCreaDetallesReferencia[2] = "&PT1".  
                nPagoaTerceros = 2.
            END. /*IF DetalleFichaTemp.IDSocio <> 00 */ 
        END. /*IF SerPagoTerceros.ComisionCaja > 0 THEN*/
    END. /* IF sIVATrasladados[12] = "Pago_Terceros" THEN */  
END PROCEDURE.   
PROCEDURE CreaDetallesPagTer:
    DO nCreaPT = 1 TO nPagoaTerceros:   
        nUltimoDetalle = nUltimoDetalle + 1.

        CREATE  DetalleFichaTemp.
        ASSIGN  
            DetalleFichaTemp.IDUnion            = nUnionFicha
            DetalleFichaTemp.IDCajaPopular      = nCajaFicha
            DetalleFichaTemp.IDSucursal         = nSucursalFicha
            DetalleFichaTemp.Ejercicio          = nEjercicio 
            DetalleFichaTemp.IDCajaAsignada     = nCajaAsignada 
            DetalleFichaTemp.IDTipoTransaccion  = sTransaccion 
            DetalleFichaTemp.IDFicha            = nFolioFicha    
            DetalleFichaTemp.IDDetalleFicha     = nUltimoDetalle            
            DetalleFichaTemp.IDServicio         = sCreaDetallesServicio[nCreaPT] 
            DetalleFichaTemp.FechaFicha         = TODAY 
            DetalleFichaTemp.IDUnionSocio       = nUnionSocioServicio
            DetalleFichaTemp.IDCajaPopularSocio = nCajaSocioServicio
            DetalleFichaTemp.IDSucursalSocio    = nSucursalSocioServicio
            DetalleFichaTemp.IDSocio            = nIDSocioServicio
            DetalleFichaTemp.IDServicioForaneo  = sIDSerForaneoSer
            DetalleFichaTemp.Concepto           = sConceptoServicio
            DetalleFichaTemp.cargo              = DECIMAL(sCreaDetallesCargo[nCreaPT])
            DetalleFichaTemp.abono              = DECIMAL(sCreaDetallesAbono[nCreaPT])
            DetalleFichaTemp.Referencia         = sCreaDetallesReferencia[nCreaPT].
        nUltimoDetalle = nUltimoDetalle + 1.
    END. /*DO nCreaPT = 1 TO nCreaPT2:*/
END PROCEDURE.
/*CTRLSOFT*
</SCRIPT>
</body>
</html>
*CTRLSOFT*/