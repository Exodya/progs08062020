/*** JVA CTRLSOFT <html>
<head>
<title>A@ntah - Monitoreo de cajeros</title>
<!--WSS  JVA CTRLSOFT ***/
 /* �ltima modifcaci�n: 11/oct/2002
 Los saldos positivos de saldo actual ser�n marcados en color negro lo que indica que el cajero debe
 de entregar los dineros en cuesti�n.
 Saldos negativos significan faltantes del cajero.
 */
 
 {I/monitorcajas.i}
 
 DEFINE INPUT-OUTPUT PARAMETER TABLE FOR ttEncdatos.
 /*DEFINE OUTPUT PARAMETER TABLE FOR tt-EncMonitorCaj.*/
 DEFINE OUTPUT PARAMETER TABLE FOR tt-MonitorCajas.
 DEFINE OUTPUT PARAMETER TABLE FOR tt-totalesCaj.
 DEFINE OUTPUT PARAMETER TABLE FOR tt-SaldoServCaj.
 
 DEFINE VAR sIDUsuario LIKE Usuario.IDUsuarioEncode.
 DEF VAR sIDAplicacion AS CHAR INITIAL "MONITORCAJA".
 DEF VAR sNombre AS CHAR.
 DEF VAR sColorSI AS CHAR.
 DEF VAR sColorSA AS CHAR.
 DEF VAR sColorSD AS CHAR.
 DEF VAR sEdoCaja AS CHAR.
 
 DEF VAR nCont AS INTEGER.
 DEF VAR sBgColor AS CHAR.
 DEF VAR nTiempo AS INTEGER.
 DEF VAR nMaxEfecCja AS DECIMAL.
 DEF VAR nSaldoIni AS DECIMAL.
 DEF VAR nSaldoActual AS DECIMAL.
 DEF VAR nSaldoDia AS DECIMAL.
 DEF VAR nCajeroPrin AS INT.
 DEF VAR nSaldoCajaPrin AS DECIMAL.
 DEF VAR nSaldoServicio AS DECIMAL.
 DEF VAR nTotalCajeros  AS DECIMAL.
 DEF VAR nTotSaldoIni AS DECIMAL.
 DEF VAR nTotSaldoActual AS DECIMAL.
 DEF VAR nTotSaldoDia AS DECIMAL.
 DEF VAR nTotDepositos AS DECIMAL.
 DEF VAR nTotRetiros AS DECIMAL.
 DEF VAR nTotCajas AS INT.
 DEF VAR nSumaCheques AS DECIMAL.
 DEF VAR nSaldoNeto AS DECIMAL.
 DEF VAR dFecha AS DATE.
 DEF VAR sHora AS CHAR.
 DEF VAR sSucursal AS CHAR.
 
   
 FIND FIRST ttEncdatos EXCLUSIVE-LOCK NO-ERROR.
 IF NOT AVAILABLE ttEncdatos THEN DO:
    CREATE ttEncdatos.
    ASSIGN ttEncdatos.INid      = 1
           ttEncdatos.respuesta = "ERROR no se encontro ttEncdatos".
    RETURN.
 END.
 
 {wslibrerias/ssiwebsecurityWS.i}
 {colores.i}
 /*** JVA CTRLSOFT {ssijavascript.i} ***/
 IF SSICanIn()="" THEN DO:
    ASSIGN ttEncdatos.respuesta = "ERROR no se encontro usuario codificado".
    RETURN.
 END.
 RUN SLogin(INPUT-OUTPUT nUnion, INPUT-OUTPUT nCaja, INPUT-OUTPUT nSucursal, INPUT-OUTPUT sIDUsuario).
 
 /*** JVA CTRLSOFT IF GET-VALUE("nSucursalVER") <> "" then
   nsucursal = Int(GET-VALUE("nSucursalVER")). JVA CTRLSOFT ***/
 
 IF nSucursalVER <> "" THEN
   nsucursal = Int(ttEncdatos.nSucursalVER).
 
 FIND SUCURSAL WHERE Sucursal.IDUnion = nUnion AND Sucursal.IDCajaPopular = nCaja AND Sucursal.IDSucursal = nSucursal
      NO-LOCK NO-ERROR.
 IF ERROR-STATUS:ERROR THEN DO:
    /*** JVA CTRLSOFT {&OUT} "No se pudo encontrar la sucursal.". ***/
    ASSIGN ttEncdatos.INid      = 1
           ttEncdatos.respuesta = "No se pudo encontrar la sucursal.".
    RETURN.
 END.
 sSucursal=STRING(nUnion,"99") + STRING(nCaja,"99") + STRING(nSucursal,"99").
 
  /*** JVA CTRLSOFT IF REQUEST_METHOD = "GET" THEN DO:
    nTiempo = 30000.
 END.
 ELSE DO:
   IF (INT(GET-VALUE("Tiempo"))) > 0 THEN
    nTiempo=INT(GET-VALUE("Tiempo")).
    ELSE nTiempo = 30000.
 END. -->
 <Script Language="SpeedScript">  JVA CTRLSOFT ***/
 
  FIND ParametrosGenerales WHERE ParametrosGenerales.IDUnion = nUnion AND
       ParametrosGenerales.IDCajaPopular=nCaja AND
       ParametrosGenerales.IDSucursal=nSucursal AND
       ParametrosGenerales.Nombre = "MAXEFECTIVOCJA" NO-LOCK NO-ERROR.
  IF NOT ERROR-STATUS:ERROR THEN DO:
    ASSIGN nMaxEfecCja=DECIMAL(ParametrosGenerales.Valor).
  END.
  ELSE DO:
   /***  JVA CTRLSOFT </script><script language="JavaScript">
   alert("No existe en la tabla ParametrosGenerales el valor para MAXEFECTIVOCJA.");
   window.open("paginablanca3.r","detalle");
   </script><Script Language="SpeedScript">  JVA CTRLSOFT ***/
   ASSIGN ttEncdatos.INid      = 1
          ttEncdatos.respuesta = "No existe en la tabla ParametrosGenerales el valor para MAXEFECTIVOCJA.".
   RETURN.
  END.
  FIND ParametrosGenerales WHERE ParametrosGenerales.IDUnion = nUnion AND
       ParametrosGenerales.IDCajaPopular=nCaja AND
       ParametrosGenerales.IDSucursal=nSucursal AND
       ParametrosGenerales.Nombre = "CAJEROPRINCIPAL" NO-LOCK NO-ERROR.
  IF NOT ERROR-STATUS:ERROR THEN DO:
    ASSIGN nCajeroPrin=INT(ParametrosGenerales.Valor).
  END.
  ELSE DO:
   /***  JVA CTRLSOFT </script><script language="JavaScript">
   alert("No existe en la tabla ParametrosGenerales el valor para CAJEROPRINCIPAL.");
   window.open("paginablanca3.r","detalle");
   </script><Script Language="SpeedScript">  JVA CTRLSOFT ***/
   ASSIGN ttEncdatos.INid      = 1
          ttEncdatos.respuesta = "No existe en la tabla ParametrosGenerales el valor para MAXEFECTIVOCJA.".
   RETURN.
  END.
  FIND FIRST SaldoServicio WHERE SaldoServicio.IDUnion = nUnion AND
             SaldoServicio.IDCajaPopular=nCaja AND
             SaldoServicio.IDSucursal=nSucursal AND
             SaldoServicio.IDServicio="CAJA" NO-LOCK NO-ERROR.
  IF NOT ERROR-STATUS:ERROR THEN DO:
     nSaldoServicio = SaldoServicio.SaldoActual.
  END.
  FOR EACH CajaAsignada WHERE CajaAsignada.IDUnion = nUnion AND
           CajaAsignada.IDCajaPopular=nCaja AND
           CajaAsignada.IDSucursal=nSucursal NO-LOCK:     
      nTotalCajeros = nTotalCajeros + Cajaasignada.SaldoActual.
  END.
  
  sHora = STRING(TIME,"HH:MM").
      
/***  JVA CTRLSOFT </script>

<STYLE type=text/css>A:link {
	COLOR: `Link`}
A:visited {
	COLOR: `Visited`}
A:hover {
	COLOR: `OverLink`}
</STYLE>
<STYLE type=text/css>A {
	TEXT-DECORATION: none}
</STYLE>
<script language="JavaScript" src="/class/funcionesjs.js"></script>      

</head>

<body bgcolor="#C0C0C0" onLoad="iniciaTimer(`nTiempo`)" topmargin="0" leftmargin="0">

<script>
 function callintervalo(){
    nTime = window.showModalDialog('modalmonitoreo.r','','dialogWidth:17; dialogHeight:9; center:yes');
    if (nTime>0){    
     clearTimeout(TimerID);
     form.Tiempo.value=nTime;
     TimerID = setTimeout('form.submit()',nTime);
    }
  } 
  
  function validatimer(objeto) {
    if (objeto.value=="Detener"){
     clearTimeout(TimerID);
     objeto.value="Inicio";
    } else
    {
      iniciaTimer(`nTiempo`);
      objeto.value="Detener";
    }
    
  }
  
  function iniciaTimer (nTime){
  // Tiempo en milisegundos para refrescar la pantalla.
  TimerID = setTimeout('form.submit()',nTime);
  }
</script>  JVA CTRLSOFT ***/

/***  JVA CTRLSOFT <form method="POST" action="" name="form">
  <div align="center">
    <center>
  <table border="0" width="100%" cellspacing="0" cellpadding="0" height="164">                
    <tr>
      <td colspan="10" bgcolor="#005395" height="22">
        <p align="center"><b><font face="MS Sans Serif" size="3" color="#FFFFFF">Monitoreo
        de Cajeros de la sucursal `sSucursal`         Fecha:`STRING(TODAY)`  Hora:`sHora`</font></b></p>  JVA CTRLSOFT ***/     
      /**  CREATE tt-EncMonitorCaj. **/
        ASSIGN /** tt-EncMonitorCaj.tt-id         = 1 **/
               ttEncdatos.ttEncPrincCaj = "Monitoreo de Cajeros de la sucursal " + STRING(sSucursal) + " Fecha: " + STRING(TODAY) + " Hora: " + STRING(sHora)
               ttEncdatos.ttEncSecCaj   = Sucursal.Nombre.
      /***  JVA CTRLSOFT </td>
    </tr>
    <tr>
      <td colspan="10" height="21">
        <hr size="3">
      </td>
    </tr>
    <tr>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" colspan="10" height="25">
        <p align="center"><font face="Arial" color="#000080" size="2"><b>`Sucursal.Nombre`
        </b></font></p>        
      </td>
    </tr>
    <!-- Encabezado -->
    <tr>
      <td bgcolor="#C0C0C0" style="border-style: outset; border-width: 1">
        <p align="center">&nbsp;<b><font face="Arial" size="2" color="#000080"></font></b></p>
      </td>
      <td width="25%" bgcolor="#C0C0C0" height="25" style="border-style: outset; border-width: 1">
        <p align="center">&nbsp;<b><font face="Arial" size="2" color="#000080">Nombre</font></b></p>
      </td>
      <td bgcolor="#C0C0C0" height="25" style="border-style: outset; border-width: 1">
        <p align="center"><b><font face="Arial" size="2" color="#000080">Cajero #</font></b>
      </td>
      <td bgcolor="#C0C0C0" height="25" style="border-style: outset; border-width: 1">
        <p align="center"><b><font face="Arial" size="2" color="#000080">Tipo</font></b>
      </td>
      <!--
      <td width="7%" style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">
        <b><font face="Arial" size="2" color="#000080">Sesi�n</font></b>
      </td>
      -->
      
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">
        <b><font face="Arial" size="2" color="#000080">Saldo ini.</font></b>
      </td>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">
        <b><font face="Arial" size="2" color="#000080">Dep�sitos</font></b>
      </td>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">
        <b><font face="Arial" size="2" color="#000080">Retiros</font></b>
      </td>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">      
        <b><font face="Arial" size="2" color="#000080">Saldo del D�a.</font></b>
      </td>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="10" align="center">
        <b><font face="Arial" size="2" color="#000080">Edo.</font></b>
      </td>
      <td style="border-style: outset; border-width: 1" bgcolor="#C0C0C0" height="25" align="center">      
        <b><font face="Arial" size="2" color="#000080">Saldo Acum.</font></b>
      </td>
    </tr>
    <!-- Detalle -->
    <!--WSS  JVA CTRLSOFT ***/

	DEF VAR IDCajeroPrincipal AS INTEGER FORMAT "999".
	FIND FIRST ParametrosGenerales WHERE ParametrosGenerales.IDUnion = nUnion AND
										ParametrosGenerales.IDCajaPopular = nCaja AND
										ParametrosGenerales.IDSucursal = nSucursal AND
										ParametrosGenerales.NombreParametro = "CAJEROPRINCIPAL" NO-LOCK NO-ERROR.
	IF AVAILABLE(ParametrosGenerales) THEN IDCajeroPrincipal = INTEGER(ParametrosGenerales.Valor).
	ELSE IDCajeroPrincipal = 0.    

    nCont = 0.
/*
    FOR EACH CajaAsignada OF Sucursal WHERE (CajaAsignada.Cargo <> 0 OR CajaAsignada.Abono <> 0 OR CajaAsignada.SaldoActual <> 0), EACH Usuario OF CajaAsignada BY IDTipoCajero BY IDCajaAsignada:
*/
    FOR EACH CajaAsignada OF Sucursal WHERE ((CajaAsignada.IDCajaAsignada = IDCajeroPrincipal) AND (CajaAsignada.Cargo <> 0 OR CajaAsignada.Abono <> 0 OR CajaAsignada.SaldoActual <> 0)), EACH Usuario OF CajaAsignada BY IDTipoCajero BY IDCajaAsignada:
        CREATE tt-MonitorCajas.
        ASSIGN tt-MonitorCajas.tt-id = tt-MonitorCajas.tt-id + 1.
        nCont = nCont + 1.
        nTotCajas = nTotCajas + 1.
        IF (nCont MODULO 2) = 0 THEN sBgColor = "#DEDDDD". ELSE sBgColor = "#CCCCFF".    
        sNombre = " " + Usuario.Nombre + " " + Usuario.PrimerApellido + " " + Usuario.SegundoApellido.
        sColorSI ="BLACK".
        sColorSA ="BLACK".
        nTotSaldoIni = nTotSaldoIni + SaldoInicial.
        nTotSaldoActual = nTotSaldoActual + SaldoActual.
        nTotDepositos = nTotDepositos + Cargo.
        nTotRetiros = nTotRetiros + Abono.
        nSaldoIni = SaldoInicial * (-1).  
        nSaldoActual = SaldoActual * (-1). 
        IF IDCajaAsignada <> nCajeroPrin THEN DO:
           IF nSaldoIni < 0 THEN DO:           
              sColorSI = "#DC2A35".
           END.
           IF nSaldoActual < 0 THEN DO:         
              sColorSA = "#DC2A35".
           END. 
        END.
        ELSE DO:
          sBgColor = "cian".
        END.
        nSaldoDia = Cargo - Abono.
        nTotSaldoDia = nTotSaldoDia + nSaldoDia.
        IF nSaldoDia < 0 THEN
           sColorSD = "RED".
        ELSE
           sColorSD = "BLUE".    
       
        nSumaCheques=0.
        FOR EACH Cheque WHERE Cheque.IDUnion = CajaAsignada.IDUnion AND
                              Cheque.IDCajaPopular= CajaAsignada.IDCajaPopular AND
                              Cheque.IDSucursal= CajaAsignada.IDSucursal AND
                              Cheque.IDCajaAsignada= CajaAsignada.IDCajaAsignada AND
                              Cheque.IDEstadoCheque=1 no-lock:
            nSumaCheques = nSumaCheques + Cheque.Importe.
        END.
        nSaldoNeto = SaldoActual - nSumaCheques.

        /***  JVA CTRLSOFT -->
        <tr>
      
        <td bgcolor="`sBgColor`" style="font-size: 8 pt; font-family: Arial; text-decoration: blink" align="center">
          <!--WSS IF CajaAsignada.IDCajaAsignada <> nCajeroPrin THEN DO:
          IF nSaldoNeto > nMaxEfecCja THEN DO: -->
             <img border="0" src="/gifs/smf_ambar.gif" width="14" height="31"></td>
          <!--WSS END.
          ELSE DO: -->
           &nbsp
          <!--WSS END.
         END.
         ELSE DO:  
           IF SSICanDo(sIDUsuario,nUnion,nCaja,sIDAplicacion,"VERDIFCAJA") THEN DO:
              IF nTotalCajeros <> nSaldoServicio THEN DO: -->
                <img border="0" src="/gifs/smf_rojo.gif" width="14" height="31"></td>
             <!--WSS END.
             ELSE DO: -->
                &nbsp
             <!--WSS END.
          END.
          ELSE DO: -->
            &nbsp
          <!--WSS END.
        END. -->
      
        <td style="background-color: `BGLink`; font-family: Arial; font-size: 8 pt; border: 1 outset `BGLink`"
        onclick="showModalDialog('desglosesaldocajero.r?CajaAsignada=`CajaAsignada.IDCajaAsignada`','','dialogWidth:40;dialogHeight:22;center:yes')"
        onmouseover="mOvr(this,'`Terciario`')" onMouseOut="mOut(this,'`BGLink`')">
        `sNombre`</td>  JVA CTRLSOFT ***/
        ASSIGN tt-MonitorCajas.tt-nombre = sNombre
        /***  JVA CTRLSOFT <!-- <td bgcolor="`sBgColor`" style="font-size: 8 pt; font-family: Arial" height="20"><font color="#000080">`sNombre`</font></td> -->
        <td bgcolor="`sBgColor`" align="center" height="20">
         <font face="Arial" size="1" color="#800080">`STRING(CajaAsignada.IDCajaAsignada,"999")`</td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-cajero = STRING(CajaAsignada.IDCajaAsignada,"999")
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" style="font-size: 8 pt; font-family: Arial" align="center" height="20">
          <font face="Arial" size="1" color="#800080">`IDTipoCajero`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-tipo   = IDTipoCajero.
        /*** JVA CTRLSOFT <!--
        <td width="7%" bgcolor="`sBgColor`" align="center" height="20"><b><font face="Arial" size="2" color="#800080">`Sesion FORMAT ("99")`</font></b></td>
        -->
        <!--WSS  JVA CTRLSOFT ***/ 
          IF nSaldoIni < 0 THEN DO:            
            nSaldoIni= nSaldoIni * (-1).
          END.
          IF nSaldoActual < 0 THEN DO:
            sEdoCaja = "F".            
          END.
          ELSE
            sEdoCaja = "S". /*** JVA CTRLSOFT -->
           
        <td bgcolor="`sBgColor`" align="right" height="20"><font face="Arial" size="1" color="`sColorSI`">$
        `nSaldoIni FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA CTRLSOFT ***/
        ASSIGN tt-MonitorCajas.tt-saldoIni = STRING(nSaldoIni,"->>>,>>>,>>>,>>9.99")
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20"><font face="Arial" size="1" color="#800080">$
        `Cargo FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-depositos = STRING(Cargo,"->>>,>>>,>>>,>>9.99")
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20"><font face="Arial" size="1" color="#800080">$
        `Abono FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-retiros = STRING(Abono,"->>>,>>>,>>>,>>9.99")
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20"><font face="Arial" size="1" color="`sColorSD`">$
        `nSaldoDia FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-saldoDia = STRING(nSaldoDia,"->>>,>>>,>>>,>>9.99")
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" style="font-size: 8 pt; font-family: Arial" align="center" height="20">
         <font face="Arial" size="1" color="#800080">`sEdoCaja`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-Estado = sEdoCaja
        /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20"><font face="Arial" size="1" color="`sColorSA`">$
        `nSaldoActual FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA CTRLSOFT ***/
               tt-MonitorCajas.tt-saldoAcum = STRING(nSaldoActual,"->>>,>>>,>>>,>>9.99").
        /*** JVA CTRLSOFT </tr>
        <!--WSS ***/
        /*MESSAGE "nSaldoIni " nSaldoIni  skip
                "Cargo "     Cargo skip
                "Abono "     Abono skip
                "nSaldoDia " nSaldoDia skip
                "sEdoCaja "  sEdoCaja skip
                "nSaldoActual " nSaldoActual
                " "  skip
                "tt-MonitorCajas.tt-saldoIni " tt-MonitorCajas.tt-saldoIni skip
                "tt-MonitorCajas.tt-depositos " tt-MonitorCajas.tt-depositos skip
                "tt-MonitorCajas.tt-retiros " tt-MonitorCajas.tt-retiros skip
                "tt-MonitorCajas.tt-saldoDia " tt-MonitorCajas.tt-saldoDia skip
                "tt-MonitorCajas.tt-Estado " tt-MonitorCajas.tt-Estado skip
                "tt-MonitorCajas.tt-saldoAcum " tt-MonitorCajas.tt-saldoAcum 
             VIEW-AS ALERT-BOX INFO BUTTONS OK.*/
        
    END. /***  JVA CTRLSOFT -->
    <tr>
      <td colspan="10" height="17" style="border-style: outset; border-width: 1"><font size="2">&nbsp;</font></td>
    </tr>  
    <!--WSS JVA CTRLSOFT ***/ 
    IF SSICanDo(sIDUsuario,nUnion,nCaja,sIDAplicacion,"VERDIFCAJA") THEN DO: /*** JVA CTRLSOFT --> ***/ 
       CREATE tt-totalesCaj.
       ASSIGN tt-totalesCaj.tt-id = tt-totalesCaj.tt-id + 1
    /*** JVA CTRLSOFT <tr>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>
      <td bgcolor="`sBgColor`" align="center" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">TOTALES DE CAJEROS</font></td>  
      <td bgcolor="`sBgColor`" align="center" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotCajas`</font></td>  JVA ***/
              tt-totalesCaj.tt-totCajeros = STRING(nTotCajas)
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotSaldoIni FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-totalesCaj.tt-totSaldoIni = STRING(nTotSaldoIni,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotDepositos FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-totalesCaj.tt-totDepositos = STRING(nTotDepositos,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotRetiros FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-totalesCaj.tt-totRetiros = STRING(nTotRetiros,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotSaldoDia FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td> JVA ***/
              tt-totalesCaj.tt-totSaldoDia = STRING(nTotSaldoDia,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp</font></td>  
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`nTotSaldoActual FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-totalesCaj.tt-totSaldoAcum = STRING(nTotSaldoActual,"->>>,>>>,>>>,>>9.99").
              
       CREATE tt-SaldoServCaj.
       ASSIGN tt-SaldoServCaj.tt-id = tt-SaldoServCaj.tt-id + 1
    /***  JVA CTRLSOFT </tr>
    <tr>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>
      <td bgcolor="`sBgColor`" align="center" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">SALDO DEL SERVICIO CAJAS</font></td>
      <td bgcolor="`sBgColor`" align="center" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`SaldoServicio.SaldoMesanterior FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-SaldoServCaj.tt-totSalIniServ = STRING(SaldoServicio.SaldoMesanterior,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`SaldoServicio.CargoMes FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-SaldoServCaj.tt-totDepoServ = STRING(SaldoServicio.CargoMes,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`SaldoServicio.AbonoMes FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
              tt-SaldoServCaj.tt-totRetiServ = STRING(SaldoServicio.AbonoMes,"->>>,>>>,>>>,>>9.99")
      /***  JVA CTRLSOFT <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp</font></td>    
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp</font></td>  
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`SaldoServicio.SaldoActual FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/ 
          tt-SaldoServCaj.tt-totSaldAcuServ = STRING(SaldoServicio.SaldoActual,"->>>,>>>,>>>,>>9.99")
    /***  JVA CTRLSOFT </tr>
    <tr>
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">&nbsp;</font></td>     
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1" colspan="7">
          <font face="Arial" size="1">DIFERENCIA</font></td>    
      <td bgcolor="`sBgColor`" align="center" height="20" style="border-style: outset; border-width: 1">
      <!--WSS IF (SaldoServicio.SaldoActual - nTotSaldoActual) < 0 THEN DO: -->
          <font face="Arial" size="1">S</font>  
      <!--WSS END.
           ELSE DO:
             IF (SaldoServicio.SaldoActual - nTotSaldoActual) > 0 THEN DO: -->
                <font face="Arial" size="1">F</font>  
             <!--WSS END.
             ELSE DO: -->
                <font face="Arial" size="1">&nbsp</font>  
             <!--WSS END.
          END. -->
      </td>         
      <td bgcolor="`sBgColor`" align="right" height="20" style="border-style: outset; border-width: 1">
          <font face="Arial" size="1">`(SaldoServicio.SaldoActual - nTotSaldoActual) FORMAT ("->>>,>>>,>>>,>>9.99")`</font></td>  JVA ***/
          tt-SaldoServCaj.tt-diferencia = STRING(SaldoServicio.SaldoActual - nTotSaldoActual,"->>>,>>>,>>>,>>9.99").
          /*MESSAGE "tt-totalesCaj " SKIP
                  "tt-totalesCaj.tt-totCajeros "  tt-totalesCaj.tt-totCajeros SKIP
                  "tt-totalesCaj.tt-totSaldoIni " tt-totalesCaj.tt-totSaldoIni SKIP
                  "tt-totalesCaj.tt-totDepositos " tt-totalesCaj.tt-totDepositos  SKIP
                  "tt-totalesCaj.tt-totRetiros "  tt-totalesCaj.tt-totRetiros  SKIP
                  "tt-totalesCaj.tt-totSaldoDia " tt-totalesCaj.tt-totSaldoDia SKIP
                  "tt-totalesCaj.tt-totSaldoAcum " tt-totalesCaj.tt-totSaldoAcum SKIP
                  "tt-SaldoServCaj " 
                  "tt-SaldoServCaj.tt-totSalIniServ " tt-SaldoServCaj.tt-totSalIniServ SKIP
                  "tt-SaldoServCaj.tt-totDepoServ " tt-SaldoServCaj.tt-totDepoServ SKIP
                  "tt-SaldoServCaj.tt-totRetiServ " tt-SaldoServCaj.tt-totRetiServ SKIP
                  "tt-SaldoServCaj.tt-totSaldAcuServ " tt-SaldoServCaj.tt-totSaldAcuServ SKIP
                  "tt-SaldoServCaj.tt-diferencia " tt-SaldoServCaj.tt-diferencia
              VIEW-AS ALERT-BOX INFO BUTTONS OK.*/
              ASSIGN ttEncdatos.respuesta = "SUCCESS".
    /***  JVA CTRLSOFT </tr>
          
    <tr>
      <td colspan="10" height="17" style="border-style: outset; border-width: 1"><font size="2">&nbsp;</font></td>
    </tr> 
    <!--WSS  JVA ***/
    END. /*** JVA CTRLSOFT  -->  
    <tr>
     <td colspan="10" style="border-style: inset; border-width: 1" height="27">
      <p align="center">
      <input type="button" value="Detener" name="btnTimer" onClick="validatimer(this)"><input type="button" value="Actualizar" name="btnActualizar" onClick="form.submit()"><input type="button" value="Intervalo de tiempo" name="btnIntervalo" onClick="callintervalo()"><input type="button" value="&lt;&lt; Atr�s" name="btnAtras" onClick="history.back()"><input type="hidden" name="Tiempo" value="`nTiempo`">
      </p>
     </td>
    </tr>
  </table>
    </center>
  </div>
 
</form>

</body>

</html>  JVA CTRLSOFT ***/

 
