{I/consultasociorecomendado.i}

CURRENT-WINDOW:WIDTH = 300.

CREATE ttEncdatos.
ASSIGN
    ttEncdatos.INid             = 1
    ttEncdatos.INtoken          = ""
    ttEncdatos.INnIDUnion       = "1"
    ttEncdatos.INnIDCajaPopular = "8" 
    ttEncdatos.INnIDSucursal    = "1"
    ttEncdatos.INsIDUsuario     = "ROOT"
 .

RUN procs/postconsultasociorecomendado.p (INPUT-OUTPUT TABLE ttEncdatos,
                                                OUTPUT TABLE ttSucursal).
                                                
                                                
FOR EACH ttEncdatos NO-LOCK:
    DISPLAY ttEncdatos
        WITH FRAME FEnc WIDTH 300 TITLE "ttEncdatos".
END.

FOR EACH ttSucursal NO-LOCK:
    DISPLAY ttSucursal
        WITH FRAME FDet WIDTH 300 DOWN TITLE "ttSucursal".
END.    
                                                    
                                                

