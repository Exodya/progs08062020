 
 /*------------------------------------------------------------------------
    File        : consultasociorecomendadoBE
    Syntax      : 
    Author(s)   : meji_
    Created     : Fri Jun 05 20:20:08 CDT 2020
    Notes       : 
  ----------------------------------------------------------------------*/
  
@program FILE(name="consultasociorecomendadoBE.cls", module="AppServer").
@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
@progress.service.resource FILE(name="consultasociorecomendadoBE", URI="/consultasociorecomendadoBE", schemaName="dsconssociorec", schemaFile="wscajap/AppServer/ds/dsconsultasociorecomendado.i").

USING Progress.Lang.*.



CLASS be.consultasociorecomendadoBE:
	/*------------------------------------------------------------------------------
			Purpose:                                                                      
			Notes:                                                                        
	------------------------------------------------------------------------------*/
	
	{"ds/dsconsultasociorecomendado.i"}
		/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
		
	CONSTRUCTOR PUBLIC consultasociorecomendadoBE (  ):
		SUPER ().
		
	END CONSTRUCTOR.
	
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records, based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="read", URI="?filter=~{filter~}", alias="", mediaType="application/json"). 
    METHOD PUBLIC VOID ReadconsultasociorecomendadoBE(
    		INPUT filter AS CHARACTER, 
    		OUTPUT DATASET dsconssociorec):
    	
    	DEFINE VARIABLE pcWhere AS CHARACTER.
    	IF filter BEGINS "WHERE " THEN
            pcWhere = filter.
        ELSE IF filter NE "" THEN
            pcWhere = "WHERE " + filter.    	      
	     
	    EMPTY TEMP-TABLE ttEncdatos.
        EMPTY TEMP-TABLE ttSucursal.
        CREATE ttEncdatos.
        CREATE ttSucursal.
              
        /* TODO: Add code to get a set of records and return the
           resulting records to the client. */      
    END METHOD.
	  
    /*------------------------------------------------------------------------------
            Purpose: Create one or more new records                                                               
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="create", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID CreateconsultasociorecomendadoBE(INPUT-OUTPUT DATASET dsconssociorec):    		        	  		
	    
	    RUN procs/postconsultasociorecomendo.p (INPUT-OUTPUT TABLE ttEncdatos,
                                                OUTPUT TABLE ttSucursal).        
        /* TODO: Add code to create a record from data passed
           in from the client. */        
    END METHOD.    
	

    
END CLASS.
