 /*------------------------------------------------------------------------
    File        :fichasconhorarioxsucBE
    Syntax      : 
    Author(s)   : meji_
    Created     : Mon Jun 08 19:39:47 CDT 2020
    Notes       : ya correji los errores :3
  ----------------------------------------------------------------------*/
  
@program FILE(name=fichasconhorarioxsucBE.cls", module="AppServer").
@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
@progress.service.resource FILE(name=fichasconhorarioxsucBE", URI="fichasconhorarioxsucBE", schemaName="dsfichasconhorarioxsuc", schemaFile="wscajap/AppServer/ds/dsfichasconhorarioxsuc.i").

USING Progress.Lang.*.



CLASS befichasconhorarioxsucBE:
	/*------------------------------------------------------------------------------
			Purpose:                                                                      
			Notes:                                                                        
	------------------------------------------------------------------------------*/
	
	{"ds/dsfichasconhorarioxsuc.i"}
		/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
		
	CONSTRUCTOR PUBLICfichasconhorarioxsucBE (  ):
		SUPER ().
		
	END CONSTRUCTOR.
	
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records, based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="read", URI="?filter=~{filter~}", alias="", mediaType="application/json"). 
    METHOD PUBLIC VOID ReadfichasconhorarioxsucBE(
    		INPUT filter AS CHARACTER, 
    		OUTPUT DATASET dsfichasconhorarioxsuc):
    	
    	DEFINE VARIABLE pcWhere AS CHARACTER.
    	IF filter BEGINS "WHERE " THEN
            pcWhere = filter.
        ELSE IF filter NE "" THEN
            pcWhere = "WHERE " + filter.    	      
	         
	    EMPTY TEMP-TABLE ttEncdatos.
        EMPTY TEMP-TABLE  ttDetFicha.
        CREATE ttEncdatos.
        CREATE  ttDetFicha.    
         
        /* TODO: Add code to get a set of records and return the
           resulting records to the client. */      
    END METHOD.
	  
    /*------------------------------------------------------------------------------
            Purpose: Create one or more new records                                                               
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="create", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID CreatefichasconhorarioxsucBE(INPUT-OUTPUT DATASET dsfichasconhorarioxsuc):    		        	  		
	            
        /* TODO: Add code to create a record from data passed
           in from the client. */
           
        RUN procs/postfichasconhorarioxsuc.p (INPUT-OUTPUT TABLE ttEncdatos,
                                                        OUTPUT TABLE ttDetFicha).                    
    END METHOD.    
	
    /*------------------------------------------------------------------------------
            Purpose:  Update one or more records                                                                  
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="update", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID UpdatefichasconhorarioxsucBE(INPUT-OUTPUT DATASET dsfichasconhorarioxsuc):    		
	
	          
        /* TODO: Add code to update a record from data passed
           in from the client. */          
    END METHOD.
	
    /*------------------------------------------------------------------------------
            Purpose:    Delete a record                                                               
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="delete", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID DeletefichasconhorarioxsucBE(INPUT-OUTPUT DATASET dsfichasconhorarioxsuc):     		       
	
	   
        /* TODO: Add code to delete a record passed in from the client. */
    END METHOD.
	
	 
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records from the ttEncdatos table, 
            		  based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="invoke", URI="/GetttEncdatos", alias="", mediaType="application/json").
    METHOD PUBLIC VOID GetttEncdatos( 
    		INPUT filter AS CHARACTER, 
    		OUTPUT TABLE ttEncdatos ):
	         
        /* TODO: Add code to get records from a specific table and return 
           the resulting records to the client. */  
    END METHOD.
	 
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records from the  ttDetFicha table, 
            		  based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="invoke", URI="/Get ttDetFicha", alias="", mediaType="application/json").
    METHOD PUBLIC VOID  ttDetFicha( 
    		INPUT filter AS CHARACTER, 
    		OUTPUT TABLE  ttDetFicha ):
	         
        /* TODO: Add code to get records from a specific table and return 
           the resulting records to the client. */  
    END METHOD. 
    
END CLASS.