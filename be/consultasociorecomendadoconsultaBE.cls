 
 /*------------------------------------------------------------------------
    File        : consultasociorecomendadoconsultaBE
    Syntax      : 
    Author(s)   : meji_
    Created     : Mon Jun 08 19:39:47 CDT 2020
    Notes       : 
  ----------------------------------------------------------------------*/
  
@program FILE(name="consultasociorecomendadoconsultaBE.cls", module="AppServer").
@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
@progress.service.resource FILE(name="consultasociorecomendadoconsultaBE", URI="/consultasociorecomendadoconsultaBE", schemaName="dsconssocioreccons", schemaFile="wscajap/AppServer/ds/dsconsultasociorecomendadoconsulta.i").

USING Progress.Lang.*.



CLASS be.consultasociorecomendadoconsultaBE:
	/*------------------------------------------------------------------------------
			Purpose:                                                                      
			Notes:                                                                        
	------------------------------------------------------------------------------*/
	
	{"ds/dsconsultasociorecomendadoconsulta.i"}
		/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
		
	CONSTRUCTOR PUBLIC consultasociorecomendadoconsultaBE (  ):
		SUPER ().
		
	END CONSTRUCTOR.
	
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records, based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="read", URI="?filter=~{filter~}", alias="", mediaType="application/json"). 
    METHOD PUBLIC VOID ReadconsultasociorecomendadoconsultaBE(
    		INPUT filter AS CHARACTER, 
    		OUTPUT DATASET dsconssocioreccons):
    	
    	DEFINE VARIABLE pcWhere AS CHARACTER.
    	IF filter BEGINS "WHERE " THEN
            pcWhere = filter.
        ELSE IF filter NE "" THEN
            pcWhere = "WHERE " + filter.    	      
	         
	    EMPTY TEMP-TABLE ttEncdatos.
        EMPTY TEMP-TABLE ttDetalle.
        CREATE ttEncdatos.
        CREATE ttDetalle.    
         
        /* TODO: Add code to get a set of records and return the
           resulting records to the client. */      
    END METHOD.
	  
    /*------------------------------------------------------------------------------
            Purpose: Create one or more new records                                                               
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="create", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID CreateconsultasociorecomendadoconsultaBE(INPUT-OUTPUT DATASET dsconssocioreccons):    		        	  		
	            
        /* TODO: Add code to create a record from data passed
           in from the client. */
           
        RUN procs/postconsultasociorecomendoconsulta.p (INPUT-OUTPUT TABLE ttEncdatos,
                                                        OUTPUT TABLE ttDetalle).                    
    END METHOD.    
	
    /*------------------------------------------------------------------------------
            Purpose:  Update one or more records                                                                  
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="update", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID UpdateconsultasociorecomendadoconsultaBE(INPUT-OUTPUT DATASET dsconssocioreccons):    		
	
	          
        /* TODO: Add code to update a record from data passed
           in from the client. */          
    END METHOD.
	
    /*------------------------------------------------------------------------------
            Purpose:    Delete a record                                                               
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="delete", URI="", alias="", mediaType="application/json").
    METHOD PUBLIC VOID DeleteconsultasociorecomendadoconsultaBE(INPUT-OUTPUT DATASET dsconssocioreccons):     		       
	
	   
        /* TODO: Add code to delete a record passed in from the client. */
    END METHOD.
	
	 
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records from the ttEncdatos table, 
            		  based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="invoke", URI="/GetttEncdatos", alias="", mediaType="application/json").
    METHOD PUBLIC VOID GetttEncdatos( 
    		INPUT filter AS CHARACTER, 
    		OUTPUT TABLE ttEncdatos ):
	         
        /* TODO: Add code to get records from a specific table and return 
           the resulting records to the client. */  
    END METHOD.
	 
    /*------------------------------------------------------------------------------
            Purpose:  Get one or more records from the ttDetalle table, 
            		  based on a filter string                                                                     
            Notes:                                                                        
    ------------------------------------------------------------------------------*/  
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    @progress.service.resourceMapping(type="REST", operation="invoke", URI="/GetttDetalle", alias="", mediaType="application/json").
    METHOD PUBLIC VOID GetttDetalle( 
    		INPUT filter AS CHARACTER, 
    		OUTPUT TABLE ttDetalle ):
	         
        /* TODO: Add code to get records from a specific table and return 
           the resulting records to the client. */  
    END METHOD. 
    
END CLASS.
